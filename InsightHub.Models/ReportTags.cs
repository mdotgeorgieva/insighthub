﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Models
{
    public  class ReportTags
    {
        [Required]
        public Guid TagId { get; set; }
        public Tag Tag { get; set; }
        [Required]
        public Guid ReportId { get; set; }
        public Report Report { get; set; }
    }
}