﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsightHub.Models
{
	public class Report
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        [StringLength(150, ErrorMessage = "{0} must be between {2} and {1} characters", MinimumLength = 10)]
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        [Required]
        public Guid AuthorId { get; set; }
        public Author Author { get; set; }
        [Required]
        public Guid IndustryId { get; set; }
        public Industry Industry { get; set; }
        [Required]
        [MinLength(500, ErrorMessage = "{0} must be between at least {1} characters long")]
        public string Summary { get; set; }
        public string Content { get; set; }
        public ICollection<ClientReports> Downloads { get; set; }
        public ICollection<ReportTags> Tags { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsApproved { get; set; }
        public bool IsFeatured { get; set; }
        public string ImagePath { get; set; }
    }
}
