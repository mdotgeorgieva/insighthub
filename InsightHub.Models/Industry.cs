﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsightHub.Models
{
	public class Industry
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "{0} must be between {2} and {1} characters", MinimumLength = 5)]
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public ICollection<Report> Reports { get; set; }
        public ICollection<UserIndustry> Subscribers { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
        public string ImagePath { get; set; }
    }
}
