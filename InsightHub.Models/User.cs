﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Models
{
	public class User : IdentityUser<Guid>
    {
        [Required]
        [RegularExpression(@"[\D]+")]
        [MaxLength(30, ErrorMessage = "{0} must have max length of {1}")]
        public string FirstName { get; set; }
        [Required]
        [RegularExpression(@"[\D]+")]
        [MaxLength(40, ErrorMessage = "{0} must have max length of {1}")]
        public string LastName { get; set; }
        public string Fullname { get; set; }
        public Client Client { get; set; }
        public Author Author { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsApproved { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsDisabled { get; set; }
    }
}
