﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Models
{
    public class UserIndustry
    {
        [Required]
        public Guid ClientId { get; set; }
        public Client Client { get; set; }
        [Required]
        public Guid IndustryId { get; set; }
        public Industry Industry { get; set; }
    }
}