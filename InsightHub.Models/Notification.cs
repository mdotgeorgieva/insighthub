﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Models
{
	public class Notification
	{
		public Guid Id { get; set; }
		[StringLength(150, ErrorMessage = "{0} must be between {2} and {1} characters", MinimumLength = 50)]
		public string Description { get; set; }

	}
}
