# InsightHub



**InsightHub** is a reports web portal designed to provide market intelligence, advisory services and insights to businesses. <br>[Click here to visit our site](https://insighthubweb.azurewebsites.net).

![Public part](images/homepage.png)

---
## Team Members
- Dimitar Grozev - [Gitlab](https://gitlab.com/dimitar_grozev)
- Maria Mateeva-Georgieva - [Gitlab](https://gitlab.com/mdotgeorgieva)
---
## Areas
- Public - accessible without authentication
- Private - available after registration
- Administrative - available for admins only
---
## Sections
- [Public part](#Public-part)
- [Private part](#Private-part)
- [Administrative part](#Administrative-part)





# Public part


## Report browser
![Reports page](images/reports.png)

### Options:
>**1.Search by different criteria** through the search bar.

>**2.Order by a particular property** by using the sort dropdown.

>**3.Click on a report** to find detailed information about it.

>**4.Save a copy of the report** by clicking the download button. ( **Client users only** )

# Report details

![Report details](images/reportdetails.png)

>Here you can read the full summary of the report and find related reports.

# Private part

## Client
### Priviliges
- **Download** particular reports
- **Subscribe** to industies and receice email notifications upon report publication
- **Browse downloaded reports** on your personal page so you never lose them

![Client page](images/clientpage.png)

<br>

## Author
### Priviliges
- **Create and publish** reports on a distinguished and reputable platform
- **Modify your reports** with the help of our friendly UI

![Create report](images/createreport.png)

---
- **Build your personal profile** where users can find more about you and your work

![Create report](images/authoredit.png)

# Administrative part

> Users with admin rights have the ability to manage the contect on the site by either modifying or removing reports.

![Pending reports](images/pendingreports.png)


> Admins also hava a **Manage users panel** which allows them to see all current active and disabled authors and clients.


# Technologies
- ASP .NET Core
- ASP .NET Identity
- Entity Framework Core
- MS SQL Server
- AJAX
- Bootstrap
- JavaScript / jQuery
- HTML5
- CSS3
- SASS
- Azure
- Swagger

# Additional information
- **300+** Unit tests covering **85%** of the business logic
- Used Azure Blob Storage to handle the binary content
- Used Gitflow workflow throughout the development process
- Implemented Continuous Integration with Gitlab
- Followed the Kanban methodology regarding task management
- Used JWT authorization for the API 