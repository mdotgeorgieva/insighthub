﻿using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Services.Utilities.JWToken;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.API.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
    public class TokensController : ControllerBase
    {
        private readonly IJWTService jWTService;

        public TokensController(IJWTService jWTService)
        {
            this.jWTService = jWTService;
        }

        [HttpPost("token")]
        public async Task<IActionResult> GetTokenAsync(TokenRequestModel model)
        {
            var result = await jWTService.GetTokenAsync(model);
            if (!result.IsAuthenticated)
                return BadRequest();

            return Ok(result);
        }
    }
}
