﻿using System;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs.API_DTO_s;
using InsightHub.Services.Mappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.API.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        private readonly IReportService reportService;
        private readonly IBlobService blobService;
        private readonly IPhotoService photoService;

        public ReportsController(IReportService reportService, IBlobService blobService, IPhotoService photoService)
        {
            this.reportService = reportService;
            this.blobService = blobService;
            this.photoService = photoService;
        }
        // GET: api/Reports
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var reports = await this.reportService.GetAsync();

            return Ok(reports);
        }

        // GET: api/Reports/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var report = await this.reportService.GetAsync(id);

            return Ok(report);
        }

        // POST: api/Reports
        [Authorize(Roles = "Author")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateReportApiDTO reportDTO)
        {
            if (ModelState.IsValid)
            {
                var serviceDTO = reportDTO.GetDTO();
                var newReport = await this.reportService.CreateAsync(serviceDTO);

                await this.blobService.UploadFileBlobAsync(reportDTO.ContentFilePath, newReport.Content);
                await this.photoService.UploadPhoto(reportDTO.ImageFilePath, newReport.ImagePath);

                return Ok();
            }

            return BadRequest();
        }

        // PUT: api/Reports/5
        [Authorize(Roles = "Author")]
        [HttpPut("author/{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UpdateAuthorReportApiDTO reportDTO)
        {
            if (ModelState.IsValid)
            {
                var serviceDTO = reportDTO.GetDTO();
                serviceDTO.Id = id;
                var newReport = await this.reportService.UpdateAsync(serviceDTO);

                if (reportDTO.ImageFilePath != null)
                {
                    await this.photoService.UploadPhoto(reportDTO.ImageFilePath, newReport.ImagePath);
                }

                if (reportDTO.ContentFilePath != null)
                {
                    await this.blobService.UploadFileBlobAsync(reportDTO.ContentFilePath, newReport.Content);
                }
                return Ok();
            }

            return BadRequest();
        }
        // PUT: api/Reports/5
        [Authorize(Roles = "Admin")]
        [HttpPut("admin/{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UpdateAdminReportApiDTO reportDTO)
        {
            if (ModelState.IsValid)
            {
                var serviceDTO = reportDTO.GetDTO();
                serviceDTO.Id = id;
                var newReport = await this.reportService.UpdateAsync(serviceDTO);


                return Ok(newReport);
            }

            return BadRequest();
        }
        // DELETE: api/ApiWithActions/5
        [Authorize(Roles = "Admin,Author")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var isDeleted = await this.reportService.DeleteAsync(id);

            return Ok(isDeleted);
        }
    }
}
