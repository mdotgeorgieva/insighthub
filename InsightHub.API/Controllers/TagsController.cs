﻿using System;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.API.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
    public class TagsController : ControllerBase
    {
        private readonly ITagService tagService;

        public TagsController(ITagService tagService)
        {
            this.tagService = tagService;
        }
        // GET: api/Tags
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var tags = await this.tagService.GetAsync();

            return Ok(tags);
        }

        // GET: api/Tags/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var tag = await this.tagService.GetAsync(id);

            return Ok(tag);
        }

        // POST: api/Tags
        [Authorize(Roles = "Admin")]

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] string name)
        {
            var newTag = await this.tagService.CreateAsync(name);

            return Ok(newTag);
        }

        // PUT: api/Tags/5
        [Authorize(Roles = "Admin")]

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] string name)
        {
            var updatedTag = await this.tagService.UpdateAsync(id, name);

            return Ok(updatedTag);
        }

        // DELETE: api/ApiWithActions/5
        [Authorize(Roles = "Admin")]

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var isDeleted = await this.tagService.DeleteAsync(id);

            return Ok(isDeleted);
        }
    }
}
