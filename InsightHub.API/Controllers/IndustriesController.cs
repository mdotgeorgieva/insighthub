﻿using System;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace InsightHub.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class IndustriesController : ControllerBase
	{
		private readonly IIndustryService industryService;

		public IndustriesController(IIndustryService industryService)
		{
			this.industryService = industryService;
		}
		// GET: api/Industries
		[HttpGet]
		public async Task<IActionResult> Get()
		{
			var industries = await this.industryService.GetAsync();
			return Ok(industries);
		}

		// GET: api/Industries/5
		[HttpGet("{id}")]
		public async Task<IActionResult> Get(Guid id)
		{
			var industryDto = await this.industryService.GetAsync(id);
			return Ok(industryDto);
		}

		// POST: api/Industries
		[Authorize(Roles = "Admin")]

		[HttpPost]
		public async Task<IActionResult> Create([FromBody] string name)
		{
			var industryDto = await this.industryService.CreateAsync(name);
			return Ok(industryDto);			
		}

		// PUT: api/Industries/5
		[Authorize(Roles = "Admin")]

		[HttpPut("{id}")]
		public async Task<ActionResult> Put(Guid id, [FromBody] string newName)
		{
			var updatedIndustry = await this.industryService.UpdateAsync(id, newName);
			return Ok(updatedIndustry);
		}

		// DELETE: api/ApiWithActions/5
		[Authorize(Roles = "Admin")]

		[HttpDelete("{id}")]
		public async Task<IActionResult>  Delete(Guid id)
		{
			var isDeleted =await this.industryService.DeleteAsync(id);
			return Ok(isDeleted);
		}
	}
}
