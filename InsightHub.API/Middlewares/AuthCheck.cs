﻿using Microsoft.AspNetCore.Http;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.API.Middlewares
{
	public class AuthCheck
    {
        private readonly RequestDelegate next;
        public AuthCheck(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {

            if (httpContext.Request.Headers["Authorize"] != "auth123")
            {
                httpContext.Response.Clear();
                httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await httpContext.Response.WriteAsync("Unauthorized");
            }

            await this.next.Invoke(httpContext);
        }
    }
}
