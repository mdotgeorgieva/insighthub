﻿namespace InsightHub.API.Utilities
{
	public class UploadFileRequest
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
    }
}
