﻿using InsightHub.Models;
using InsightHub.Services.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace InsightHub.Services.Mappers
{
	public static class TagMapper
    {
        public static TagDTO GetDto (this Tag tag)
        {
            var tagDto = new TagDTO
            {
                Id = tag.Id,
                Name = tag.Name
            };

            return tagDto;
        }

        public static ICollection<TagDTO> GetDto(this ICollection<Tag> tags)
            => tags.Select(GetDto).ToList();
    }
}
