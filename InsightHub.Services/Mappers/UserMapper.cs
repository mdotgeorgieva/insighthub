﻿using InsightHub.Models;
using InsightHub.Services.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace InsightHub.Services.Mappers
{
	public static class UserMapper
	{
		public static UserDTO GetDto(this User user)
		{
			var userDto = new UserDTO
			{
				Id = user.Id,
				Username = user.UserName,
				FirstName = user.FirstName,
				LastName = user.LastName,
				Role = user.Author!=null ? "Author" : "Client"
			};

			return userDto;
		}

		public static ICollection<UserDTO> GetDto(this ICollection<User> users)
			=> users.Select(GetDto).ToList();
		public static AuthorDTO GetDTO(this Author author)
		{
			var authorDTO = new AuthorDTO
			{
				Id = author.Id,
				Bio = author.Bio,
				FirstName = author.User.FirstName,
				LastName = author.User.LastName,
				ImagePath = author.ImagePath
			};

			return authorDTO;
		}

		public static ICollection<AuthorDTO> GetDTO(this ICollection<Author> users)
			=> users.Select(GetDTO).ToList();
	}
}
