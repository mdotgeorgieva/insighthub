﻿using InsightHub.Models;
using InsightHub.Services.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace InsightHub.Services.Mappers
{
	public static class IndustryMapper
	{
		public static IndustryDTO GetDTO(this Industry industry)
		{
			var industryDTO = new IndustryDTO
			{
				Id = industry.Id,
				Name = industry.Name,
				imagePath = industry.ImagePath,
				Reports = industry.Reports?.GetDTO()
				
			};

			return industryDTO;
		}

		public static ICollection<IndustryDTO> GetDTO(this ICollection<Industry> industries)
			=> industries.Select(GetDTO).ToList();
	}
}
