using InsightHub.Models;
using InsightHub.Services.DTOs;
using InsightHub.Services.DTOs.API_DTO_s;
using System.Collections.Generic;
using System.Linq;

namespace InsightHub.Services.Mappers
{
	public static class ReportMapper
	{
		public static ReportDTO GetDTO(this Report report)
		{
			var reportDTO = new ReportDTO
			{
				Id = report.Id,
				Name = report.Name,
				Author = report.Author?.User.Fullname,
				AuthorId = report.AuthorId,
				Content = report.Content,
				CreatedOn = report.CreatedOn,
				Downloads = report.Downloads?.Count,
				ImagePath = report.ImagePath,
				Industry = report.Industry?.Name,
				IndustryId = report.IndustryId,
				Summary = report.Summary,
				Tags = report.Tags?.Select(t => t.Tag?.Name).ToList(),
				IsFeatured = report.IsFeatured
			};

			return reportDTO;
		}

		public static ICollection<ReportDTO> GetDTO(this ICollection<Report> reports)
			=> reports.Select(GetDTO).ToList();

		public static ReportDTO GetDTO(this CreateReportApiDTO report)
		{
			var reportDto = new ReportDTO
			{
				AuthorId = report.AuthorId,
				IndustryId = report.IndustryId,
				Name = report.Name,
				Summary = report.Summary,
				Tags = report.Tags,
				ImagePath = report.ImageFilePath?.Split('.').Last()
			};
			return reportDto;
		}

		public static ReportDTO GetDTO(this UpdateAuthorReportApiDTO report)
		{
			var reportDto = new ReportDTO
			{
				IndustryId = report.IndustryId,
				Name = report.Name,
				Summary = report.Summary,
				Tags = report.Tags,
				ImagePath = report.ImageFilePath?.Split('.').Last()
			};
			return reportDto;
		}

		public static ReportDTO GetDTO(this UpdateAdminReportApiDTO report)
		{
			var reportDto = new ReportDTO
			{
				IndustryId = report.IndustryId,
				Tags = report.Tags,
			};
			return reportDto;
		}
	}
}
