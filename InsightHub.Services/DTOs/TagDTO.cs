﻿using System;

namespace InsightHub.Services.DTOs
{
	public class TagDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
