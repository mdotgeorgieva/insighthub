﻿using System;
using System.Collections.Generic;

namespace InsightHub.Services.DTOs
{
	public class IndustryDTO
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public ICollection<ReportDTO> Reports { get; set; }
		public string imagePath { get; set; }

	}
}
