﻿using System.Collections.Generic;

namespace InsightHub.Services.DTOs
{
	public class UsersDTO
	{
		public ICollection<UserDTO> Active { get; set; }
		public ICollection<UserDTO> Disabled { get; set; }
	}
}
