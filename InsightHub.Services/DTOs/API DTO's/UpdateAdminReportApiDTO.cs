﻿using System;
using System.Collections.Generic;

namespace InsightHub.Services.DTOs.API_DTO_s
{
	public class UpdateAdminReportApiDTO
	{
		public Guid IndustryId { get; set; }
		public ICollection<string> Tags { get; set; }
	}
}
