﻿using System;
using System.Collections.Generic;

namespace InsightHub.Services.DTOs.API_DTO_s
{
	public class CreateReportApiDTO
	{
		public string Name { get; set; }
		public Guid AuthorId { get; set; }
		public Guid IndustryId { get; set; }
		public string Summary { get; set; }
		public string ContentFilePath { get; set; }
		public ICollection<string> Tags { get; set; }
		public string ImageFilePath { get; set; }
	}
}
