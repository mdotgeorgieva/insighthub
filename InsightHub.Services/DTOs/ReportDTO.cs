using System;
using System.Collections.Generic;

namespace InsightHub.Services.DTOs
{
	public class ReportDTO
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public Guid AuthorId { get; set; }
		public string Author { get; set; }
		public Guid IndustryId { get; set; }
		public string Industry { get; set; }
		public string Summary { get; set; }
		public string Content { get; set; }
		public int? Downloads { get; set; }
		public ICollection<string> Tags { get; set; }
		public DateTime CreatedOn { get; set; }
		public string ImagePath { get; set; }
		public bool IsFeatured { get; set; }
	}
}
