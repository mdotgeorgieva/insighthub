﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.Mappers;
using InsightHub.Services.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Services
{
	public class TagService : ITagService
    {
        private readonly InsightHubContext context;

        public TagService(InsightHubContext context)
        {
            this.context = context;
        }
        public async Task<ICollection<TagDTO>> GetAsync()
        {
            var tags = await this.context.Tags.Where(t => t.IsDeleted == false)
                                              .ToListAsync();

            tags.CheckCollection();

            var tagsDto = tags.GetDto();

            return tagsDto;
        }

        public async Task<TagDTO> GetAsync(Guid id)
        {
            id.IsEmpty();

            var tag = await this.context.Tags.FirstOrDefaultAsync(t => t.Id == id && t.IsDeleted == false);

            tag.CheckForNull(ExceptionMessages.tagNullMsg);

            var tagDto = tag.GetDto();

            return tagDto;
        }

        public async Task<TagDTO> CreateAsync(string name)
        {
            name.CheckForNull(ExceptionMessages.nameNullMsg);

            if (await this.context.Tags.AnyAsync(t => t.Name == name))
            {
                throw new InvalidOperationException("This tag already exist in the database");
            }

            var newTag = new Tag
            {
                Id = Guid.NewGuid(),
                Name = name,
                NormalizedName = name.ToUpper()
            };

            await this.context.AddAsync(newTag);
            await this.context.SaveChangesAsync();

            var tagDto = newTag.GetDto();

            return tagDto;
        }

        public async Task<TagDTO> UpdateAsync(Guid id, string name)
        {
            id.IsEmpty();
            name.CheckForNull(ExceptionMessages.nameNullMsg);

            if (await this.context.Tags.AnyAsync(t => t.Name == name))
            {
                throw new InvalidOperationException("This tag already exist in the database");
            }

            var tagToUpdate = await this.context.Tags.FirstOrDefaultAsync(t => t.Id == id && t.IsDeleted == false);

            tagToUpdate.CheckForNull(ExceptionMessages.tagNullMsg);

            tagToUpdate.Name = name;
            tagToUpdate.NormalizedName = name.ToUpper();
            tagToUpdate.ModifiedOn = DateTime.UtcNow;

            this.context.Update(tagToUpdate);
            await this.context.SaveChangesAsync();

            var tagDto = tagToUpdate.GetDto();

            return tagDto;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            id.IsEmpty();

            var tagToDelete = await this.context.Tags.FirstOrDefaultAsync(t => t.Id == id && t.IsDeleted == false);

            tagToDelete.CheckForNull(ExceptionMessages.tagNullMsg);

            tagToDelete.IsDeleted = true;
            tagToDelete.DeletedOn = DateTime.UtcNow;

            this.context.Update(tagToDelete);
            await this.context.SaveChangesAsync();

            return true;
        }
    }
}
