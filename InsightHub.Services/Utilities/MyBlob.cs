﻿using System.IO;

namespace InsightHub.Services.Utilities
{
	public class MyBlob
    {
        public MyBlob(Stream content, string contentType)
        {
            this.Content = content;
            this.ContentType = contentType;
        }

        public Stream Content { get; set; }
        public string ContentType { get; set; }
    }
}
