﻿namespace InsightHub.Services.Utilities
{
	public static class ExceptionMessages
	{
		//TODO Check convention
		public const string guidEmptyMsg = "Id cannot be empty";
		public const string tagNullMsg = "Tag cannot be null";
		public const string nameNullMsg = "Name cannot be null";
		public const string collectionEmptyMsg = "Collection count is null";
		public const string industryNullMsg = "Industry cannot be null";
		public const string reportNullMsg = "Report cannot be null";
		public const string createReportInvalidMsg = "AuthorId and/or IndustryId cannot be null";
		public const string userNullMsg = "User is missing or null";
		public const string blobNullMsg = "Blob name cannot be null";
		public const string imageNullMsg = "Image cannot be null";
		public const string subNullMsg = "Subscription cannot be null";
		public const string blobPathNullMsg = "Block path cannot be null";
		public const string stringNullMsg = "String cannot be null";
	}
}
