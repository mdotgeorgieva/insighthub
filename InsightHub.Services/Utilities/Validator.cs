﻿using System;
using System.Collections.Generic;

namespace InsightHub.Services.Utilities
{
	public static class Validator
	{

        public static void CheckForNull<T>(this T obj, string msg)
        {
            if (obj == null)
            {
                string name = typeof(T).Name;
                throw new ArgumentNullException(name, msg);
            }
        }
        public static void IsEmpty(this Guid id, string msg = ExceptionMessages.guidEmptyMsg)
        {
            if (id == Guid.Empty)
                throw new ArgumentNullException("id", msg);
        }

        public static void CheckCollection<T>(this ICollection<T> collection)
        {
            if (collection.Count == 0)
            {
                throw new ArgumentOutOfRangeException(ExceptionMessages.collectionEmptyMsg);
            }
        }
    }
}
