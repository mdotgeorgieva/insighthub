﻿using InsightHub.Services.Contracts;
using System;
using System.Net;
using System.Net.Mail;

namespace InsightHub.Services
{
	public class EmailService:IEmailService
	{


		public bool SendEmailNotificationOnAccountApproval(string userEmail)
		{
			MailMessage msg = new MailMessage();

			msg.From = new MailAddress("insighthubbot@gmail.com");
			msg.To.Add(userEmail);
			msg.Subject = "InsightHub account approval";
			msg.Body = "Your account has been approved!" + Environment.NewLine + "<b>Welcome to the <a href='https://localhost:44395/'>InsightHub</a> family!<b>";
			msg.IsBodyHtml = true;
			SmtpClient client = new SmtpClient();
			client.UseDefaultCredentials = true;
			client.Host = "smtp.gmail.com";
			client.Port = 587;
			client.EnableSsl = true;
			client.DeliveryMethod = SmtpDeliveryMethod.Network;
			client.Credentials = new NetworkCredential("insighthubbot@gmail.com", "insighthub99");
			client.Timeout = 20000;
			try
			{
				client.Send(msg);
				return true;
			}
			catch
			{
				return false;
			}
			finally
			{
				msg.Dispose();
			}

		}

		public bool SendEmailWithNewReports(string userEmail, string reportName,Guid reportId)
		{
			var url = "https://localhost:44395/Reports/Details?reportId=" + $"{reportId}";
			MailMessage msg = new MailMessage();

			msg.From = new MailAddress("insighthubbot@gmail.com");
			msg.To.Add(userEmail);
			msg.Subject = "InsightHub new report digest";
			msg.Body = $"A new report <b><a href='{url}'> {reportName}</a> </b> has been uploaded on InsightHub.com";
			msg.IsBodyHtml = true;
			SmtpClient client = new SmtpClient();
			client.UseDefaultCredentials = true;
			client.Host = "smtp.gmail.com";
			client.Port = 587;
			client.EnableSsl = true;
			client.DeliveryMethod = SmtpDeliveryMethod.Network;
			client.Credentials = new NetworkCredential("insighthubbot@gmail.com", "insighthub99");
			client.Timeout = 20000;
			try
			{
				client.Send(msg);
				return true;
			}
			catch
			{
				return false;
			}
			finally
			{
				msg.Dispose();
			}

		}
	}
}
