﻿using InsightHub.Services.Contracts;
using InsightHub.Services.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Threading.Tasks;

namespace InsightHub.Services
{
	public class PhotoService : IPhotoService
	{
		private readonly IConfiguration configuration;

		public PhotoService(IConfiguration configuration)
		{
			this.configuration = configuration;
		}

		public async Task<bool> UploadPhoto(IFormFile image, string fileName)
		{
			image.CheckForNull(ExceptionMessages.imageNullMsg);
			fileName.CheckForNull(ExceptionMessages.nameNullMsg);
			string directory = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
			var filePath = directory + this.configuration["StoredFilesPath"] + fileName;

			using (var stream = File.Create(filePath))
			{
				await image.CopyToAsync(stream);
			}

			return true;
		}

		public async Task<bool> UploadProfilePicture(IFormFile image, string fileName)
		{
			image.CheckForNull(ExceptionMessages.imageNullMsg);
			fileName.CheckForNull(ExceptionMessages.nameNullMsg);
			string directory = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
			var filePath = directory + this.configuration["ProfilePicturesPath"] + fileName;

			using (var stream = File.Create(filePath))
			{
				await image.CopyToAsync(stream);
			}

			return true;
		}
		public async Task<bool> UploadPhoto(string imagePath, string imageName)
		{
			imagePath.CheckForNull(ExceptionMessages.imageNullMsg);
			imageName.CheckForNull(ExceptionMessages.nameNullMsg);

			string directory = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
			var filePath = directory + this.configuration["StoredFilesPath"] + imageName;


			using (var stream = File.Create(filePath))
			{
				var image = new FileStream(imagePath, FileMode.Open);

				await image.CopyToAsync(stream);
			}

			return true;
		}
	}
}
