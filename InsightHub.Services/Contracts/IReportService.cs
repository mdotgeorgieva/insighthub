using InsightHub.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
	public interface IReportService
    {
        Task<ICollection<ReportDTO>> GetAsync();
        Task<ReportDTO> GetAsync(Guid id);
        Task<(ICollection<ReportDTO>, int)> GetReportByPage(string industry, string searchCriteria, string searchString,
                                                                   string sortString, int currentPage, int itemsPerPage);
        Task<ReportDTO> CreateAsync(ReportDTO reportDTO);
        Task<ReportDTO> UpdateAsync(ReportDTO reportDTO);
        Task<bool> DeleteAsync(Guid id);
        Task<ICollection<ReportDTO>> GetPendingAsync();
        Task<ICollection<ReportDTO>> GetAuthorReportsAsync(Guid userId);
        Task<ICollection<ReportDTO>> GetClientReportsAsync(Guid userId);
        Task<ICollection<ReportDTO>> GetFeaturedReportsAsync();
        Task<ICollection<ReportDTO>> GetNewReportsAsync();
        Task<ICollection<ReportDTO>> GetPopularReportsAsync();
        Task<ICollection<ReportDTO>> GetRelatedReportsAsync(string industryName, Guid reportId);
    }
}
