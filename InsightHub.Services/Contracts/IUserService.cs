﻿using InsightHub.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
	public interface IUserService
	{
		Task<ICollection<UserDTO>> GetUsersAsync();
		Task<UserDTO> GetUserAsync(Guid id);
		Task<ICollection<UserDTO>> GetActiveAuthorsAsync();
		Task<ICollection<AuthorDTO>> GetAuthorsForPage();
		Task<ICollection<UserDTO>> GetDisabledAuthorsAsync();
		Task<ICollection<UserDTO>> GetActiveClientsAsync();
		Task<ICollection<UserDTO>> GetDisabledClientsAsync();
		Task<ICollection<UserDTO>> GetPendingUsersAsync();
		Task<bool> CreateAuthorAsync(Guid userId,string bio);
		Task<bool> CreateClientAsync(Guid userId);
		Task<string> CheckUser(string email);
		Task<bool> ApproveUserAsync(Guid userId);
		Task<bool> DeleteAsync(Guid id);
		Task<bool> DisableAsync(Guid userId);
		Task<bool> EnableAsync(Guid userId);
		Task<bool> ApproveReportAsync(Guid reportId);
		Task<bool> RefusePendingReportAsync(Guid reportId);
		Task<bool> RefusePendingUserAsync(Guid userId);
		Task<bool> IsSubscribed(Guid userId, string industry);
		Task<bool> Subscribe(Guid userId, string industry);
		Task<bool> Unsubscribe(Guid userId, string industry);
		Task<bool> DownloadAsync(Guid userId, Guid reportId);
		Task<string[]> GetAuthorInfoAsync(Guid authorId);
		Task<ICollection<AuthorDTO>> GetHomepageAuthorsAsync();
		Task<Guid> GetAuthorIdAsync(Guid userId);
		Task<bool> UpdateAuthorProfileAsync(Guid authorId, string imagePath,string bio);
		Task<bool> ToggleReportAsync(Guid reportId);
	}
}
