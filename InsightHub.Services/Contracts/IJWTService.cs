﻿using InsightHub.Services.Utilities.JWToken;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
	public interface IJWTService
    {
        Task<AuthenticationModel> GetTokenAsync(TokenRequestModel model);
    }
}
