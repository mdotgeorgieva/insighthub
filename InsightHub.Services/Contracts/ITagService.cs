﻿using InsightHub.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
	public interface ITagService
    {
        Task<ICollection<TagDTO>> GetAsync();
        Task<TagDTO> GetAsync(Guid id);
        Task<TagDTO> CreateAsync(string name);
        Task<TagDTO> UpdateAsync(Guid id, string name);
        Task<bool> DeleteAsync(Guid id);
    }
}
