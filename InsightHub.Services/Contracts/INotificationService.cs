﻿using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
	public interface INotificationService
	{
		Task<bool> CreateNotificationAsync(string description);
		int GetCount();
		Task<bool> Remove();
	}
}
