﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
	public interface IPhotoService
    {
        Task<bool> UploadPhoto(IFormFile image, string fileName);
        Task<bool> UploadProfilePicture(IFormFile image, string fileName);
        Task<bool> UploadPhoto(string imagePath, string imageName);
    }
}
