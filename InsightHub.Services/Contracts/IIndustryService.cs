﻿using InsightHub.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
	public interface IIndustryService
	{
		Task<IndustryDTO> CreateAsync(string name);
		Task<IndustryDTO> GetAsync(Guid id);
		Task<ICollection<IndustryDTO>> GetAsync();
		Task<IndustryDTO> UpdateAsync(Guid id, string newName);
		Task<bool> DeleteAsync(Guid id);
	}
}