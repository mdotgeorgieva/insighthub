﻿using System;

namespace InsightHub.Services.Contracts
{
	public interface IEmailService
	{
		bool SendEmailNotificationOnAccountApproval(string userEmail);
		bool SendEmailWithNewReports(string userEmail, string reportName,Guid reportId);
	}
}
