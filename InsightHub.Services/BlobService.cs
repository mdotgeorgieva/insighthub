﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace InsightHub.Services
{
	public class BlobService : IBlobService
    {
        private readonly IConfiguration configuration;
        private readonly BlobServiceClient blobServiceClient;

        public BlobService(IConfiguration configuration, BlobServiceClient blobServiceClient)
        {
            this.configuration = configuration;
            this.blobServiceClient = blobServiceClient;
        }
        public async Task<MyBlob> GetBlobAsync(string id)
        {
            id.CheckForNull(ExceptionMessages.blobNullMsg);

            var containerClient = this.blobServiceClient.GetBlobContainerClient(configuration.GetValue<string>("Container"));
            var blobClient = containerClient.GetBlobClient(id);
            var blobDownloadInfo = await blobClient.DownloadAsync();

            return new MyBlob(blobDownloadInfo.Value.Content, blobDownloadInfo.Value.ContentType);
        }

        public async Task<MyBlob> GetBlobImagesAsync(string id)
        {
            id.CheckForNull(ExceptionMessages.blobNullMsg);

            var containerClient = this.blobServiceClient.GetBlobContainerClient(configuration.GetValue<string>("Container"));
            var blobClient = containerClient.GetBlobClient(id);
            var blobDownloadInfo = await blobClient.DownloadAsync();

            return new MyBlob(blobDownloadInfo.Value.Content, blobDownloadInfo.Value.ContentType);
        }

        public async Task UploadFileBlobAsync(string id, IFormFile file)
        {
            id.CheckForNull(ExceptionMessages.blobNullMsg);

            var containerClient = this.blobServiceClient.GetBlobContainerClient(configuration.GetValue<string>("Container"));
            var blobClient = containerClient.GetBlobClient(id);
            await using var data = file.OpenReadStream();
            await blobClient.UploadAsync(data, new BlobHttpHeaders { ContentType = file.ContentType });
        }

        public async Task DeleteBlob(string id)
        {
            id.CheckForNull(ExceptionMessages.blobNullMsg);

            var containerClient = this.blobServiceClient.GetBlobContainerClient(configuration.GetValue<string>("Container"));
            var blobClient = containerClient.GetBlobClient(id);

            await blobClient.DeleteIfExistsAsync();
        }

        public async Task UploadFileBlobAsync(string filePath, string fileName)
        {
            filePath.CheckForNull(ExceptionMessages.blobPathNullMsg);
            fileName.CheckForNull(ExceptionMessages.blobNullMsg);

            var containerClient = this.blobServiceClient.GetBlobContainerClient(configuration.GetValue<string>("Container"));
            var blobClient = containerClient.GetBlobClient(fileName);

            await blobClient.UploadAsync(filePath, new BlobHttpHeaders { ContentType = filePath.GetContentType() });
        }
    }
}
