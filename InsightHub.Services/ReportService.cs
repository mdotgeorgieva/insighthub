using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.Mappers;
using InsightHub.Services.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Services
{
	public class ReportService : IReportService
    {
        private readonly InsightHubContext context;
        private readonly ITagService tagService;
        private readonly INotificationService notificationService;

        public ReportService(InsightHubContext context, ITagService tagService,INotificationService notificationService)
        {
            this.context = context;
            this.tagService = tagService;
            this.notificationService = notificationService;
        }

        public async Task<ICollection<ReportDTO>> GetAsync()
        {
            var reports = await this.GetReports().ToListAsync();

            reports.CheckCollection();

            var reportsDto = reports.GetDTO();

            return reportsDto;
        }
        public async Task<ICollection<ReportDTO>> GetPendingAsync()
        {
            var reports = await this.context.Reports
                                                .Include(r=>r.Author)
                                                    .ThenInclude(a=>a.User)
                                                .Include(r=>r.Industry)
                                                .Include(r=>r.Downloads)
                                                .Include(r=>r.Tags)
                                                    .ThenInclude(t=>t.Tag)
                                                .Where(r => !r.IsApproved && !r.IsDeleted)
                                                .ToListAsync();
              
            var reportDtos = reports.GetDTO();

            return reportDtos;
        }
        public async Task<ReportDTO> GetAsync(Guid id)
        {
            id.IsEmpty();

            var report = await this.context.Reports.Include(r => r.Author)
                                                   .ThenInclude(a => a.User)
                                                   .Include(r => r.Industry)
                                                   .Include(r => r.Downloads)
                                                   .Include(r => r.Tags)
                                                   .ThenInclude(rt => rt.Tag)
                                                   .FirstOrDefaultAsync(r => r.Id == id && !r.IsDeleted);

            report.CheckForNull(ExceptionMessages.reportNullMsg);

            var reportDto = report.GetDTO();

            return reportDto;
        }

        public async Task<(ICollection<ReportDTO>, int)> GetReportByPage(string industry,string searchCriteria, string searchString,
                                                                   string sortString, int currentPage, int itemsPerPage)
        {
            var reportsquery = this.GetReports();

            if (!string.IsNullOrEmpty(industry))
            {
                reportsquery = reportsquery.Where(r => r.Industry.NormalizedName == industry.ToUpper());
            }

            if (!string.IsNullOrEmpty(searchString) || searchCriteria=="isFeatured")
            {
                reportsquery = this.SearchReports(searchCriteria, searchString, reportsquery);
            }

            reportsquery = this.SortReports(sortString, reportsquery);

            var pages = this.GetTotalPages(itemsPerPage, reportsquery);

            if (currentPage == 1 || currentPage == 0)
            {
                reportsquery = reportsquery.Take(itemsPerPage);
            }
            else
            {
                reportsquery = reportsquery.Skip((currentPage - 1) * itemsPerPage)
                                           .Take(itemsPerPage);
            }

            var reports = await reportsquery.ToListAsync();

            var reportsDto = reports.GetDTO();

            return (reportsDto, pages);
        }

        public IQueryable<Report> GetReports()
        {
            var reportsquery = this.context.Reports.Include(r => r.Author)
                                                   .ThenInclude(a => a.User)
                                                   .Include(r => r.Tags)
                                                   .ThenInclude(rt => rt.Tag)
                                                   .Include(r => r.Industry)
                                                   .Include(r => r.Downloads)
                                                   .Where(r => !r.IsDeleted && r.IsApproved);

            return reportsquery;
        }

        public IQueryable<Report> SearchReports(string searchCriteria, string searchString, IQueryable<Report> reports)
        {
            var normalizedString = searchString?.ToUpper();
            var filteredReports = searchCriteria switch
            {
                "name" => reports.Where(r => r.NormalizedName.Contains(normalizedString)),
                "author" => reports.Where(r => r.Author.User.Fullname.ToUpper().Contains(normalizedString)),
                "isFeatured" => reports.Where(r => r.IsFeatured == true),
                "tag" => reports.Where(r => r.Tags.Where(rt => rt.Tag.NormalizedName.Contains(normalizedString)).Any()),
                _ => reports.Where(r => r.NormalizedName.Contains(normalizedString) || 
                                        r.Author.User.Fullname.ToUpper().Contains(normalizedString) ||
                                        r.Tags.Where(rt => rt.Tag.NormalizedName.Contains(normalizedString)).Any())
            };

            return filteredReports;
        }

        public IQueryable<Report> SortReports(string sortString, IQueryable<Report> reports)
        {
            var sortedReports = sortString switch
            {
                "name_asc" => reports.OrderBy(r => r.Name),
                "name_desc" => reports.OrderByDescending(r => r.Name),
                "downloads_asc" => reports.Include(r => r.Downloads).OrderBy(r => r.Downloads.Count),
                "downloads_desc" => reports.Include(r => r.Downloads).OrderByDescending(r => r.Downloads.Count),
                "date_asc" => reports.OrderBy(r => r.CreatedOn),
                _ => reports.OrderByDescending(r => r.CreatedOn),
            };

            return sortedReports;
        }

        public int GetTotalPages(int itemsPerPage, IQueryable<Report> reports)
        {
            double pages = Math.Ceiling((double)reports.Count() / itemsPerPage);

            int pagesInt = (int)pages;

            return pagesInt;
        }

        public async Task<ReportDTO> CreateAsync(ReportDTO reportDTO)
        {
            reportDTO.AuthorId.IsEmpty();
            reportDTO.IndustryId.IsEmpty();
            reportDTO.Name.CheckForNull(ExceptionMessages.nameNullMsg);

            var authorExists = await this.context.Users.Include(u => u.Author)
                                                       .FirstOrDefaultAsync(u => u.Id == reportDTO.AuthorId && 
                                                                                 u.IsApproved && !u.IsDeleted && !u.IsDisabled);

            var industryExists = await this.context.Industries.AnyAsync(i => i.Id == reportDTO.IndustryId && !i.IsDeleted);

            Report newReport;

            if (authorExists != null && industryExists)
            {
                newReport = new Report()
                {
                    Id = Guid.NewGuid(),
                    Name = reportDTO.Name,
                    NormalizedName = reportDTO.Name.ToUpper(),
                    AuthorId = authorExists.Author.Id,
                    Summary = reportDTO.Summary,
                    IndustryId = reportDTO.IndustryId,
                    Downloads = new List<ClientReports>()
                };
                reportDTO.Id = newReport.Id;
                newReport.Content = reportDTO.Id.ToString();
                newReport.ImagePath = reportDTO.ImagePath != null? reportDTO.Id.ToString() + '.' + reportDTO.ImagePath : "technology.jpg";

            }
            else
            {
                throw new ArgumentNullException(ExceptionMessages.createReportInvalidMsg);
            }

            await this.context.Reports.AddAsync(newReport);
            await this.context.SaveChangesAsync();
            if(reportDTO.Tags != null)
            {
                await this.AddTags(reportDTO);
            }
            await this.notificationService.CreateNotificationAsync($"Report {reportDTO.Name} was created on {DateTime.UtcNow}");
            var reportToReturn = newReport.GetDTO();

            return reportToReturn;
        }

        public async Task<ReportDTO> UpdateAsync(ReportDTO reportDTO)
        {
            reportDTO.Id.IsEmpty();

            if (!(await this.context.Industries.AnyAsync(i => i.Id == reportDTO.IndustryId && !i.IsDeleted)))
            {
                throw new InvalidOperationException("Industry doesn't exist");
            }

            var reportToUpdate = await this.context.Reports.Include(r => r.Tags)
                                                           .FirstOrDefaultAsync(r => r.Id == reportDTO.Id && 
                                                                                     !r.IsDeleted);

            reportToUpdate.CheckForNull(ExceptionMessages.reportNullMsg);
            if (reportDTO.Name != null)
            {
                reportToUpdate.Name = reportDTO.Name;
                reportToUpdate.NormalizedName = reportDTO.Name.ToUpper();
            }
            reportToUpdate.IndustryId = reportDTO.IndustryId;
            if (reportDTO.Summary != null)
                reportToUpdate.Summary = reportDTO.Summary;
            if (reportDTO.Content != null)
            {
                    reportToUpdate.IsApproved = false;
                    reportToUpdate.Content = reportDTO.Content;
            }
            if (reportDTO.ImagePath != null)
                reportToUpdate.ImagePath = reportDTO.Id.ToString() + '.' + reportDTO.ImagePath;
 
            reportToUpdate.ModifiedOn = DateTime.UtcNow;

            this.context.ReportTags.RemoveRange(reportToUpdate.Tags);

            await this.context.SaveChangesAsync();

            if(reportDTO.Tags != null)
                await AddTags(reportDTO);


            var updatedReportDto = reportToUpdate.GetDTO();

            return updatedReportDto;
        }

        private async Task<bool> AddTags(ReportDTO reportDTO)
        {

            foreach (var tag in reportDTO.Tags)
            {
                ReportTags reportTag = new ReportTags();
                var dbTag = await this.context.Tags.FirstOrDefaultAsync(t => t.NormalizedName == tag.ToUpper());

                if (dbTag == null)
                {
                    var newTag = await this.tagService.CreateAsync(tag);
                    reportTag.ReportId = reportDTO.Id;
                    reportTag.TagId = newTag.Id;
                }
                else
                {
                    reportTag.ReportId = reportDTO.Id;
                    reportTag.TagId = dbTag.Id;
                }
                await this.context.ReportTags.AddAsync(reportTag);
            }
            
            await this.context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            id.IsEmpty();

            var reportToDelete = await this.context.Reports.FirstOrDefaultAsync(r => r.Id == id && !r.IsDeleted);

            reportToDelete.CheckForNull(ExceptionMessages.tagNullMsg);

            reportToDelete.IsDeleted = true;
            reportToDelete.DeletedOn = DateTime.UtcNow;

            this.context.Update(reportToDelete);
            await this.context.SaveChangesAsync();

            return true;
        }

        public async Task<ICollection<ReportDTO>> GetAuthorReportsAsync(Guid userId)
        {
            userId.IsEmpty();

            var author = await this.context.Authors.FirstOrDefaultAsync(a => a.UserId == userId);
            author.CheckForNull(ExceptionMessages.userNullMsg);

            var reports = await this.GetReports().Where(r => r.AuthorId == author.Id).OrderByDescending(r=>r.CreatedOn).ToListAsync();

            var reportDtos = reports.GetDTO();

            return reportDtos;
        }

        public async Task<ICollection<ReportDTO>> GetClientReportsAsync(Guid userId)
        {
            userId.IsEmpty();

            var client = await this.context.Clients.FirstOrDefaultAsync(c => c.UserId == userId);

            client.CheckForNull(ExceptionMessages.userNullMsg);

            var reports =await this.context.ClientReports
                                                .Include(cr => cr.Report)
                                                    .ThenInclude(r=>r.Author)
                                                        .ThenInclude(a=>a.User)
                                                .Include(cr=>cr.Report)
                                                    .ThenInclude(r=>r.Industry)
                                                .Include(cr => cr.Report)
                                                    .ThenInclude(r => r.Downloads)
                                                .Include(cr=>cr.Report)
                                                    .ThenInclude(r=>r.Tags)
                                                        .ThenInclude(t=>t.Tag)
                                                .Where(cr => cr.UserId == client.Id && ! cr.Report.IsDeleted && cr.Report.IsApproved)
                                                .Select(cr => cr.Report)
                                                .ToListAsync();

            var reportDtos = reports.GetDTO();

            return reportDtos;
        }

        public async Task<ICollection<ReportDTO>> GetFeaturedReportsAsync()
        {
            var reports = await this.context.Reports.Where(r => r.IsFeatured && !r.IsDeleted && r.IsApproved)
                                                    .OrderBy(r => Guid.NewGuid())
                                                    .Take(3)
                                                    .ToListAsync();

            var reportDtos = reports.GetDTO();

            return reportDtos;
        }

        public async Task<ICollection<ReportDTO>> GetNewReportsAsync()
        {
            var reports = await this.context.Reports
                                            .Where(r=>r.IsApproved && !r.IsDeleted)
                                            .OrderByDescending(r => r.CreatedOn)
                                            .Take(3)
                                            .ToListAsync();
                                                  

            var reportDtos = reports.GetDTO();

            return reportDtos;
        }

        public async Task<ICollection<ReportDTO>> GetPopularReportsAsync()
        {
            var reports = await this.context.Reports
                                            .Include(r => r.Downloads)
                                            .Where(r=>r.IsApproved && !r.IsDeleted)
                                            .OrderByDescending(r => r.Downloads.Count)
                                            .Take(3)
                                            .ToListAsync();

            var reportDtos = reports.GetDTO();

            return reportDtos;

        }

        public async Task<ICollection<ReportDTO>> GetRelatedReportsAsync(string industryName, Guid reportId)
        {
            industryName.CheckForNull(ExceptionMessages.industryNullMsg);

            var reports = await this.context.Reports.Where(r => r.Industry.Name == industryName && 
                                                                r.Id!= reportId &&
                                                                !r.IsDeleted && r.IsApproved)
                                                   .OrderBy(r => Guid.NewGuid())
                                                   .Take(3)
                                                   .ToListAsync();
            var reportsDto = reports.GetDTO();
            return reportsDto;
        }
    }
}
