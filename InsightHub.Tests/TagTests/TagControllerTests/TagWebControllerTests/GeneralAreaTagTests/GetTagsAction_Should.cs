﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagTests.TagControllerTests.TagWebControllerTests.GeneralAreaTagTests
{
	[TestClass]
	public class GetTagsAction_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectCodeWhen_GetTagsDataIsValid()
		{
			ICollection<TagDTO> list = new List<TagDTO>();
			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.GetAsync()).Returns(Task.FromResult(list));

			var tagController = new TagsController(mockTagService.Object);

			var result =await tagController.GetTags();
			var okCode = result as OkObjectResult;

			Assert.AreEqual(200, okCode.StatusCode);
		}

		[TestMethod]
		public async Task CallsCorrectServicesWhen_GetTagsDataIsValid()
		{
			ICollection<TagDTO> list = new List<TagDTO>();
			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.GetAsync()).Returns(Task.FromResult(list));

			var tagController = new TagsController(mockTagService.Object);

			var result = await tagController.GetTags();

			mockTagService.Verify(x => x.GetAsync(), Times.Once);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectCodeWhen_GetTagsDataIsInvalid()
		{
			ICollection<TagDTO> list = new List<TagDTO>();
			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.GetAsync()).Throws(new ArgumentOutOfRangeException());

			var tagController = new TagsController(mockTagService.Object);

			var result = await tagController.GetTags();

			var noContent  = result as NoContentResult;

			Assert.AreEqual(204, noContent.StatusCode);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectCodeWhen_GetTagsDataIsInvalid2()
		{
			ICollection<TagDTO> list = new List<TagDTO>();
			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.GetAsync()).Throws(new Exception());

			var tagController = new TagsController(mockTagService.Object);

			var result = await tagController.GetTags();

			var noContent = result as NotFoundResult;

			Assert.AreEqual(404, noContent.StatusCode);
		}
	}
}
