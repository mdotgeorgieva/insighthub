﻿using InsightHub.API.Controllers;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagTests.TagControllerTests.TagAPIControllerTests
{
	[TestClass]
	public class TagAPIGet_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectStatusCodeWhen_TagAPIGetDataIsValid()
		{
			ICollection<TagDTO> tags = new List<TagDTO>();
			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.GetAsync()).Returns(Task.FromResult(tags));

			var tagController = new TagsController(mockTagService.Object);

			var result =await tagController.Get();
			var statusCode = result as OkObjectResult;

			Assert.AreEqual(200, statusCode.StatusCode);
		}

		[TestMethod]
		public async Task CallsCorrectServicesWhen_TagAPIGetDataIsValid()
		{
			ICollection<TagDTO> tags = new List<TagDTO>();

			var mockTagService = new Mock<ITagService>();
			mockTagService.Setup(x => x.GetAsync()).Returns(Task.FromResult(tags));

			var tagController = new TagsController(mockTagService.Object);

			var result = await tagController.Get();

			mockTagService.Verify(x => x.GetAsync(),Times.Once);
		}
	}
}
