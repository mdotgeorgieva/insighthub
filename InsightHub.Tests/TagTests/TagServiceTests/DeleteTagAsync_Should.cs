﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagTests.TagServiceTests
{
	[TestClass]
    public class DeleteTagAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_DeleteTagParamIsValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_DeleteTagParamIsValid));

            var testTag = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(testTag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);

            Assert.IsTrue(await sut.DeleteAsync(testTag.Id));
        }

        [TestMethod]
        public async Task ThrowWhen_DeleteTagIdIsEmpty()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_DeleteTagIdIsEmpty));

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);


            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(Guid.Empty));
        }

        [TestMethod]
        public async Task ThrowWhen_DeleteTagIdIsWrong()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_DeleteTagIdIsWrong));

            var testTag = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(testTag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);


            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(Guid.NewGuid()));
        }

        [TestMethod]
        public async Task ThrowWhen_DeleteTagIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_DeleteTagIsDeleted));

            var testTag = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag",
                IsDeleted =true
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(testTag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);


            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(testTag.Id));
        }
    }
}
