﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagTests.TagServiceTests
{
	[TestClass]
    public class GetTagsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_GetTagsParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_GetTagsParamsAreValid));

            var testTag1 = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag1"
            };
            var testTag2 = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag2"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddRangeAsync(testTag1, testTag2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);
            var resultCollection = await sut.GetAsync();
            var resultList = resultCollection.ToList();

            Assert.AreEqual(2, resultList.Count);
            Assert.AreEqual(testTag1.Name, resultList[0].Name);
            Assert.AreEqual(testTag2.Name, resultList[1].Name);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneTagIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneTagIsDeleted));

            var testTag1 = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag1"
            };
            var testTag2 = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag2",
                IsDeleted = true
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddRangeAsync(testTag1, testTag2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);
            var resultCollection = await sut.GetAsync();
            var resultList = resultCollection.ToList();

            Assert.AreEqual(1, resultList.Count);
            Assert.AreEqual(testTag1.Name, resultList[0].Name);
        }

        [TestMethod]
        public async Task ThrowWhen_NoTagsAreFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_NoTagsAreFound));

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);


            await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.GetAsync());
        }
    }
}
