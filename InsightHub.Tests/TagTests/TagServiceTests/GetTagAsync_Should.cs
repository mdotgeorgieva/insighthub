﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagTests.TagServiceTests
{
	[TestClass]
    public class GetTagAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_GetTagParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_GetTagParamsAreValid));

            var testTag = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag"
            };

            using(var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(testTag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);
            var resultTag = await sut.GetAsync(testTag.Id);

            Assert.AreEqual(testTag.Name, resultTag.Name);
        }

        [TestMethod]
        public async Task ThrowWhen_GetTagGuidIsEmpty()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_GetTagGuidIsEmpty));

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);


            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAsync(Guid.Empty));
        }

        [TestMethod]
        public async Task ThrowWhen_GetTagIdIsWrong()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_GetTagIdIsWrong));

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);


            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAsync(Guid.NewGuid()));
        }

        [TestMethod]
        public async Task ThrowWhen_TagIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_TagIsDeleted));

            var testTag = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag",
                IsDeleted = true
            };

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);


            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAsync(testTag.Id));
        }
    }
}
