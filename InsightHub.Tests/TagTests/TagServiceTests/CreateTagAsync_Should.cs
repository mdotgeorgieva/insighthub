﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagTests.TagServiceTests
{
	[TestClass]
    public class CreateTagAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_CreateTagNameIsValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_CreateTagNameIsValid));
            var name = "TestTag";

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);
            var result = await sut.CreateAsync(name);

            Assert.AreEqual(name, result.Name);
            Assert.IsInstanceOfType(result, typeof(TagDTO));
            Assert.AreEqual(1, assertContext.Tags.Count());
        }

        [TestMethod]
        public async Task ThrowWhen_CreateTagNameIsNull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_CreateTagNameIsNull));
            string name = null;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAsync(name));
        }

        [TestMethod]
        public async Task ThrowWhen_CreateTagNameIsTaken()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_CreateTagNameIsTaken));

            var name = "TestTag";

            var testTag = new Tag
            {
                Id = Guid.NewGuid(),
                Name = "TestTag"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(testTag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new TagService(assertContext);

            await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.CreateAsync(name));
        }
    }
}
