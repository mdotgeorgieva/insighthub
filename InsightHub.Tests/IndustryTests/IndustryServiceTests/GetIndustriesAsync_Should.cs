﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryTests.IndustryServiceTests
{
	[TestClass]
	public class GetIndustriesAsync_Should
	{
		[TestMethod]
		public async Task ReturnCorrectGetWhen_IndustriesParamsAreValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectGetWhen_IndustriesParamsAreValid));
			var industry1 = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "Industry1"
			};
			var industry2 = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "Industry2"
			};

			using (var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.AddRangeAsync(industry1, industry2);
				await arrangeContext.SaveChangesAsync();
			}
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			var result = await sut.GetAsync();
			var list = result.ToList();
			Assert.AreEqual(2, list.Count);
			Assert.AreEqual(industry1.Id, list[0].Id);
			Assert.AreEqual(industry1.Name, list[0].Name);
			Assert.AreEqual(industry2.Id, list[1].Id);
			Assert.AreEqual(industry2.Name, list[1].Name);

		}
		[TestMethod]
		public async Task ReturnCorrectWhen_GetIndustriesAreDeleted()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_GetIndustriesAreDeleted));
			var industry1 = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "Industry1",
				IsDeleted = true

			};
			var industry2 = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "Industry2"
			};

			using (var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.AddRangeAsync(industry1, industry2);
				await arrangeContext.SaveChangesAsync();
			}
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			var result = await sut.GetAsync();
			var list = result.ToList();
			Assert.AreEqual(1, list.Count);
			Assert.AreEqual(industry2.Id, list[0].Id);
			Assert.AreEqual(industry2.Name, list[0].Name);

		}
		[TestMethod]
		public async Task ThrowWhen_NoIndustriesAreFound()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrowWhen_NoIndustriesAreFound));
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.GetAsync());
		}
	}
}

