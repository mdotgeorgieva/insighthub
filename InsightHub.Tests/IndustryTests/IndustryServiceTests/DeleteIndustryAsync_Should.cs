﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryTests.IndustryServiceTests
{
	[TestClass]
	public class DeleteIndustryAsync_Should
	{
		[TestMethod]
		public async Task ReturnCorrectWhen_DeleteIndustryIdIsValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_DeleteIndustryIdIsValid));
			var industry = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "MyIndustry",
			};
			using (var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.AddAsync(industry);
				await arrangeContext.SaveChangesAsync();
			}
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);

			Assert.IsTrue(await sut.DeleteAsync(industry.Id));
		}


		[TestMethod]
		public async Task ThrowWhen_DeleteIndustryIdIsEmpty()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrowWhen_DeleteIndustryIdIsEmpty));
		
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(Guid.Empty));
		}
		[TestMethod]
		public async Task ThrowWhen_DeleteIndustryIsAlreadyDeleted()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrowWhen_DeleteIndustryIsAlreadyDeleted));
			var industry = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "MyIndustry",
				IsDeleted=true
			};
			using (var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.AddAsync(industry);
				await arrangeContext.SaveChangesAsync();
			}
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(industry.Id));
		}

		[TestMethod]
		public async Task ThrowWhen_DeleteIndustryIdIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrowWhen_DeleteIndustryIdIsInvalid));

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(Guid.NewGuid()));
		}
	}
}
