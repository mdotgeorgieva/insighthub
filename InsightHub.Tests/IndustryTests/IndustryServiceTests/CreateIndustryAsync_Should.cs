﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryTests.IndustryServiceTests
{
	[TestClass]
	public class CreateIndustryAsync_Should
	{
		[TestMethod]
		public async Task ReturnCorrenctWhen_CreatedIndustryNameIsValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrenctWhen_CreatedIndustryNameIsValid));
			string name = "Industry";
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			var result = await sut.CreateAsync(name);
			Assert.IsInstanceOfType(result, typeof(IndustryDTO));
			Assert.AreEqual(name, result.Name);
			Assert.AreEqual(1, assertContext.Industries.Count());
		}

		[TestMethod]
		public async Task ThrownWhen_CreatedIndustryNameIsNull()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrownWhen_CreatedIndustryNameIsNull));
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAsync(null));
		}

		[TestMethod]
		public async Task ThrownWhen_CreatedIndustryNameIsTaken()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrownWhen_CreatedIndustryNameIsTaken));
			var industry = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "Industry"
			};

			using (var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.AddRangeAsync(industry);
				await arrangeContext.SaveChangesAsync();
			}
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.CreateAsync("Industry"));
		}
	}
}
