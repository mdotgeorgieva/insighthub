﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryTests.IndustryServiceTests
{
	[TestClass]
	public class GetIndustryAsync_Should
	{
		[TestMethod]
		public async Task ReturnCorrectWhen_GetIndustryParamsAreValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_GetIndustryParamsAreValid));

			var industry = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "MyIndustry",
			};
			using(var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.AddAsync(industry);
				await arrangeContext.SaveChangesAsync();
			}
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			var result = await sut.GetAsync(industry.Id);

			Assert.IsInstanceOfType(result,typeof(IndustryDTO));
			Assert.AreEqual( industry.Id, result.Id);
			Assert.AreEqual( industry.Name, result.Name);
		}

		[TestMethod]
		public async Task ThrowWhen_GetIndustryGuidIsEmpty()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrowWhen_GetIndustryGuidIsEmpty));

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			await Assert.ThrowsExceptionAsync<ArgumentNullException>(()=>sut.GetAsync(Guid.Empty));
		}

		[TestMethod]
		public async Task ThrowWhen_GetIndustryIdIsWrong()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrowWhen_GetIndustryIdIsWrong));

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAsync(Guid.NewGuid()));
		}

		[TestMethod]
		public async Task ThrowWhen_IndustryIsDeleted()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ThrowWhen_IndustryIsDeleted));

			var industry = new Industry()
			{
				Id = Guid.NewGuid(),
				Name = "MyIndustry",
				IsDeleted=true
			};
			using (var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.AddAsync(industry);
				await arrangeContext.SaveChangesAsync();
			}
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new IndustryService(assertContext);
			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAsync(industry.Id));
		}
	}
}
