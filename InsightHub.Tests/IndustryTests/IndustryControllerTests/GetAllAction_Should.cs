﻿using InsightHub.API.Controllers;
using InsightHub.Data;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryTests.IndustryControllerTests
{
	[TestClass]
    public class GetAllAction_Should
    {
        [TestMethod]
        public async Task ReturnOkWhen_IndustriesDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnOkWhen_IndustriesDataIsCorrect));
            ICollection<IndustryDTO> collection = new List<IndustryDTO>();
            var mockIndustryService = new Mock<IIndustryService>();
            mockIndustryService.Setup(x => x.GetAsync()).Returns(Task.FromResult(collection));

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new IndustriesController(mockIndustryService.Object);

            var result = await sut.Get() as OkObjectResult;

            Assert.AreEqual(200, result.StatusCode);
        }
    }
}
