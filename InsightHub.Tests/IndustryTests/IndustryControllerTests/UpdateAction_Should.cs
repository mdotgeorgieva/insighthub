﻿using InsightHub.API.Controllers;
using InsightHub.Data;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryTests.IndustryControllerTests
{
	[TestClass]
    public class UpdateAction_Should
    {
        [TestMethod]
        public async Task ReturnOkWhen_UpdateIndustryDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnOkWhen_UpdateIndustryDataIsCorrect));
            var mockIndustryService = new Mock<IIndustryService>();
            mockIndustryService.Setup(x => x.UpdateAsync(It.IsAny<Guid>(), It.IsAny<String>())).Returns(Task.FromResult(new IndustryDTO()));

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new IndustriesController(mockIndustryService.Object);

            var result = await sut.Put(Guid.NewGuid(), "test") as OkObjectResult;

            Assert.AreEqual(200, result.StatusCode);
        }
    }
}
