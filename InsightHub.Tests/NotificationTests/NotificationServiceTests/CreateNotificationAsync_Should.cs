﻿using InsightHub.Data;
using InsightHub.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace InsightHub.Tests.NotificationTests.NotificationServiceTests
{
	[TestClass]
    public class CreateNotificationAsync_Should
    {
        [TestMethod]
        public async Task ReturnTrueWhen_CreateNotificationParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnTrueWhen_CreateNotificationParamsAreValid));
            //Arrange
            var testDescription = "test description";

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new NotificationService(assertContext);

            var result = await sut.CreateNotificationAsync(testDescription);

            Assert.IsTrue(result);
        }
    }
}
