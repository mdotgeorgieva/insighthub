﻿using InsightHub.Data;
using InsightHub.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.NotificationTests.NotificationServiceTests
{
	[TestClass]
    public class GetCount_Should
    {
        [TestMethod]
        public void ReturnCorrectWhen_GetCountUserDataIsValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_GetCountUserDataIsValid));
            Utils.Seed(options);

            //Act & Assert
            using var assertContnext = new InsightHubContext(options);
            var sut = new NotificationService(assertContnext);

            var result = sut.GetCount();

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void ReturnCorrectWhen_GetCountReportDataIsValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_GetCountReportDataIsValid));
            Utils.Seed(options);

            Utils.report.IsApproved = false;

            using(var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.report);
                testContext.SaveChanges();
            }

            //Act & Assert
            using var assertContnext = new InsightHubContext(options);
            var sut = new NotificationService(assertContnext);

            var result = sut.GetCount();

            Assert.AreEqual(2, result);
        }
    }
}
