﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Web.Controllers;
using InsightHub.Web.Models.IndexModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.HomeTests
{
	[TestClass]
	public class HomeIndex_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectModelWhen_HomeIndexDataIsValid()
		{
			ICollection<ReportDTO> reports = new List<ReportDTO>();

			var mockReportService = new Mock<IReportService>();
			mockReportService.Setup(x => x.GetFeaturedReportsAsync()).Returns(Task.FromResult(reports));
			mockReportService.Setup(x => x.GetPopularReportsAsync()).Returns(Task.FromResult(reports));
			mockReportService.Setup(x => x.GetNewReportsAsync()).Returns(Task.FromResult(reports));

			ICollection<AuthorDTO> authors = new List<AuthorDTO>();

			var mockUserService = new Mock<IUserService>();
			mockUserService.Setup(x => x.GetHomepageAuthorsAsync()).Returns(Task.FromResult(authors));

			var homeController = new HomeController(mockReportService.Object, mockUserService.Object);

			var result =await homeController.Index() as ViewResult;

			Assert.IsInstanceOfType(result.Model, typeof(HomePageVM));
		}

		[TestMethod]
		public async Task ShouldCallCorrectServicesWhen_HomeIndexDataIsValid()
		{
			ICollection<ReportDTO> reports = new List<ReportDTO>();

			var mockReportService = new Mock<IReportService>();
			mockReportService.Setup(x => x.GetFeaturedReportsAsync()).Returns(Task.FromResult(reports));
			mockReportService.Setup(x => x.GetPopularReportsAsync()).Returns(Task.FromResult(reports));
			mockReportService.Setup(x => x.GetNewReportsAsync()).Returns(Task.FromResult(reports));

			ICollection<AuthorDTO> authors = new List<AuthorDTO>();

			var mockUserService = new Mock<IUserService>();
			mockUserService.Setup(x => x.GetHomepageAuthorsAsync()).Returns(Task.FromResult(authors));

			var homeController = new HomeController(mockReportService.Object, mockUserService.Object);

			var result = await homeController.Index() as ViewResult;

			mockReportService.Verify(x => x.GetFeaturedReportsAsync(), Times.Once);
			mockReportService.Verify(x => x.GetPopularReportsAsync(), Times.Once);
			mockReportService.Verify(x => x.GetNewReportsAsync(), Times.Once);
			mockUserService.Verify(x => x.GetHomepageAuthorsAsync(), Times.Once);
		}
	}
}
