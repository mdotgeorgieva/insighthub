﻿using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Author.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NToastNotify;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportControllerTests.ReportsWebControllerTests.AuthorAreaReportTests
{
	[TestClass]
	public class AuthorReportsDelete_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectViewWhen_ReportsDeleteDataIsValid()
		{
			var mockReportService = new Mock<IReportService>();
			mockReportService.Setup(x => x.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));
			var mockIndustryService = new Mock<IIndustryService>();
			var mockPhotoService = new Mock<IPhotoService>();
			var mockBlobService = new Mock<IBlobService>();
			mockBlobService.Setup(x => x.DeleteBlob(It.IsAny<string>())).Returns(Task.FromResult(true));
			var mockToastNotification = new Mock<IToastNotification>().Object;

			var reportsController = new ReportsController(mockReportService.Object, mockIndustryService.Object, mockBlobService.Object, mockPhotoService.Object,mockToastNotification);

			var result = (RedirectToPageResult) await reportsController.Delete(Guid.NewGuid(), "author");

			Assert.AreEqual("/Account/Manage/Reports", result.PageName);
		}

		[TestMethod]
		public async Task ShouldReturnCorrectViewWhen_ReportsDeleteDataIsValid2()
		{
			var mockReportService = new Mock<IReportService>();
			mockReportService.Setup(x => x.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));
			var mockIndustryService = new Mock<IIndustryService>();
			var mockPhotoService = new Mock<IPhotoService>();
			var mockBlobService = new Mock<IBlobService>();
			mockBlobService.Setup(x => x.DeleteBlob(It.IsAny<string>())).Returns(Task.FromResult(true));
			var mockToastNotification = new Mock<IToastNotification>().Object;

			var reportsController = new ReportsController(mockReportService.Object, mockIndustryService.Object, mockBlobService.Object, mockPhotoService.Object,mockToastNotification);

			var result = (RedirectToActionResult)await reportsController.Delete(Guid.NewGuid(), "client");

			Assert.AreEqual("Index", result.ActionName);
		}

	}
}
