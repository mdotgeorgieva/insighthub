﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Web.Controllers;
using InsightHub.Web.Models.IndexModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportControllerTests.ReportsWebControllerTests.GeneralAreaReportsTests
{
	[TestClass]
	public class ReportsActions_Should
	{
		[TestMethod]
		public async Task ReturnViewWhen_ReportsDataIsCorrect()
		{
            //Arrange
            ICollection<ReportDTO> list = new List<ReportDTO>();
            var mockReportService = new Mock<IReportService>();
            mockReportService.Setup(x => x.GetReportByPage(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), 
                                                            It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                                                            .Returns(Task.FromResult((list,2)));
            var mockUserService = new Mock<IUserService>().Object;

            //Act,Assert
            var controller = new ReportsController(mockReportService.Object, mockUserService);
            var reportsQuery = new ReportQueryVM();
            var result = await controller.Index(reportsQuery) as ViewResult;
            var model = result.ViewData.Model;

            Assert.IsInstanceOfType(model, typeof(ReportQueryVM));
        }

        [TestMethod]
        public async Task CallRightServicesWhen_ReportsDataIsCorrect()
        {
            //Arrange
            ICollection<ReportDTO> list = new List<ReportDTO>();
            var mockReportService = new Mock<IReportService>();
            mockReportService.Setup(x => x.GetReportByPage(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                                                            It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                                                            .Returns(Task.FromResult((list, 2)));
            var mockUserService = new Mock<IUserService>().Object;

            //Act,Assert
            var controller = new ReportsController(mockReportService.Object, mockUserService);
            var reportsQuery = new ReportQueryVM();
            var result = await controller.Index(reportsQuery) as ViewResult;
            var model = result.ViewData.Model;

            mockReportService.Verify(x => x.GetReportByPage(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                                                            It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
        }

        [TestMethod]
        public async Task RedirectsWhen_ReportsModelDataIsInvalid()
        {
            //Arrange
            var mockUserService = new Mock<IUserService>().Object;
            //Act,Assert
            var controller = new ReportsController(null, mockUserService);

            var result =(RedirectToActionResult) await controller.Index(null);

            Assert.AreEqual("Index", result.ActionName);
        }
    }
}
