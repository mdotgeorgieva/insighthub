﻿using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Admin.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NToastNotify;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportControllerTests.ReportsWebControllerTests.AdminAreaReportsTests
{
	[TestClass]
	public class AdminReportsApproveReport_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectViewWhen_ApproveReportDataIsValid()
		{
			//Arrange

			var mockUserService = new Mock<IUserService>();
			mockUserService.Setup(x => x.ApproveReportAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

			var mockToastNotificationService = new Mock<IToastNotification>();
			var mockBlobService = new Mock<IBlobService>();
			var mockReportService = new Mock<IReportService>();

			//Act,Assert
			var reportController = new ReportsController(mockUserService.Object, mockToastNotificationService.Object, mockBlobService.Object, mockReportService.Object);

			var result =(RedirectToActionResult) await reportController.ApproveReport(Guid.Empty);

			Assert.AreEqual("Pending", result.ActionName);
		}

		[TestMethod]
		public async Task CallsCorrectServicesWhen_ApproveReportDataIsValid()
		{
			//Arrange

			var mockUserService = new Mock<IUserService>();
			mockUserService.Setup(x => x.ApproveReportAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

			var mockToastNotificationService = new Mock<IToastNotification>();
			var mockBlobService = new Mock<IBlobService>();
			var mockReportService = new Mock<IReportService>();

			//Act,Assert
			var reportController = new ReportsController(mockUserService.Object, mockToastNotificationService.Object, mockBlobService.Object, mockReportService.Object);

			var result = await reportController.ApproveReport(Guid.Empty);

			mockUserService.Verify(x => x.ApproveReportAsync(It.IsAny<Guid>()), Times.Once);
		}

		[TestMethod]
		public async Task CallsCorrectServicesWhen_ApproveReportDataIsInvalid()
		{
			//Arrange

			var mockUserService = new Mock<IUserService>();
			mockUserService.Setup(x => x.ApproveReportAsync(It.IsAny<Guid>())).Throws(new ArgumentNullException());

			var mockToastNotificationService = new Mock<IToastNotification>();
			var mockBlobService = new Mock<IBlobService>();
			var mockReportService = new Mock<IReportService>();

			//Act,Assert
			var reportController = new ReportsController(mockUserService.Object, mockToastNotificationService.Object, mockBlobService.Object, mockReportService.Object);

			var result = (RedirectToActionResult)await reportController.ApproveReport(Guid.Empty);

			Assert.AreEqual("Pending", result.ActionName);
		}
	}
}
