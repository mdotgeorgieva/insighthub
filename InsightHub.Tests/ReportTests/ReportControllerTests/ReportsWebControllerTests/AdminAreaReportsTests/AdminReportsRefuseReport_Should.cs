﻿using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Admin.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NToastNotify;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportControllerTests.ReportsWebControllerTests.AdminAreaReportsTests
{
	[TestClass]
	public class AdminReportsRefuseReport_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectViewWhen_RefuseReportDataIsValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectViewWhen_RefuseReportDataIsValid));

			var mockUserService = new Mock<IUserService>();
			mockUserService.Setup(x => x.RefusePendingReportAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

			var mockToastNotificationService = new Mock<IToastNotification>();
			var mockBlobService = new Mock<IBlobService>();
			var mockReportService = new Mock<IReportService>();

			//Act,Assert
			var reportController = new ReportsController(mockUserService.Object, mockToastNotificationService.Object, mockBlobService.Object, mockReportService.Object);

			var result = (RedirectToActionResult)await reportController.RefuseReport(Guid.Empty);

			Assert.AreEqual("Pending", result.ActionName);
		}

		[TestMethod]
		public async Task CallsCorrectServicesWhen_RefuseReportDataIsValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(CallsCorrectServicesWhen_RefuseReportDataIsValid));

			var mockUserService = new Mock<IUserService>();
			mockUserService.Setup(x => x.RefusePendingReportAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

			var mockToastNotificationService = new Mock<IToastNotification>();
			var mockBlobService = new Mock<IBlobService>();
			var mockReportService = new Mock<IReportService>();

			//Act,Assert
			var reportController = new ReportsController(mockUserService.Object, mockToastNotificationService.Object, mockBlobService.Object, mockReportService.Object);

			var result = await reportController.RefuseReport(Guid.Empty);

			mockUserService.Verify(x => x.RefusePendingReportAsync(It.IsAny<Guid>()), Times.Once);
			mockBlobService.Verify(x => x.DeleteBlob(It.IsAny<string>()), Times.Once);
		}

		[TestMethod]
		public async Task CallsCorrectServicesWhen_RefuseReportDataIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(CallsCorrectServicesWhen_RefuseReportDataIsInvalid));

			var mockUserService = new Mock<IUserService>();
			mockUserService.Setup(x => x.RefusePendingReportAsync(It.IsAny<Guid>())).Throws(new ArgumentNullException());

			var mockToastNotificationService = new Mock<IToastNotification>();
			var mockBlobService = new Mock<IBlobService>();
			var mockReportService = new Mock<IReportService>();

			//Act,Assert
			var reportController = new ReportsController(mockUserService.Object, mockToastNotificationService.Object, mockBlobService.Object, mockReportService.Object);

			var result = (RedirectToActionResult)await reportController.RefuseReport(Guid.Empty);

			Assert.AreEqual("Pending", result.ActionName);
		}
	}
}
