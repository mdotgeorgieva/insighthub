﻿using InsightHub.API.Controllers;
using InsightHub.Data;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportControllerTests.ReportsAPIControllerTests
{
	[TestClass]
    public class GetReportByIdAction_Should
    {
        [TestMethod]
        public async Task ReturnOkWhen_ReportByIdDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnOkWhen_ReportByIdDataIsCorrect));
            ReportDTO report = new ReportDTO();
            var mockReportService = new Mock<IReportService>();
            mockReportService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(report));

            var mockBlobService = new Mock<IBlobService>().Object;
            var mockPhotoService = new Mock<IPhotoService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new ReportsController(mockReportService.Object, mockBlobService, mockPhotoService);

            var result = await sut.Get(Guid.NewGuid()) as OkObjectResult;

            Assert.AreEqual(200, result.StatusCode);
        }
    }
}
