﻿using InsightHub.API.Controllers;
using InsightHub.Data;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportControllerTests.ReportsAPIControllerTests
{
	[TestClass]
    public class DeleteAction_Should
    {
        [TestClass]
        public class GetReportByIdAction_Should
        {
            [TestMethod]
            public async Task ReturnOkWhen_DeleteReportDataIsCorrect()
            {
                //Arrange
                var options = Utils.GetOptions(nameof(ReturnOkWhen_DeleteReportDataIsCorrect));
                var mockReportService = new Mock<IReportService>();
                mockReportService.Setup(x => x.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

                var mockBlobService = new Mock<IBlobService>().Object;
                var mockPhotoService = new Mock<IPhotoService>().Object;

                //Act&Assert
                using var assertContext = new InsightHubContext(options);
                var sut = new ReportsController(mockReportService.Object, mockBlobService, mockPhotoService);

                var result = await sut.Delete(Guid.NewGuid()) as OkObjectResult;

                Assert.AreEqual(200, result.StatusCode);
            }
        }
    }
}
