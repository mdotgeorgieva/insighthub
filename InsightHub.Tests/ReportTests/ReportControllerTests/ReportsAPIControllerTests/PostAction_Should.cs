﻿using InsightHub.API.Controllers;
using InsightHub.Data;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.DTOs.API_DTO_s;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportControllerTests.ReportsAPIControllerTests
{
	[TestClass]
    public class PostAction_Should
    {
        [TestMethod]
        public async Task ReturnOkWhen_CreateReportDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnOkWhen_CreateReportDataIsCorrect));
            CreateReportApiDTO reportForController = new CreateReportApiDTO();
            ReportDTO report = new ReportDTO();
            var mockReportService = new Mock<IReportService>();
            mockReportService.Setup(x => x.CreateAsync(It.IsAny<ReportDTO>())).Returns(Task.FromResult(report));

            var mockBlobService = new Mock<IBlobService>().Object;
            var mockPhotoService = new Mock<IPhotoService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new ReportsController(mockReportService.Object, mockBlobService, mockPhotoService);

            var result = await sut.Post(reportForController) as OkResult;

            Assert.AreEqual(200, result.StatusCode);
        }
        [TestMethod]
        public async Task CallRightServiceWhen_CreateReportDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CallRightServiceWhen_CreateReportDataIsCorrect));
            CreateReportApiDTO reportForController = new CreateReportApiDTO();
            var mockReportService = new Mock<IReportService>();
            mockReportService.Setup(x => x.CreateAsync(It.IsAny<ReportDTO>())).Returns(Task.FromResult(new ReportDTO()));

            var mockBlobService = new Mock<IBlobService>().Object;
            var mockPhotoService = new Mock<IPhotoService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new ReportsController(mockReportService.Object, mockBlobService, mockPhotoService);

            var result = await sut.Post(reportForController) as OkResult;

            mockReportService.Verify(x => x.CreateAsync(It.IsAny<ReportDTO>()), Times.Once);
        }
    }
}
