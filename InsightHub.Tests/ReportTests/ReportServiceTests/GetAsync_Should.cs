﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportServiceTests
{
	[TestClass]
	public class GetAsync_Should
	{
		[TestMethod]
		public async Task ReturnCorrentWhen_GetReportParamsAreValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrentWhen_GetReportParamsAreValid));
			Utils.Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result =await sut.GetAsync();
			var list = result.ToList();
			Assert.AreEqual(2,list.Count);
			Assert.AreEqual(Utils.report.Id,list[0].Id);

		}

		[TestMethod]
		public async Task ThrowWhen_GetReportsDatabaseIsEmpty()
		{
			var options = Utils.GetOptions(nameof(ThrowWhen_GetReportsDatabaseIsEmpty));
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotification = new Mock<INotificationService>().Object;
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotification);

			await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.GetAsync());
		}
		
	}
}
