﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportServiceTests
{
	[TestClass]
	public class GetFeaturedReportsAsync_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_GetFeaturedReportsAreValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_GetFeaturedReportsAreValid));
			Utils.Seed(options);

			var mockNotificationService = new Mock<INotificationService>().Object;
			var mockTagService = new Mock<ITagService>().Object;
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result = await sut.GetFeaturedReportsAsync();
			var list = result.ToList();
			Assert.AreEqual(1, list.Count);
			Assert.AreEqual(Utils.report1.Id, list[0].Id);
			
		}

	}
}
