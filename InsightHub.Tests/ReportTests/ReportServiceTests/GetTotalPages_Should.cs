﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace InsightHub.Tests.ReportTests.ReportServiceTests
{
	[TestClass]
	public class GetTotalPages_Should
	{
		[TestMethod]
		public void  ShouldReturnCorrectWhen_ItemsAreValid_1()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_ItemsAreValid_1));
			Utils.Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var reportQuery = sut.GetReports();
			var result = sut.GetTotalPages(2, reportQuery);

			Assert.AreEqual(1, result);
		}

		[TestMethod]
		public void ShouldReturnCorrectWhen_ItemsAreValid_2()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_ItemsAreValid_2));
			Utils.Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotification = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotification);

			var reportQuery = sut.GetReports();
			var result = sut.GetTotalPages(1, reportQuery);

			Assert.AreEqual(2, result);
		}

		[TestMethod]
		public void ShouldReturnCorrectWhen_NoItemsAreFiltered()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_NoItemsAreFiltered));
			Utils.Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotification = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotification);

			var reportQuery = sut.GetReports().Where(r=>r.Name.Contains("random string"));
			var result = sut.GetTotalPages(1, reportQuery);

			Assert.AreEqual(0, result);
		}
	}
}
