﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportServiceTests
{
	[TestClass]
	public class CreateAsync_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_CreateReportParamsAreValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_CreateReportParamsAreValid));
			Utils.Seed(options);
			var reportDto = new ReportDTO
			{
				Name = "My new report",
				AuthorId = Utils.user.Id,
				IndustryId = Utils.industry.Id,
				Tags = new List<string> { "happy","sad"}
			};

			var mockTagService = new Mock<ITagService>();
			mockTagService.SetupSequence(t => t.CreateAsync(It.IsAny<string>()))
							.Returns(Task.FromResult(new TagDTO { Id = Guid.NewGuid(), Name = "happy" }))
							.Returns(Task.FromResult(new TagDTO { Id = Guid.NewGuid(), Name = "sad" }));

			var mockNotificationService = new Mock<INotificationService>();

			mockNotificationService.Setup(n => n.CreateNotificationAsync(It.IsAny<string>()))
													.Returns(Task.FromResult(true));
													
													

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService.Object,mockNotificationService.Object);

			var result =await sut.CreateAsync(reportDto);
			Assert.AreEqual(result.Name, reportDto.Name);
			mockNotificationService.Verify(n => n.CreateNotificationAsync(It.IsAny<string>()), Times.Once());
		}

		[TestMethod]
		public async Task ShouldThrowWhen_CreateReportAuthorIdIsEmpty()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_CreateReportAuthorIdIsEmpty));
			Utils.Seed(options);
			var reportDto = new ReportDTO
			{
				Name = "My new report",
				AuthorId = Guid.Empty,
				IndustryId = Utils.industry.Id,
				Tags = new List<string> { "happy", "sad" }
			};

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAsync(reportDto));
		}

		[TestMethod]
		public async Task ShouldThrowWhen_CreateReportIndustryIdIsEmpty()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_CreateReportIndustryIdIsEmpty));
			Utils.Seed(options);
			var reportDto = new ReportDTO
			{
				Name = "My new report",
				AuthorId = Utils.author.Id,
				IndustryId = Guid.Empty,
				Tags = new List<string> { "happy", "sad" }
			};

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAsync(reportDto));
		}

		[TestMethod]
		public async Task ShouldThrowWhen_CreateReportNameIsNullIsEmpty()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_CreateReportNameIsNullIsEmpty));
			Utils.Seed(options);
			var reportDto = new ReportDTO
			{
				Name = null,
				AuthorId = Utils.author.Id,
				IndustryId = Utils.industry.Id,
				Tags = new List<string> { "happy", "sad" }
			};

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAsync(reportDto));
		}

		[TestMethod]
		public async Task ShouldThrowWhen_CreateReportAuthorIdIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_CreateReportAuthorIdIsInvalid));
			Utils.Seed(options);
			var reportDto = new ReportDTO
			{
				Name = null,
				AuthorId = Guid.NewGuid(),
				IndustryId = Utils.industry.Id,
				Tags = new List<string> { "happy", "sad" }
			};

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAsync(reportDto));
		}

		[TestMethod]
		public async Task ShouldThrowWhen_CreateReportIndustryIdIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_CreateReportIndustryIdIsInvalid));
			Utils.Seed(options);
			var reportDto = new ReportDTO
			{
				Name = null,
				AuthorId = Utils.author.Id,
				IndustryId = Guid.NewGuid(),
				Tags = new List<string> { "happy", "sad" }
			};

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAsync(reportDto));
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_CreateReportNewTagsAreAdded()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_CreateReportNewTagsAreAdded));
			Utils.Seed(options);
			var reportDto = new ReportDTO
			{
				Name = "My new report",
				AuthorId = Utils.user.Id,
				IndustryId = Utils.industry.Id,
				Tags = new List<string> { "happy", "sad" }
			};

			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var tagService = new TagService(assertContext);
			var sut = new ReportService(assertContext, tagService, mockNotificationService);
			var result =await sut.CreateAsync(reportDto);

			Assert.AreEqual(4, assertContext.Tags.Count());
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_CreateReportExistringTagsAreUsed()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_CreateReportExistringTagsAreUsed));
			Utils.Seed(options);
			var reportDto = new ReportDTO
			{
				Name = "My new report",
				AuthorId = Utils.user.Id,
				IndustryId = Utils.industry.Id,
				Tags = new List<string> { "Tag", "Tag1" }
			};

			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var tagService = new TagService(assertContext);
			var sut = new ReportService(assertContext, tagService, mockNotificationService);
			var result = await sut.CreateAsync(reportDto);

			Assert.AreEqual(2, assertContext.Tags.Count());
		}

		[TestMethod]
		public async Task ShouldReturnCorrectWhen_CreateReportAddsNotificationToTable()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_CreateReportAddsNotificationToTable));
			Utils.Seed(options);
			var reportDto = new ReportDTO
			{
				Name = "My new report",
				AuthorId = Utils.user.Id,
				IndustryId = Utils.industry.Id,
				Tags = new List<string> { "Tag", "Tag1" }
			};

			var mockTagService = new Mock<ITagService>().Object;
			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var notificationService = new NotificationService(assertContext);
			var sut = new ReportService(assertContext, mockTagService, notificationService);
			var result = await sut.CreateAsync(reportDto);

			Assert.AreEqual(1, assertContext.Notifications.Count());
		}
	}
}
