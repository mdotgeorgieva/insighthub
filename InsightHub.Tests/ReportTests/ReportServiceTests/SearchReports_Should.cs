﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace InsightHub.Tests.ReportTests.ReportServiceTests
{
	[TestClass]
	public class SearchReports_Should
	{
		[TestMethod]
		public void  ReturnCorrectWhen_ReportsSearchCriteriaIsName()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_ReportsSearchCriteriaIsName));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportsQuery = sut.GetReports();
			var result = sut.SearchReports("name", "test", reportsQuery).ToList();

			Assert.AreEqual(2, result.Count);

		}

		[TestMethod]
		public void ReturnCorrectWhen_ReportsSearchCriteriaIsName1()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_ReportsSearchCriteriaIsName1));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportsQuery = sut.GetReports();
			var result = sut.SearchReports("name", "random string", reportsQuery).ToList();

			Assert.AreEqual(0, result.Count);

		}

		[TestMethod]
		public void ReturnCorrectWhen_ReportsSearchCriteriaIsAuthor()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_ReportsSearchCriteriaIsAuthor));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportsQuery = sut.GetReports();
			var result = sut.SearchReports("author", "j", reportsQuery).ToList();

			Assert.AreEqual(2, result.Count);

		}

		[TestMethod]
		public void ReturnCorrectWhen_ReportsSearchCriteriaIsAuthor1()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_ReportsSearchCriteriaIsAuthor1));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportsQuery = sut.GetReports();
			var result = sut.SearchReports("author", "random author", reportsQuery).ToList();

			Assert.AreEqual(0, result.Count);

		}

		[TestMethod]
		public void ReturnCorrectWhen_ReportsSearchCriteriaIsFeatured()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_ReportsSearchCriteriaIsFeatured));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportsQuery = sut.GetReports();
			var result = sut.SearchReports("isFeatured", "", reportsQuery).ToList();

			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(Utils.report1.Id, result[0].Id);

		}

		[TestMethod]
		public void ReturnCorrectWhen_ReportsSearchCriteriaIsTag()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_ReportsSearchCriteriaIsTag));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportsQuery = sut.GetReports();
			var result = sut.SearchReports("tag", "tag", reportsQuery).ToList();

			Assert.AreEqual(2, result.Count);

		}

		[TestMethod]
		public void ReturnCorrectWhen_ReportsSearchCriteriaIsTag1()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_ReportsSearchCriteriaIsTag1));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportsQuery = sut.GetReports();
			var result = sut.SearchReports("tag", "random tag", reportsQuery).ToList();

			Assert.AreEqual(0, result.Count);

		}
		[TestMethod]
		public void ReturnCorrectWhen_ReportsSearchCriteriaIsEmpty_Title()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_ReportsSearchCriteriaIsEmpty_Title));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportsQuery = sut.GetReports();
			var result = sut.SearchReports(null, "test", reportsQuery).ToList();

			Assert.AreEqual(2, result.Count);

		}
		[TestMethod]
		public void ReturnCorrectWhen_ReportsSearchCriteriaIsEmpty_Author()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_ReportsSearchCriteriaIsEmpty_Author));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportsQuery = sut.GetReports();
			var result = sut.SearchReports(null, "j", reportsQuery).ToList();

			Assert.AreEqual(2, result.Count);

		}
		[TestMethod]
		public void ReturnCorrectWhen_ReportsSearchCriteriaIsEmpty_Tag()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ReturnCorrectWhen_ReportsSearchCriteriaIsEmpty_Tag));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportsQuery = sut.GetReports();
			var result = sut.SearchReports(null, "tag", reportsQuery).ToList();

			Assert.AreEqual(2, result.Count);

		}
	}
}
