﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace InsightHub.Tests.ReportTests.ReportServiceTests
{
	[TestClass]
	public class SortReports_Should
	{
		[TestMethod]
		public void ShouldReturnCorrectWhen_SortStringIsNameAsc()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_SortStringIsNameAsc));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportQuery = sut.GetReports();
			var result = sut.SortReports("name_asc", reportQuery).ToList();

			Assert.AreEqual(Utils.report.Id, result[0].Id);
			Assert.AreEqual(Utils.report1.Id, result[1].Id);
		}

		[TestMethod]
		public void ShouldReturnCorrectWhen_SortStringIsNameDesc()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_SortStringIsNameDesc));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportQuery = sut.GetReports();
			var result = sut.SortReports("name_desc", reportQuery).ToList();

			Assert.AreEqual(Utils.report.Id, result[1].Id);
			Assert.AreEqual(Utils.report1.Id, result[0].Id);
		}

		[TestMethod]
		public void ShouldReturnCorrectWhen_SortStringIsDownloadsAsc()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_SortStringIsDownloadsAsc));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportQuery = sut.GetReports();
			var result = sut.SortReports("downloads_asc", reportQuery).ToList();

			Assert.AreEqual(Utils.report.Id, result[1].Id);
			Assert.AreEqual(Utils.report1.Id, result[0].Id);
		}

		[TestMethod]
		public void ShouldReturnCorrectWhen_SortStringIsDownloadsDesc()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_SortStringIsDownloadsDesc));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportQuery = sut.GetReports();
			var result = sut.SortReports("downloads_desc", reportQuery).ToList();

			Assert.AreEqual(Utils.report.Id, result[0].Id);
			Assert.AreEqual(Utils.report1.Id, result[1].Id);
		}

		[TestMethod]
		public void ShouldReturnCorrectWhen_SortStringIsCreatedOnAsc()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_SortStringIsCreatedOnAsc));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportQuery = sut.GetReports();
			var result = sut.SortReports("date_asc", reportQuery).ToList();

			Assert.AreEqual(Utils.report.Id, result[0].Id);
			Assert.AreEqual(Utils.report1.Id, result[1].Id);
		}

		[TestMethod]
		public void ShouldReturnCorrectWhen_SortStringIsEmpty()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_SortStringIsEmpty));
			Utils.Seed(options);

			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var reportQuery = sut.GetReports();
			var result = sut.SortReports(null, reportQuery).ToList();

			Assert.AreEqual(Utils.report.Id, result[1].Id);
			Assert.AreEqual(Utils.report1.Id, result[0].Id);
		}
	}
}
