﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportServiceTests
{
	[TestClass]
	public class GetNewReportsAsync_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrectWhen_GetNewReportsAreValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrectWhen_GetNewReportsAreValid));
			Utils.Seed(options);
			var report = new Report
			{
				Id = Guid.NewGuid(),
				Name = "test report",
				NormalizedName = "TEST REPORT",
				AuthorId = Utils.author.Id,
				IndustryId = Utils.industry.Id,
				CreatedOn = DateTime.Parse("Jan 1, 2020"),
				IsApproved = true,
				Downloads = new List<ClientReports>()
			};
			using(var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.Reports.AddAsync(report);
				await arrangeContext.SaveChangesAsync();
			}
			var mockNotificationService = new Mock<INotificationService>().Object;
			var mockTagService = new Mock<ITagService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);

			var result =await sut.GetNewReportsAsync();
			var list = result.ToList();

			Assert.AreEqual(3, list.Count);
			Assert.AreEqual(report.Id, list[0].Id);
			Assert.AreEqual(Utils.report.Id, list[2].Id);
			Assert.AreEqual(Utils.report1.Id, list[1].Id);
		}
	}
}
