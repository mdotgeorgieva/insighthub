﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportTests.ReportServiceTests
{
	[TestClass]
	public class GetAsyncById_Should
	{
		[TestMethod]
		public async Task ShouldReturnCorrect_WhenGetReportByIdParamsAreValid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldReturnCorrect_WhenGetReportByIdParamsAreValid));

			Utils.Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			var result =await sut.GetAsync(Utils.report.Id);
			Assert.IsInstanceOfType(result, typeof(ReportDTO));
			Assert.AreEqual(Utils.report.Id, result.Id);
		}
		[TestMethod]
		public async Task ShouldThrowWhen_GetReportByIdWhenIdIsEmpty()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_GetReportByIdWhenIdIsEmpty));

			Utils.Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAsync(Guid.Empty));
		}

		[TestMethod]
		public async Task ShouldThrowWhen_GetReportByIdWhenIdIsInvalid()
		{
			//Arrange
			var options = Utils.GetOptions(nameof(ShouldThrowWhen_GetReportByIdWhenIdIsInvalid));

			Utils.Seed(options);
			var mockTagService = new Mock<ITagService>().Object;
			var mockNotificationService = new Mock<INotificationService>().Object;

			//Act,Assert
			using var assertContext = new InsightHubContext(options);
			var sut = new ReportService(assertContext, mockTagService, mockNotificationService);
			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAsync(Guid.NewGuid()));
		}
	}
}
