﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Web.Areas.Admin.Controllers;
using InsightHub.Web.Areas.Admin.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NToastNotify;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserControllerTests.UsersWebControllerTests.AdminAreaUsersTests
{
	[TestClass]
    public class PendingAction_Should
    {
        [TestMethod]
        public async Task ReturnViewWhen_PendingDataIsCorrect()
        {
            ICollection<UserDTO> userList = new List<UserDTO>();
            ICollection<ReportDTO> reportList = new List<ReportDTO>();

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetPendingUsersAsync()).Returns(Task.FromResult(userList));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>();
            mockReportService.Setup(x => x.GetPendingAsync()).Returns(Task.FromResult(reportList));

            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService.Object);

            var result = await controller.Pending() as ViewResult;
            var model = result.ViewData.Model;

            Assert.IsInstanceOfType(model, typeof(PendingVM));
        }

        [TestMethod]
        public async Task CallRightServicesWhen_PendingDataIsCorrect()
        {
            ICollection<UserDTO> userList = new List<UserDTO>();
            ICollection<ReportDTO> reportList = new List<ReportDTO>();

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetPendingUsersAsync()).Returns(Task.FromResult(userList));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>();
            mockReportService.Setup(x => x.GetPendingAsync()).Returns(Task.FromResult(reportList));

            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService.Object);

            var result = await controller.Pending() as ViewResult;

            mockUserService.Verify(x => x.GetPendingUsersAsync(), Times.Once);
            mockReportService.Verify(x => x.GetPendingAsync(), Times.Once);
        }

        [TestMethod]
        public async Task RedirectWhen_PendingServiceThrows()
        {
            var exception = new ArgumentNullException();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetPendingUsersAsync()).Throws(exception);

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.Pending();

            Assert.AreEqual("Index", result.ActionName);
        }
    }
}
