﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Web.Areas.Admin.Controllers;
using InsightHub.Web.Areas.Admin.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NToastNotify;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserControllerTests.UsersWebControllerTests.AdminAreaUsersTests
{
	[TestClass]
    public class AuthorsAction_Should
    {
        [TestMethod]
        public async Task ReturnViewWhen_AuthorsDataIsCorrect()
        {
            ICollection<UserDTO> list = new List<UserDTO>();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetActiveAuthorsAsync()).Returns(Task.FromResult(list));
            mockUserService.Setup(x => x.GetDisabledAuthorsAsync()).Returns(Task.FromResult(list));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification, 
                                                    mocknotificationService, mockReportService);

            var result = await controller.Authors() as ViewResult;
            var model = result.ViewData.Model;

            Assert.IsInstanceOfType(model, typeof(UsersVM));
        }

        [TestMethod]
        public async Task CallRightServicesWhen_AuthorsDataIsCorrect()
        {
            ICollection<UserDTO> list = new List<UserDTO>();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetActiveAuthorsAsync()).Returns(Task.FromResult(list));
            mockUserService.Setup(x => x.GetDisabledAuthorsAsync()).Returns(Task.FromResult(list));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = await controller.Authors() as ViewResult;

            mockUserService.Verify(x => x.GetActiveAuthorsAsync(), Times.Once);
            mockUserService.Verify(x => x.GetDisabledAuthorsAsync(), Times.Once);
        }

        [TestMethod]
        public async Task RedirectWhen_AuthorsServiceThrows()
        {
            var exception = new ArgumentNullException();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetActiveAuthorsAsync()).Throws(exception);

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult) await controller.Authors();

            Assert.AreEqual("Index", result.ActionName);
        }
    }
}
