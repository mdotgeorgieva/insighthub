﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Web.Areas.Admin.Controllers;
using InsightHub.Web.Areas.Admin.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NToastNotify;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserControllerTests.UsersWebControllerTests.AdminAreaUsersTests
{
	[TestClass]
    public class ClientsAction_Should
    {
        [TestMethod]
        public async Task ReturnViewWhen_ClientsDataIsCorrect()
        {
            ICollection<UserDTO> list = new List<UserDTO>();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetActiveClientsAsync()).Returns(Task.FromResult(list));
            mockUserService.Setup(x => x.GetDisabledClientsAsync()).Returns(Task.FromResult(list));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = await controller.Clients() as ViewResult;
            var model = result.ViewData.Model;

            Assert.IsInstanceOfType(model, typeof(UsersVM));
        }

        [TestMethod]
        public async Task CallRightServicesWhen_ClientDataIsCorrect()
        {
            ICollection<UserDTO> list = new List<UserDTO>();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetActiveClientsAsync()).Returns(Task.FromResult(list));
            mockUserService.Setup(x => x.GetDisabledClientsAsync()).Returns(Task.FromResult(list));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = await controller.Clients() as ViewResult;

            mockUserService.Verify(x => x.GetActiveClientsAsync(), Times.Once);
            mockUserService.Verify(x => x.GetDisabledClientsAsync(), Times.Once);
        }

        [TestMethod]
        public async Task RedirectWhen_AuthorsServiceThrows()
        {
            var exception = new ArgumentNullException();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetActiveClientsAsync()).Throws(exception);

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.Clients();

            Assert.AreEqual("Index", result.ActionName);
        }
    }
}
