﻿using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Admin.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NToastNotify;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserControllerTests.UsersWebControllerTests.AdminAreaUsersTests
{
	[TestClass]
    public class RefuseUserAction_Should
    {
        [TestMethod]
        public async Task CallCorrectServiceWhen_RefuseUserDataIsCorrect()
        {
            var userId = Guid.NewGuid();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.RefusePendingUserAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.RefuseUser(userId);

            mockUserService.Verify(x => x.RefusePendingUserAsync(userId), Times.Once);
        }

        [TestMethod]
        public async Task RedirectWhen_RefuseDataIsCorrect()
        {
            var userId = Guid.NewGuid();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.RefusePendingUserAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.RefuseUser(userId);

            Assert.AreEqual("Pending", result.ActionName);
        }

        [TestMethod]
        public async Task RedirectWhen_RefuseServiceThrows()
        {
            var userId = Guid.NewGuid();
            var exception = new ArgumentNullException();

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.RefusePendingUserAsync(It.IsAny<Guid>())).Throws(exception);

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.RefuseUser(userId);

            Assert.AreEqual("Pending", result.ActionName);
        }
    }
}
