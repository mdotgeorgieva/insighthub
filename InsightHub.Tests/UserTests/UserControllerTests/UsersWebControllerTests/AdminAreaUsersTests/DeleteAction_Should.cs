﻿using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Admin.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NToastNotify;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserControllerTests.UsersWebControllerTests.AdminAreaUsersTests
{
	[TestClass]
    public class DeleteAction_Should
    {
        [TestMethod]
        public async Task CallCorrectServiceWhen_DeleteDataIsCorrect()
        {
            var userId = Guid.NewGuid();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.Delete(userId, "authors");

            mockUserService.Verify(x => x.DeleteAsync(userId), Times.Once);
        }

        [TestMethod]
        public async Task RedirectWhen_DeleteComingFromAuthors()
        {
            var userId = Guid.NewGuid();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.Delete(userId, "authors");

            Assert.AreEqual("Authors", result.ActionName);
        }

        [TestMethod]
        public async Task RedirectWhen_DeleteComingFromClients()
        {
            var userId = Guid.NewGuid();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.Delete(userId, "clients");

            Assert.AreEqual("Clients", result.ActionName);
        }

        [TestMethod]
        public async Task RedirectWhen_DeleteComingFromElse()
        {
            var userId = Guid.NewGuid();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.Delete(userId, "default");

            Assert.AreEqual("Index", result.ActionName);
        }

        [TestMethod]
        public async Task RedirectWhen_DeleteServiceThrows()
        {
            var userId = Guid.NewGuid();
            var exception = new ArgumentNullException();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.DeleteAsync(It.IsAny<Guid>())).Throws(exception);

            var mocknotificationService = new Mock<INotificationService>().Object;
            var mockReportService = new Mock<IReportService>().Object;
            var mockToastNotification = new Mock<IToastNotification>().Object;

            var controller = new UsersController(mockUserService.Object, mockToastNotification,
                                                    mocknotificationService, mockReportService);

            var result = (RedirectToActionResult)await controller.Delete(userId, "default");

            Assert.AreEqual("Authors", result.ActionName);
        }
    }
}
