﻿using InsightHub.API.Controllers;
using InsightHub.Data;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserControllerTests.UsersAPIControllerTests
{
	[TestClass]
    public class GetUserById_Should
    {
        [TestMethod]
        public async Task ReturnOkWhen_UserByIdDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnOkWhen_UserByIdDataIsCorrect));
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetUserAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new UserDTO()));

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UsersController(mockUserService.Object);

            var result = await sut.Get(Guid.NewGuid()) as OkObjectResult;

            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public async Task CallRightServiceWhen_UserByUdDataIsCorrect()
        {
            var options = Utils.GetOptions(nameof(CallRightServiceWhen_UserByUdDataIsCorrect));
            ICollection<UserDTO> collection = new List<UserDTO>();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetUserAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new UserDTO()));

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UsersController(mockUserService.Object);

            var result = await sut.Get(Guid.NewGuid()) as OkResult;

            mockUserService.Verify(x => x.GetUserAsync(It.IsAny<Guid>()), Times.Once);
        }
    }
}
