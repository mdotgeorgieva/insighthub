﻿using InsightHub.API.Controllers;
using InsightHub.Data;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserControllerTests.UsersAPIControllerTests
{
	[TestClass]
    public class GetAuthorsAction_Should
    {
        [TestMethod]
        public async Task ReturnOkWhen_AuthorsDataIsCorrect()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnOkWhen_AuthorsDataIsCorrect));
            ICollection<UserDTO> collection = new List<UserDTO>();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetActiveAuthorsAsync()).Returns(Task.FromResult(collection));
            mockUserService.Setup(x => x.GetDisabledAuthorsAsync()).Returns(Task.FromResult(collection));


            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UsersController(mockUserService.Object);

            var result = await sut.GetAuthors() as OkObjectResult;

            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public async Task CallRightServiceWhen_ClientsDataIsCorrect()
        {
            var options = Utils.GetOptions(nameof(CallRightServiceWhen_ClientsDataIsCorrect));
            ICollection<UserDTO> collection = new List<UserDTO>();
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(x => x.GetActiveAuthorsAsync()).Returns(Task.FromResult(collection));
            mockUserService.Setup(x => x.GetDisabledAuthorsAsync()).Returns(Task.FromResult(collection));

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UsersController(mockUserService.Object);

            var result = await sut.GetAuthors() as OkObjectResult;

            mockUserService.Verify(x => x.GetActiveAuthorsAsync(), Times.Once);
            mockUserService.Verify(x => x.GetDisabledAuthorsAsync(), Times.Once);
        }
    }
}
