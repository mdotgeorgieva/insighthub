﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class GetAuthorsForPage_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_AllAuthorsForPageAreActive()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_AllAuthorsForPageAreActive));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetAuthorsForPage();
            var resultList = result.ToList();

            Assert.AreEqual(2, result.Count);
            Assert.IsInstanceOfType(result, typeof(ICollection<AuthorDTO>));
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneForPageIsNotApproved()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneForPageIsNotApproved));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user1.IsApproved = false;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user1);
                await testContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetAuthorsForPage();
            var resultList = result.ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(Utils.author.Id, resultList[0].Id);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneForPageIsDisabled()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneForPageIsDisabled));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user1.IsDisabled = true;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user1);
                await testContext.SaveChangesAsync();
            }
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetAuthorsForPage();
            var resultList = result.ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(Utils.author.Id, resultList[0].Id);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneForPageIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneForPageIsDeleted));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user1.IsDeleted = true;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user1);
                await testContext.SaveChangesAsync();
            }
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetAuthorsForPage();
            var resultList = result.ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(Utils.author.Id, resultList[0].Id);
        }
    }
}
