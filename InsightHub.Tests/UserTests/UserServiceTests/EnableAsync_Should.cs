﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class EnableAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_EnableParamsArevalid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_EnableParamsArevalid));
            var mockEmailService = new Mock<IEmailService>().Object;
            var user = new User()
            {
                Id = Guid.NewGuid(),
                UserName = "user1@ih.com",
                FirstName = "John",
                LastName = "Smith",
                Fullname = "John Smith",
                PhoneNumber = "+1666777666",
                NormalizedUserName = "USER1@IH.COM",
                Email = "user1@ih.com",
                NormalizedEmail = "USER1@IH.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = true,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
                EmailConfirmed = true,
                IsApproved = true,
                IsDisabled = true
            };
            var hasher = new PasswordHasher<User>();

            user.PasswordHash = hasher.HashPassword(user, "user");

            var author = new Author
            {
                Id = Guid.NewGuid(),
                UserId = user.Id,
                Bio = "test bio"
            };

            using (var arrangedContext = new InsightHubContext(options))
            {
                await arrangedContext.Users.AddAsync(user);
                await arrangedContext.Authors.AddAsync(author);
                await arrangedContext.SaveChangesAsync();
            }

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.EnableAsync(user.Id);
            var updatedUser = await assertContext.Users.FirstOrDefaultAsync(u => u.Id == user.Id);

            Assert.IsTrue(result);
            Assert.IsFalse(updatedUser.IsDisabled);
            //Assert.IsNull(updatedUser.LockoutEnabled); TODO Check!
        }

        [TestMethod]
        public async Task ThrowWhen_EnableUserIdIsEmpty()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_EnableUserIdIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.EnableAsync(Guid.Empty));
        }

        [TestMethod]
        public async Task ThrowWhen_EnabledUserDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_EnabledUserDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.EnableAsync(Guid.NewGuid()));
        }
    }
}
