﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class DisableAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_DisableUserParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_DisableUserParamsAreValid));
            var mockEmailService = new Mock<IEmailService>().Object;

            var user = new User()
            {
                Id = Guid.NewGuid(),
                UserName = "user1@ih.com",
                FirstName = "John",
                LastName = "Smith",
                Fullname = "John Smith",
                PhoneNumber = "+1666777666",
                NormalizedUserName = "USER1@IH.COM",
                Email = "user1@ih.com",
                NormalizedEmail = "USER1@IH.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = true,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
                EmailConfirmed = true,
                IsApproved = true,
                IsDisabled = false
            };
            var hasher = new PasswordHasher<User>();

            user.PasswordHash = hasher.HashPassword(user, "user");

            var author = new Author
            {
                Id = Guid.NewGuid(),
                UserId = user.Id,
                Bio = "test bio"
            };

            var role = new Role { Id = Guid.Parse("6C8FCD7E-62F6-4F3E-A73D-ACBFD60B97AC"), Name = "Author", NormalizedName = "AUTHOR" };
            var userRole = new IdentityUserRole<Guid>()
            {
                RoleId = role.Id,
                UserId = user.Id
            };

            using (var arrangedContext = new InsightHubContext(options))
            {
                await arrangedContext.Users.AddAsync(user);
                await arrangedContext.Roles.AddAsync(role);
                await arrangedContext.UserRoles.AddAsync(userRole);
                await arrangedContext.Authors.AddAsync(author);
                await arrangedContext.SaveChangesAsync();
            }

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.DisableAsync(user.Id);
            var updatedUser = await assertContext.Users.FirstOrDefaultAsync(u => u.Id == user.Id);

            Assert.IsTrue(result);
            Assert.IsTrue(updatedUser.IsDisabled);
        }

        [TestMethod]
        public async Task ThrowWhen_DisableUserIdIsEmpty()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_DisableUserIdIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DisableAsync(Guid.Empty));
        }

        [TestMethod]
        public async Task ThrowWhen_DisabledUserDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_DisabledUserDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DisableAsync(Guid.NewGuid()));
        }

        [TestMethod]
        public async Task ThrowWhen_TryToDisableAdmin()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_TryToDisableAdmin));
            var mockEmailService = new Mock<IEmailService>().Object;

            var user = new User()
            {
                Id = Guid.NewGuid(),
                UserName = "user1@ih.com",
                FirstName = "John",
                LastName = "Smith",
                Fullname = "John Smith",
                PhoneNumber = "+1666777666",
                NormalizedUserName = "USER1@IH.COM",
                Email = "user1@ih.com",
                NormalizedEmail = "USER1@IH.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = true,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
                EmailConfirmed = true,
                IsApproved = true,
                IsDisabled = false
            };
            var hasher = new PasswordHasher<User>();

            user.PasswordHash = hasher.HashPassword(user, "user");

            var role = new Role { Id = Guid.Parse("6C8FCD7E-62F6-4F3E-A73D-ACBFD60B97AC"), Name = "Admin", NormalizedName = "ADMIN" };
            var userRole = new IdentityUserRole<Guid>()
            {
                RoleId = role.Id,
                UserId = user.Id
            };

            using (var arrangedContext = new InsightHubContext(options))
            {
                await arrangedContext.Users.AddAsync(user);
                await arrangedContext.Roles.AddAsync(role);
                await arrangedContext.UserRoles.AddAsync(userRole);
                await arrangedContext.SaveChangesAsync();
            }

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.DisableAsync(user.Id));
        }
    }
}
