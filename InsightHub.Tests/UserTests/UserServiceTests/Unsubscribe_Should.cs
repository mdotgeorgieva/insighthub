﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class Unsubscribe_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_UnsubParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_UnsubParamsAreValid));
            var mockEmailService = new Mock<IEmailService>().Object;

            Utils.Seed(options);
            var sub = new UserIndustry
            {
                ClientId = Utils.client.Id,
                IndustryId = Utils.industry.Id
            };

            using(var testContext = new InsightHubContext(options))
            {
                await testContext.UserIndustries.AddAsync(sub);
                await testContext.SaveChangesAsync();
            }

            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.Unsubscribe(Utils.user2.Id, Utils.industry.Name);

            Assert.IsTrue(result);
            Assert.AreEqual(0, assertContext.UserIndustries.Count());
        }

        [TestMethod]
        public async Task ThrowWhen_UnsubUserIdIsEmpty()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_UnsubUserIdIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;
            var industryName = "test industry";

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Unsubscribe(Guid.Empty, industryName));
        }

        [TestMethod]
        public async Task ThrowWhen_UnsubUserDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_UnsubUserDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;
            var industryName = "test industry";

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Unsubscribe(Guid.NewGuid(), industryName));
        }

        [TestMethod]
        public async Task ThrowWhen_UnsubIndustryIsNull()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_UnsubIndustryIsNull));
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.Seed(options);

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Unsubscribe(Utils.user2.Id, null));
        }

        [TestMethod]
        public async Task ThrowWhen_UnsubIndustryDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_UnsubIndustryDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.Seed(options);

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Unsubscribe(Utils.user2.Id, "no name"));
        }

        [TestMethod]
        public async Task ThrowWhen_UnsubSubDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_UnsubSubDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.Seed(options);

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Unsubscribe(Utils.user2.Id, Utils.industry.Name));
        }
    }
}
