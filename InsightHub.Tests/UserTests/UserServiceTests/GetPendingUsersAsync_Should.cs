﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class GetPendingUsersAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_AllUsersAreActive()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_AllUsersAreActive));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetPendingUsersAsync();

            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneUserIsNotApproved()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneUserIsNotApproved));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            var testUser = new User
            {
                Id = Guid.NewGuid(),
                UserName = "testuser@ih.com",
                FirstName = "Johnny",
                LastName = "Smith",
                Fullname = "Johnny Smith",
                PhoneNumber = "+1666777666",
                NormalizedUserName = "TESTUSER@IH.COM",
                Email = "testuser@ih.com",
                NormalizedEmail = "TESTUSER@IH.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = false,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
                EmailConfirmed = true,
            };

            using (var testContext = new InsightHubContext(options))
            {
                await testContext.Users.AddAsync(testUser);
                await testContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetPendingUsersAsync();
            var resultList = result.ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(testUser.Id, resultList[0].Id);
            Assert.IsInstanceOfType(result, typeof(ICollection<UserDTO>));
        }
    }
}
