﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class CheckUser_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_CheckedUserIsActive()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_CheckedUserIsActive));
            var mockEmailService = new Mock<IEmailService>().Object;

            var user = new User()
            {
                Id = Guid.NewGuid(),
                UserName = "user1@ih.com",
                FirstName = "John",
                LastName = "Smith",
                Fullname = "John Smith",
                PhoneNumber = "+1666777666",
                NormalizedUserName = "USER1@IH.COM",
                Email = "user1@ih.com",
                NormalizedEmail = "USER1@IH.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = false,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
                EmailConfirmed = true,
                IsApproved = true
            };
            var hasher = new PasswordHasher<User>();

            user.PasswordHash = hasher.HashPassword(user, "user");

            using (var arrangedContext = new InsightHubContext(options))
            {
                await arrangedContext.Users.AddAsync(user);
                await arrangedContext.SaveChangesAsync();
            }

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.CheckUser(user.Email);

            Assert.AreEqual("ok", result);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_CheckedUserDoesNotExist()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_CheckedUserDoesNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;
            var testMail = "testmail@ih.com";
            var user = new User()
            {
                Id = Guid.NewGuid(),
                UserName = "user1@ih.com",
                FirstName = "John",
                LastName = "Smith",
                Fullname = "John Smith",
                PhoneNumber = "+1666777666",
                NormalizedUserName = "USER1@IH.COM",
                Email = "user1@ih.com",
                NormalizedEmail = "USER1@IH.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = false,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
                EmailConfirmed = true,
                IsApproved = true
            };
            var hasher = new PasswordHasher<User>();

            user.PasswordHash = hasher.HashPassword(user, "user");

            using (var arrangedContext = new InsightHubContext(options))
            {
                await arrangedContext.Users.AddAsync(user);
                await arrangedContext.SaveChangesAsync();
            }

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.CheckUser(testMail);

            Assert.AreEqual("unexisting", result);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_CheckedUserIsNotApproved()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_CheckedUserIsNotApproved));
            var mockEmailService = new Mock<IEmailService>().Object;

            var user = new User()
            {
                Id = Guid.NewGuid(),
                UserName = "user1@ih.com",
                FirstName = "John",
                LastName = "Smith",
                Fullname = "John Smith",
                PhoneNumber = "+1666777666",
                NormalizedUserName = "USER1@IH.COM",
                Email = "user1@ih.com",
                NormalizedEmail = "USER1@IH.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = false,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
                EmailConfirmed = true,
                IsApproved = false
            };
            var hasher = new PasswordHasher<User>();

            user.PasswordHash = hasher.HashPassword(user, "user");

            using (var arrangedContext = new InsightHubContext(options))
            {
                await arrangedContext.Users.AddAsync(user);
                await arrangedContext.SaveChangesAsync();
            }

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.CheckUser(user.Email);

            Assert.AreEqual("notApproved", result);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_CheckedUserIsDeleted()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_CheckedUserIsDeleted));
            var mockEmailService = new Mock<IEmailService>().Object;

            var user = new User()
            {
                Id = Guid.NewGuid(),
                UserName = "user1@ih.com",
                FirstName = "John",
                LastName = "Smith",
                Fullname = "John Smith",
                PhoneNumber = "+1666777666",
                NormalizedUserName = "USER1@IH.COM",
                Email = "user1@ih.com",
                NormalizedEmail = "USER1@IH.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = false,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
                EmailConfirmed = true,
                IsApproved = true,
                IsDeleted = true
            };
            var hasher = new PasswordHasher<User>();

            user.PasswordHash = hasher.HashPassword(user, "user");

            using (var arrangedContext = new InsightHubContext(options))
            {
                await arrangedContext.Users.AddAsync(user);
                await arrangedContext.SaveChangesAsync();
            }

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.CheckUser(user.Email);

            Assert.AreEqual("deleted", result);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_CheckedUserIsDisabled()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_CheckedUserIsDisabled));
            var mockEmailService = new Mock<IEmailService>().Object;

            var user = new User()
            {
                Id = Guid.NewGuid(),
                UserName = "user1@ih.com",
                FirstName = "John",
                LastName = "Smith",
                Fullname = "John Smith",
                PhoneNumber = "+1666777666",
                NormalizedUserName = "USER1@IH.COM",
                Email = "user1@ih.com",
                NormalizedEmail = "USER1@IH.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = false,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
                EmailConfirmed = true,
                IsApproved = true,
                IsDisabled = true
            };
            var hasher = new PasswordHasher<User>();

            user.PasswordHash = hasher.HashPassword(user, "user");

            using (var arrangedContext = new InsightHubContext(options))
            {
                await arrangedContext.Users.AddAsync(user);
                await arrangedContext.SaveChangesAsync();
            }

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.CheckUser(user.Email);

            Assert.AreEqual("disabled", result);
        }

        [TestMethod]
        public async Task ThrowWhen_CheckUserEmailIsNull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_CheckUserEmailIsNull));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CheckUser(null));
        }
    }
}
