﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class GetHomepageAuthorsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_AllAuthorsForHomeAreActive()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_AllAuthorsForHomeAreActive));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetHomepageAuthorsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(resultList[0].Id, Utils.author1.Id);
            Assert.AreEqual(resultList[1].Id, Utils.author.Id);
            Assert.IsInstanceOfType(result, typeof(ICollection<AuthorDTO>));
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneForHomeIsNotApproved()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneForHomeIsNotApproved));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user1.IsApproved = false;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user1);
                await testContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetHomepageAuthorsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(Utils.author.Id, resultList[0].Id);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneForHomeIsDisabled()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneForHomeIsDisabled));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user1.IsDisabled = true;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user1);
                await testContext.SaveChangesAsync();
            }
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetHomepageAuthorsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(Utils.author.Id, resultList[0].Id);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneForHomeIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneForHomeIsDeleted));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user1.IsDeleted = true;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user1);
                await testContext.SaveChangesAsync();
            }
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetHomepageAuthorsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(Utils.author.Id, resultList[0].Id);
        }
    }
}
