﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
   public class Subscribe_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_SubParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_SubParamsAreValid));
            var mockEmailService = new Mock<IEmailService>().Object;

            Utils.Seed(options);

            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.Subscribe(Utils.user2.Id, Utils.industry.Name);

            Assert.IsTrue(result);
            Assert.AreEqual(1, assertContext.UserIndustries.Count());
        }

        [TestMethod]
        public async Task ThrowWhen_SubUserIdIsEmpty()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_SubUserIdIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;
            var industryName = "test industry";

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Subscribe(Guid.Empty, industryName));
        }

        [TestMethod]
        public async Task ThrowWhen_SubUserDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_SubUserDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;
            var industryName = "test industry";

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Subscribe(Guid.NewGuid(), industryName));
        }

        [TestMethod]
        public async Task ThrowWhen_SubIndustryIsNull()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_SubIndustryIsNull));
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.Seed(options);

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Subscribe(Utils.user2.Id, null));
        }

        [TestMethod]
        public async Task ThrowWhen_SubIndustryDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_SubIndustryDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.Seed(options);

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Subscribe(Utils.user2.Id, "no name"));
        }
    }
}
