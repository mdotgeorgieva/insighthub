﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class ApproveReportAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_ApproveReportParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_ApproveReportParamsAreValid));
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.Seed(options);

            Utils.report.IsApproved = false;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.report);
                await testContext.SaveChangesAsync();
            }

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.ApproveReportAsync(Utils.report.Id);
            var updatedReport = await assertContext.Reports.FirstOrDefaultAsync(r => r.Id == Utils.report.Id);

            Assert.IsTrue(result);
            Assert.IsTrue(updatedReport.IsApproved);
        }

        [TestMethod]
        public async Task ThrowWhen_ApproveReportIdIsEmpty()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ApproveReportIdIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.ApproveReportAsync(Guid.Empty));
        }

        [TestMethod]
        public async Task ThrowWhen_ApproveReportDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ApproveReportDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.ApproveReportAsync(Guid.NewGuid()));
        }
    }
}
