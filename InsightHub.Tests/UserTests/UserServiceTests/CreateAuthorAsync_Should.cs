﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class CreateAuthorAsync_Should
    {
        [TestMethod]
        public async Task ReturnTrueWhen_CreateAuthorParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnTrueWhen_CreateAuthorParamsAreValid));
			var mockEmailService = new Mock<IEmailService>().Object;

			string bio = "Test bio";
			var user = new User()
			{
				Id = Guid.NewGuid(),
				UserName = "user1@ih.com",
				FirstName = "John",
				LastName = "Smith",
				Fullname = "John Smith",
				PhoneNumber = "+1666777666",
				NormalizedUserName = "USER1@IH.COM",
				Email = "user1@ih.com",
				NormalizedEmail = "USER1@IH.COM",
				CreatedOn = DateTime.UtcNow,
				LockoutEnabled = false,
				SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
				EmailConfirmed = true,
				IsApproved = true
			};
			var hasher = new PasswordHasher<User>();

			user.PasswordHash = hasher.HashPassword(user, "user");

			using (var arrangedContext = new InsightHubContext(options))
			{
				await arrangedContext.Users.AddAsync(user);
				await arrangedContext.SaveChangesAsync();
			}

			//Act&Assert

			using var assertContext = new InsightHubContext(options);
			var sut = new UserService(assertContext, mockEmailService);

			var result = await sut.CreateAuthorAsync(user.Id, bio);

			Assert.IsTrue(result);
			Assert.AreEqual(1, assertContext.Authors.Count());
		}

		[TestMethod]

		public async Task ReturnFalseWhen_CreateAuthorUserIdIsWrong()
		{
			var options = Utils.GetOptions(nameof(ReturnFalseWhen_CreateAuthorUserIdIsWrong));
			var mockEmailService = new Mock<IEmailService>().Object;

			string bio = "Test bio";
			var user = new User()
			{
				Id = Guid.NewGuid(),
				UserName = "user1@ih.com",
				FirstName = "John",
				LastName = "Smith",
				Fullname = "John Smith",
				PhoneNumber = "+1666777666",
				NormalizedUserName = "USER1@IH.COM",
				Email = "user1@ih.com",
				NormalizedEmail = "USER1@IH.COM",
				CreatedOn = DateTime.UtcNow,
				LockoutEnabled = false,
				SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
				EmailConfirmed = true,
				IsApproved = true
			};
			var hasher = new PasswordHasher<User>();

			user.PasswordHash = hasher.HashPassword(user, "user");

			using (var arrangedContext = new InsightHubContext(options))
			{
				await arrangedContext.Users.AddAsync(user);
				await arrangedContext.SaveChangesAsync();
			}

			//Act&Assert

			using var assertContext = new InsightHubContext(options);
			var sut = new UserService(assertContext, mockEmailService);

			var result = await sut.CreateAuthorAsync(Guid.NewGuid(), bio);

			Assert.IsFalse(result);
			Assert.AreEqual(0, assertContext.Authors.Count());
		}

		[TestMethod]

		public async Task ThrowWhen_CreateAuthorUserIdIsEmpty()
		{
			var options = Utils.GetOptions(nameof(ThrowWhen_CreateAuthorUserIdIsEmpty));
			var mockEmailService = new Mock<IEmailService>().Object;
			string bio = "Test bio";

			using var assertContext = new InsightHubContext(options);
			var sut = new UserService(assertContext, mockEmailService);


			await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAuthorAsync(Guid.Empty, bio));
			Assert.AreEqual(0, assertContext.Authors.Count());
		}
	}
}
