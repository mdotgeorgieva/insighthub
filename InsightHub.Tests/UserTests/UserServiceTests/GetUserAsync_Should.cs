﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class GetUserAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_GetUserParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_GetUserParamsAreValid));
            Utils.Seed(options);
            var mockEmailService = new Mock<EmailService>().Object;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetUserAsync(Utils.user.Id);

            Assert.AreEqual("user1@ih.com", result.Username);
            Assert.AreEqual("John", result.FirstName);
            Assert.IsInstanceOfType(result, typeof(UserDTO));
        }

        [TestMethod]
        public async Task ThrowWhen_GetUserIdIsEmpty()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_GetUserIdIsEmpty));
            var mockEmailService = new Mock<EmailService>().Object;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetUserAsync(Guid.Empty));
        }

        [TestMethod]
        public async Task ThrowWhen_GetUserIdIsWrong()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_GetUserIdIsWrong));
            var mockEmailService = new Mock<EmailService>().Object;
            Utils.Seed(options);


            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetUserAsync(Guid.NewGuid()));
        }
    }
}
