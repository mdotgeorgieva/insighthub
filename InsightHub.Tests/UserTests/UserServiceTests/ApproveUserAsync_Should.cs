﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class ApproveUserAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_ApproveUserParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_ApproveUserParamsAreValid));
            var mockEmailService = new Mock<IEmailService>().Object;

            var user = new User()
            {
                Id = Guid.NewGuid(),
                UserName = "user1@ih.com",
                FirstName = "John",
                LastName = "Smith",
                Fullname = "John Smith",
                PhoneNumber = "+1666777666",
                NormalizedUserName = "USER1@IH.COM",
                Email = "user1@ih.com",
                NormalizedEmail = "USER1@IH.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = true,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
                EmailConfirmed = true,
                IsApproved = false,
            };
            var hasher = new PasswordHasher<User>();

            user.PasswordHash = hasher.HashPassword(user, "user");

            using (var arrangedContext = new InsightHubContext(options))
            {
                await arrangedContext.Users.AddAsync(user);
                await arrangedContext.SaveChangesAsync();
            }

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.ApproveUserAsync(user.Id);
            var updatedUser = await assertContext.Users.FirstOrDefaultAsync(u => u.Id == user.Id);

            Assert.IsTrue(result);
            Assert.IsTrue(updatedUser.IsApproved);
        }

        [TestMethod]
        public async Task ThrowWhen_ApproveUserIdIsEmpty()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ApproveUserIdIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.ApproveUserAsync(Guid.Empty));
        }

        [TestMethod]
        public async Task ThrowWhen_ApproveUserDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ApproveUserDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.ApproveUserAsync(Guid.NewGuid()));
        }
    }
}