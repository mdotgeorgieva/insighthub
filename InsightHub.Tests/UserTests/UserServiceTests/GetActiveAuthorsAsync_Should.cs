﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class GetActiveAuthorsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_AllAuthorsAreActive()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_AllAuthorsAreActive));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetActiveAuthorsAsync();

            Assert.AreEqual(2, result.Count);
            Assert.IsInstanceOfType(result, typeof(ICollection<UserDTO>));
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneIsNotApproved()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneIsNotApproved));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user1.IsApproved = false;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user1);
                await testContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetActiveAuthorsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(Utils.user.Id, resultList[0].Id);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneIsDisabled()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneIsDisabled));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user1.IsDisabled = true;

            using(var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user1);
                await testContext.SaveChangesAsync();
            }
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetActiveAuthorsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(Utils.user.Id, resultList[0].Id);
        }

        [TestMethod]
        public async Task ReturnCorrectWhen_OneIsDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_OneIsDeleted));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user1.IsDeleted = true;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user1);
                await testContext.SaveChangesAsync();
            }
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetActiveAuthorsAsync();
            var resultList = result.ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(Utils.user.Id, resultList[0].Id);
        }
    }
}
