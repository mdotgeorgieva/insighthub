﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class RefusePendingUserAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_RefuseUserParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_RefuseUserParamsAreValid));
            var mockEmailService = new Mock<IEmailService>().Object;

            Utils.Seed(options);
            Utils.user.IsApproved = false;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user);
                await testContext.SaveChangesAsync();
            }

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.RefusePendingUserAsync(Utils.user.Id);
            var updatedUser = await assertContext.Users.FirstOrDefaultAsync(u => u.Id == Utils.user.Id);

            Assert.IsTrue(result);
            Assert.IsTrue(updatedUser.IsDeleted);
        }

        [TestMethod]
        public async Task ThrowWhen_RefuseUserIdIsEmpty()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_RefuseUserIdIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.RefusePendingUserAsync(Guid.Empty));
        }

        [TestMethod]
        public async Task ThrowWhen_RefuseUserDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_RefuseUserDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.RefusePendingUserAsync(Guid.NewGuid()));
        }
    }
}
