﻿using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class GetAuthorInfoAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_AllAuthorsForInfoAreActive()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectWhen_AllAuthorsForInfoAreActive));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.GetAuthorInfoAsync(Utils.author.Id);

            Assert.AreEqual(result[1], Utils.author.Bio);
            Assert.IsInstanceOfType(result, typeof(string[]));
        }

        [TestMethod]
        public async Task ThrowWhen_AuthorForInfoIdIsEmpty()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_AuthorForInfoIdIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAuthorInfoAsync(Guid.Empty));
        }

        [TestMethod]
        public async Task ThrowWhen_AuthorForInfoIdIsWrong()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_AuthorForInfoIdIsWrong));
            var mockEmailService = new Mock<IEmailService>().Object;
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAuthorInfoAsync(Guid.NewGuid()));
        }

        [TestMethod]
        public async Task ThrowWhen_AuthorForInfoNotApproved()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_AuthorForInfoNotApproved));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user.IsApproved = false;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user);
                await testContext.SaveChangesAsync();
            }

            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAuthorInfoAsync(Utils.author.Id));
        }

        [TestMethod]
        public async Task ThrowWhen_AuthorForInfoIsDisabled()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_AuthorForInfoIsDisabled));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user.IsDisabled = true;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user);
                await testContext.SaveChangesAsync();
            }
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAuthorInfoAsync(Utils.author.Id));
        }

        [TestMethod]
        public async Task ThrowWhen_AuthorForInfoDeleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowWhen_AuthorForInfoDeleted));
            Utils.Seed(options);
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.user.IsDeleted = true;

            using (var testContext = new InsightHubContext(options))
            {
                testContext.Update(Utils.user);
                await testContext.SaveChangesAsync();
            }
            //Act, Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAuthorInfoAsync(Utils.author.Id));
        }
    }
}
