using InsightHub.Data;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.UserTests.UserServiceTests
{
	[TestClass]
    public class Download_Should
    {
        [TestMethod]
        public async Task ReturnTrueWhen_DownloadIsUnique()
        {
            var options = Utils.GetOptions(nameof(ReturnTrueWhen_DownloadIsUnique));
            var mockEmailService = new Mock<IEmailService>().Object;

            Utils.Seed(options);

            using (var testContext = new InsightHubContext(options))
            {
                testContext.ClientReports.Remove(Utils.download);
                await testContext.SaveChangesAsync();
            }

            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.DownloadAsync(Utils.user2.Id, Utils.report.Id);

            Assert.IsTrue(result);
            Assert.AreEqual(1, assertContext.ClientReports.Count());
        }

        [TestMethod]
        public async Task ReturnFalseWhen_DownloadIsNotUnique()
        {
            var options = Utils.GetOptions(nameof(ReturnFalseWhen_DownloadIsNotUnique));
            var mockEmailService = new Mock<IEmailService>().Object;

            Utils.Seed(options);

            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            var result = await sut.DownloadAsync(Utils.user2.Id, Utils.report.Id);

            Assert.IsFalse(result);
            Assert.AreEqual(1, assertContext.ClientReports.Count());
        }

        [TestMethod]
        public async Task ThrowWhen_DownloadUserIdIsEmpty()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_DownloadUserIdIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DownloadAsync(Guid.Empty, Guid.NewGuid()));
        }

        [TestMethod]
        public async Task ThrowWhen_DownloadUserDoenNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_DownloadUserDoenNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DownloadAsync(Guid.NewGuid(), Guid.NewGuid()));
        }

        [TestMethod]
        public async Task ThrowWhen_DownloadReportIdIsEmpty()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_DownloadReportIdIsEmpty));
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.Seed(options);

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DownloadAsync(Utils.user2.Id, Guid.Empty));
        }

        [TestMethod]
        public async Task ThrowWhen_DownloadReportDoesNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_DownloadReportDoesNotExist));
            var mockEmailService = new Mock<IEmailService>().Object;
            Utils.Seed(options);

            //Act&Assert
            using var assertContext = new InsightHubContext(options);
            var sut = new UserService(assertContext, mockEmailService);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DownloadAsync(Utils.user2.Id, Guid.NewGuid()));
        }
    }
}
