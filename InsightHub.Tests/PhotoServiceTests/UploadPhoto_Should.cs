﻿using InsightHub.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.IO;
using System.Threading.Tasks;

namespace InsightHub.Tests.PhotoServiceTests
{
	[TestClass]
    public class UploadPhoto_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhen_UploadPhotoParamsAreValid()
        {
            //Arrange
            var mockConfigurations = new Mock<IConfiguration>().Object;
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "test.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);

            string testName = "test";

            //Act & Assert
            var sut = new PhotoService(mockConfigurations);
            var result = await sut.UploadPhoto(fileMock.Object, testName);

            Assert.IsTrue(result);
        }

        //[TestMethod]
        //public async Task ThrowWhen_UploadImgIsNull()
        //{
        //    //Arrange
        //    var mockConfigurations = new Mock<IConfiguration>().Object;
        //    string testName = "test";

        //    //Act & Assert
        //    var sut = new PhotoService(mockConfigurations);

        //    await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UploadPhoto(string.Empty, testName));
        //}

        [TestMethod]
        public async Task ThrowWhen_UploadFilePathIsNull()
        {
            //Arrange
            var mockConfigurations = new Mock<IConfiguration>().Object;
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var fileName = "test.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);


            //Act & Assert
            var sut = new PhotoService(mockConfigurations);

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UploadPhoto(fileMock.Object, null));
        }
    }
}
