﻿using InsightHub.Data;
using InsightHub.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace InsightHub.Tests
{
	public static class Utils
	{
		public static Report report;
		public static Report report1;
		public static Report report2;
		public static Report report3;
		public static Industry industry;
		public static Industry industry1;
		public static Author author;
		public static Author author1;
		public static Client client;
		public static Tag tag;
		public static Tag tag1;
		public static User user;
		public static User user1;
		public static User user2;
		public static ClientReports download;

        public static DbContextOptions<InsightHubContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<InsightHubContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }

        public async static void Seed(DbContextOptions<InsightHubContext> options)
        {
			user = new User()
			{
				Id = Guid.NewGuid(),
				UserName = "user1@ih.com",
				FirstName = "John",
				LastName = "Smith",
				Fullname= "John Smith",
				PhoneNumber = "+1666777666",
				NormalizedUserName = "USER1@IH.COM",
				Email = "user1@ih.com",
				NormalizedEmail = "USER1@IH.COM",
				CreatedOn = DateTime.UtcNow,
				LockoutEnabled = false,
				SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
				EmailConfirmed = true,
				IsApproved = true
			};
			var hasher = new PasswordHasher<User>();

			user.PasswordHash = hasher.HashPassword(user, "user");

			user1 = new User()
			{
				Id = Guid.NewGuid(),
				UserName = "user1@ih.com",
				FirstName = "Jack",
				LastName = "Williams",
				Fullname = "Jack Williams",
				PhoneNumber = "+1666777666",
				NormalizedUserName = "USER1@IH.COM",
				Email = "user1@ih.com",
				NormalizedEmail = "USER1@IH.COM",
				CreatedOn = DateTime.UtcNow,
				LockoutEnabled = false,
				SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGWN",
				EmailConfirmed = true,
				IsApproved = true
			};

			user1.PasswordHash = hasher.HashPassword(user1, "user1");

			user2 = new User()
			{
				Id = Guid.NewGuid(),
				UserName = "user2@ih.com",
				FirstName = "Josh",
				LastName = "Costa",
				Fullname = "Josh Costa",
				PhoneNumber = "+1666777666",
				NormalizedUserName = "USER2@IH.COM",
				Email = "user2@ih.com",
				NormalizedEmail = "USER2@IH.COM",
				CreatedOn = DateTime.UtcNow,
				LockoutEnabled = false,
				SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVZBHGWN",
				EmailConfirmed = true,
				IsApproved = true
			};

			user2.PasswordHash = hasher.HashPassword(user1, "user2");

			client = new Client
			{
				Id = Guid.NewGuid(),
				UserId = user2.Id
			};

	

			industry = new Industry
			{
				Id = Guid.NewGuid(),
				Name = "test industry",
				NormalizedName = "TEST INDUSTRY"
			};
			industry1 = new Industry
			{
				Id = Guid.NewGuid(),
				Name = "test industry 1",
				NormalizedName = "TEST INDUSTRY 1"

			};
			author = new Author
			{
				Id = Guid.NewGuid(),
				UserId = user.Id,
				Bio = "test bio"
			};
			author1 = new Author
			{
				Id = Guid.NewGuid(),
				UserId = user1.Id
			};
			report = new Report
			{
				Id = Guid.NewGuid(),
				Name = "test report",
				NormalizedName = "TEST REPORT",
				AuthorId = author.Id,
				IndustryId = industry.Id,
				CreatedOn = DateTime.Parse("Jan 1, 1999"),
				IsApproved = true,
				Downloads = new List<ClientReports>()
			};
			report1 = new Report
			{
				Id = Guid.NewGuid(),
				Name = "test report 1",
				NormalizedName = "TEST REPORT 1",
				AuthorId = author1.Id,
				IndustryId = industry1.Id,
				CreatedOn = DateTime.Parse("Sep 11, 2009"),
				IsApproved = true,
				IsFeatured=true,
				Downloads = new List<ClientReports>()

			};
			report2 = new Report
			{
				Id = Guid.NewGuid(),
				Name = "test report 2",
				NormalizedName = "TEST REPORT 2",
				AuthorId = author1.Id,
				IndustryId = industry1.Id,
				CreatedOn = DateTime.Parse("Feb 2, 1989"),
				IsApproved = false,
				Downloads = new List<ClientReports>()

			};
			report3 = new Report
			{
				Id = Guid.NewGuid(),
				Name = "test report 3",
				NormalizedName = "TEST REPORT 3",
				AuthorId = author1.Id,
				IndustryId = industry1.Id,
				CreatedOn = DateTime.Parse("Jul 24, 2005"),
				IsApproved = true,
				IsFeatured = true,
				IsDeleted = true,
				Downloads = new List<ClientReports>()

			};
			download = new ClientReports
			{
				ReportId = report.Id,
				UserId = client.Id
			};
			tag = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "Tag",
				NormalizedName = "TAG"

			};
			tag1 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "Tag1",
				NormalizedName = "TAG1"
			};
			var reportTag = new ReportTags
			{
				ReportId = report.Id,
				TagId = tag.Id
			};
			var reportTag1 = new ReportTags
			{
				ReportId = report1.Id,
				TagId = tag1.Id
			};
			var reportTag2 = new ReportTags
			{
				ReportId = report2.Id,
				TagId = tag1.Id
			};
			using (var arrangeContext = new InsightHubContext(options))
			{
				await arrangeContext.Industries.AddRangeAsync(industry,industry1);
				await arrangeContext.Tags.AddRangeAsync(tag, tag1);
				await arrangeContext.Users.AddRangeAsync(user,user1,user2);
				await arrangeContext.Clients.AddRangeAsync(client);
				await arrangeContext.Authors.AddRangeAsync(author,author1);
				await arrangeContext.Reports.AddRangeAsync(report,report1,report2,report3);
				await arrangeContext.ClientReports.AddRangeAsync(download);
				await arrangeContext.ReportTags.AddRangeAsync(reportTag, reportTag1, reportTag2);
				await arrangeContext.SaveChangesAsync();
			}
		}
    }
}
