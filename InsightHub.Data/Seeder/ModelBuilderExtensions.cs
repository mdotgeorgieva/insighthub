﻿using InsightHub.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;

namespace InsightHub.Data.Seeder
{
	public static class ModelBuilderExtensions
	{
		public static void Seed(this ModelBuilder builder)
		{
			builder.Entity<Role>().HasData(
			  new Role { Id = Guid.Parse("297D06E6-C058-486F-A18A-06A971EBFCD7"), Name = "Admin", NormalizedName = "ADMIN" },
			  new Role { Id = Guid.Parse("6C8FCD7E-62F6-4F3E-A73D-ACBFD60B97AB"), Name = "Author", NormalizedName = "AUTHOR" },
			  new Role { Id = Guid.Parse("7f66990c-36bc-4381-9f81-32e06e168319"), Name = "Client", NormalizedName = "CLIENT" }

				);

			var managerUser = new User()
			{
				Id = Guid.Parse("7BD06FE6-79CA-43A1-862B-446A1466BB93"),
				UserName = "admin@ih.com",
				FirstName = "John",
				LastName = "Smith",
				Fullname = "John Smith",
				PhoneNumber = "+1666777666",
				NormalizedUserName = "ADMIN@IH.COM",
				Email = "admin@ih.com",
				NormalizedEmail = "ADMIN@IH.COM",
				CreatedOn = DateTime.UtcNow,
				LockoutEnabled = false,
				SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
				EmailConfirmed = true,
				IsApproved = true
			};
			var user1 = new User()
			{
				Id = Guid.Parse("7BD06FE6-79CA-43A1-862B-446A1466BB94"),
				UserName = "jim@ih.com",
				FirstName = "Jim",
				LastName = "Doe",
				Fullname = "Jim Doe",
				PhoneNumber = "+1666777666",
				NormalizedUserName = "JIM@IH.COM",
				Email = "jim@ih.com",
				NormalizedEmail = "JIM@IH.COM",
				CreatedOn = DateTime.UtcNow,
				LockoutEnabled = true,
				SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVZBHGXN",
				EmailConfirmed = true,
				IsApproved = true
			};
			var user2 = new User()
			{
				Id = Guid.Parse("7BD06FE6-79CA-43A1-862B-446A1466BB95"),
				UserName = "rose@ih.com",
				FirstName = "Rose",
				LastName = "James",
				Fullname = "Rose James",
				PhoneNumber = "+1666777666",
				NormalizedUserName = "ROSE@IH.COM",
				Email = "rose@ih.com",
				NormalizedEmail = "ROSE@IH.COM",
				CreatedOn = DateTime.UtcNow,
				LockoutEnabled = true,
				SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVQBHGXN",
				EmailConfirmed = true,
				IsApproved = true
			};
			var user3 = new User()
			{
				Id = Guid.Parse("7BD06FE6-79CA-43A1-862B-446A1466BB96"),
				UserName = "cate@ih.com",
				FirstName = "Cate",
				LastName = "Williams",
				Fullname = "Cate Williams",
				PhoneNumber = "+1666777666",
				NormalizedUserName = "CATE@IH.COM",
				Email = "cate@ih.com",
				NormalizedEmail = "CATE@IH.COM",
				CreatedOn = DateTime.UtcNow,
				LockoutEnabled = true,
				SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5MVQBHGXN",
				EmailConfirmed = true,
				IsApproved = true
			};
			var user4 = new User()
			{
				Id = Guid.Parse("7BD06FE6-79CA-43A1-862B-446A1466BB97"),
				UserName = "max@ih.com",
				FirstName = "Max",
				LastName = "Upton",
				Fullname = "Max Upton",
				PhoneNumber = "+1666777666",
				NormalizedUserName = "MAX@IH.COM",
				Email = "max@ih.com",
				NormalizedEmail = "MAX@IH.COM",
				CreatedOn = DateTime.UtcNow,
				LockoutEnabled = true,
				SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5XVQBHGXN",
				EmailConfirmed = true,
				IsApproved = true
			};
			var user5 = new User()
			{
				Id = Guid.Parse("7BD06FE6-79CA-43A1-862B-446A1466BB98"),
				UserName = "david@ih.com",
				FirstName = "David",
				LastName = "Scott",
				Fullname = "David Scott",
				PhoneNumber = "+1666777666",
				NormalizedUserName = "DAVID@IH.COM",
				Email = "david@ih.com",
				NormalizedEmail = "DAVID@IH.COM",
				CreatedOn = DateTime.UtcNow,
				LockoutEnabled = true,
				SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5XVJBHGXN",
				EmailConfirmed = true,
				IsApproved = true
			};
			var hasher = new PasswordHasher<User>();

			managerUser.PasswordHash = hasher.HashPassword(managerUser, "admin1");
			user1.PasswordHash = hasher.HashPassword(user1, "author");
			user2.PasswordHash = hasher.HashPassword(user2, "author");
			user3.PasswordHash = hasher.HashPassword(user3, "author");
			user4.PasswordHash = hasher.HashPassword(user4, "author");
			user5.PasswordHash = hasher.HashPassword(user5, "author");

			builder.Entity<User>().HasData(managerUser, user1, user2, user3, user4, user5);

			builder.Entity<IdentityUserRole<Guid>>().HasData(
				new IdentityUserRole<Guid>()
				{
					RoleId = Guid.Parse("297D06E6-C058-486F-A18A-06A971EBFCD7"),
					UserId = Guid.Parse("7BD06FE6-79CA-43A1-862B-446A1466BB93")
				},
				 new IdentityUserRole<Guid>()
				 {
					 RoleId = Guid.Parse("6C8FCD7E-62F6-4F3E-A73D-ACBFD60B97AB"),
					 UserId = user1.Id
				 },
				 new IdentityUserRole<Guid>()
				 {
					 RoleId = Guid.Parse("6C8FCD7E-62F6-4F3E-A73D-ACBFD60B97AB"),
					 UserId = user2.Id
				 },
				 new IdentityUserRole<Guid>()
				 {
					 RoleId = Guid.Parse("6C8FCD7E-62F6-4F3E-A73D-ACBFD60B97AB"),
					 UserId = user3.Id
				 },
				 new IdentityUserRole<Guid>()
				 {
					 RoleId = Guid.Parse("6C8FCD7E-62F6-4F3E-A73D-ACBFD60B97AB"),
					 UserId = user4.Id
				 },
				 new IdentityUserRole<Guid>()
				 {
					 RoleId = Guid.Parse("6C8FCD7E-62F6-4F3E-A73D-ACBFD60B97AB"),
					 UserId = user5.Id
				 }

			);
			var author = new Author
			{
				Id = Guid.NewGuid(),
				Bio = "Jim Doe has been involved with ICT research in Africa since 1997 and has participated in diverse research projects in 14 African countries. He started his career in 1994 as a public relations officer with Software Technologies Limited (an East African Oracle distributor), before serving as a research analyst/project officer for Telecom Forum Africa in 1997.",
				UserId = user1.Id,
				ImagePath = "3.png",

			};
			var author1 = new Author
			{
				Id = Guid.NewGuid(),
				Bio = "Rosa James has a background in journalism and has been published in various local, regional, and international magazines and newspapers. She has spoken at various industry events across sub-Saharan Africa including countries like Ethiopia, Ghana, Kenya, Nigeria, Rwanda, Tanzania and Zambia as well as leading and facilitating at IDC’s events in the region. Rose is also a published author of a children’s novel.",
				UserId = user2.Id,
				ImagePath = "1.png",

			};
			var author2 = new Author
			{
				Id = Guid.NewGuid(),
				Bio = "Cate Williams has been an analyst InsightHub for several years. Early on as an analyst she covered CRM applications, but more recently her work has been in integration middleware covering markets such as API management, file sync and share, and B2B integration.",
				UserId = user3.Id,
				ImagePath = "2.png",

			};
			var author3 = new Author
			{
				Id = Guid.NewGuid(),
				Bio = "Max Upton has more than 15 years of experience in business research, focusing on financial services and business innovation. His consulting work for InsightHub has allowed him to work closely with leading banks and regulators in the region for their technology and innovation strategies. Mr. Upton is concurrently head of InsightHub's business and operations in Thailand.",
				UserId = user4.Id,
				ImagePath = "7.png",

			};
			var author4 = new Author
			{
				Id = Guid.NewGuid(),
				Bio = "Previously, David Scott served for ten years as Enterprise Architect at CareFirst Blue Cross/Blue Shield, responsible for building business and technology alignment roadmaps for the ACA/HIX Health Reform, Mandated Implementations, PCMH, and Provider Network domains to ensure alignment of annual and tactical IT project planning with business goals.",
				UserId = user5.Id,
				ImagePath = "8.jpg",

			};

			builder.Entity<Author>().HasData(
				author, author1, author2, author3, author4
				);

			var industry = new Industry
			{
				Id = Guid.NewGuid(),
				Name = "Energy",
				NormalizedName = "ENERGY"
			};
			var industry1 = new Industry
			{
				Id = Guid.NewGuid(),
				Name = "Information Technology",
				NormalizedName = "INFORMATION TECHNOLOGY"
			};
			var industry2 = new Industry
			{
				Id = Guid.NewGuid(),
				Name = "Healthcare",
				NormalizedName = "HEALTHCARE"
			};
			var industry3 = new Industry
			{
				Id = Guid.NewGuid(),
				Name = "Finance",
				NormalizedName = "FINANCE"
			};
			var industry4 = new Industry
			{
				Id = Guid.NewGuid(),
				Name = "Marketing",
				NormalizedName = "MARKETING"
			};

			builder.Entity<Industry>().HasData(
			industry, industry1, industry2, industry3, industry4
				);

			var tag = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "computers",
				NormalizedName = "COMPUTERS"
			};
			var tag1 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "software",
				NormalizedName = "SOFTWARE"
			};
			var tag2 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "hardware",
				NormalizedName = "HARDWARE"
			};
			var tag3 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "web",
				NormalizedName = "WEB"
			};
			var tag4 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "medicine",
				NormalizedName = "MEDICINE"
			};
			var tag5 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "research",
				NormalizedName = "RESEARCH"
			};
			var tag6 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "vaccine",
				NormalizedName = "VACCINE"
			};
			var tag7 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "laboratory",
				NormalizedName = "LABORATORY"
			};
			var tag8 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "oil",
				NormalizedName = "OIL"
			};
			var tag9 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "gas",
				NormalizedName = "GAS"
			};
			var tag10 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "solar",
				NormalizedName = "SOLAR"
			};
			var tag11 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "nuclear",
				NormalizedName = "NUCLEAR"
			};
			var tag12 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "stock",
				NormalizedName = "STOCK"
			};
			var tag13 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "investment",
				NormalizedName = "INVESTMENT"
			};
			var tag14 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "insurance",
				NormalizedName = "INSURANCE"
			};
			var tag15 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "profit",
				NormalizedName = "PROFIT"
			};
			var tag16 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "sales",
				NormalizedName = "SALES"
			};
			var tag17 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "trend",
				NormalizedName = "TREND"
			};
			var tag18 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "influence",
				NormalizedName = "INFLUENCE"
			};
			var tag19 = new Tag
			{
				Id = Guid.NewGuid(),
				Name = "hospital",
				NormalizedName = "HOSPITAL"
			};

			builder.Entity<Tag>().HasData(
				tag, tag1, tag2, tag3, tag4, tag5, tag6, tag7, tag8, tag9, tag10, tag11, tag12, tag13, tag14, tag15, tag16, tag17, tag18, tag19
				);
			// ------------------------- HEALTH
			var report = new Report
			{
				Id = Guid.Parse("0070a7dc-c760-4a34-8c87-d4747824191b"),
				Name = "European Value-Based Healthcare Digital Transformation",
				NormalizedName = "European Value-Based Healthcare Digital Transformation".ToUpper(),
				AuthorId = author.Id,
				ImagePath = "0070a7dc-c760-4a34-8c87-d4747824191b.jpg",
				Content = "0070a7dc-c760-4a34-8c87-d4747824191b",
				IndustryId = industry2.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 28),
				Summary = " With the advent of the value-based paradigm, healthcare organizations are today tasked to ensure equitable access and financial sustainability while improving and reducing the variation of clinical outcomes in healthcare systems. Increasing collaboration across the healthcare value chain, adopting a more personalized approach to treatment, championing the patient experience, and adopting outcome-based business models are fast emerging as necessary for the healthcare industry to succeed. The IDC Health Insights: European Value-Based Healthcare Digital Transformation Strategies service focuses on analyzing European healthcare providers, payers, and public health policy makers' digital strategy and best practices in supporting the adoption of value-based healthcare to deliver the maximum value to patients. Value-based healthcare is a key driver of digital transformation as it requires an unprecedented level of availability of patient information. Key solutions as HIE, population health management, smart patient engagement tools, advanced analytics and cognitive, and cloud and mobility will fundamentally impact care providers and payers' value-based healthcare strategies execution and success. Approach. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports.",
			};
			builder.Entity<Report>().HasData(report);
			builder.Entity<ReportTags>().HasData(
				 new ReportTags
				 {
					 ReportId = report.Id,
					 TagId = tag4.Id
				 },
				 new ReportTags
				 {
					 ReportId = report.Id,
					 TagId = tag5.Id
				 }
				);
			var report1 = new Report
			{
				Id = Guid.Parse("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244"),
				Name = "European Digital Hospital Evolution",
				NormalizedName = "European Digital Hospital Evolution".ToUpper(),
				AuthorId = author1.Id,
				ImagePath = "e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244.jpg",
				Content = "e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244",
				IndustryId = industry2.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 26),
				Summary = "European Digital Hospital service provides detailed information about the evolution of digital hospital transformation and technology adoption. It offers valuable insights into IT solution trends within the hospital sector, including healthcare-specific technologies such as electronic health records; admission, discharge, and transfer; digital medical imaging systems; VNA and archiving; and business intelligence and patient management systems. This service focuses on a vital component of healthcare — the digital hospital. Healthcare systems across the world are facing care quality and sustainability challenges that require a new approach to care delivery and reimbursement models. The fully digital hospital is in the center of that journey, being a key enabler of change. The hospital CIOs are facing new technology but, at the same time, a growing legacy IT burden that must be managed alongside of new digital initiatives. In healthcare, management of legacy systems is therefore a growing challenge — when looking at not only obsolete digital applications but also infrastructure and, in the end, how healthcare providers purchase digital applications and platforms. When IT fails to deliver because of legacy and lack of agility, the business model and clinical processes become legacy as well. Key solutions such as EHR, RIS, PACS, VNA, ECM, advanced analytics, cloud, and mobility will fundamentally impact hospitals' digital strategies execution and success.This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports.",
			};
			builder.Entity<Report>().HasData(report1);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report1.Id,
				 TagId = tag5.Id
			 },
			 new ReportTags
			 {
				 ReportId = report1.Id,
				 TagId = tag7.Id
			 }
			);
			var report2 = new Report
			{
				Id = Guid.Parse("c03a5b96-20c4-4bac-9c92-aea551fc9806"),
				Name = "Health Insights: European Life Science and Pharma",
				NormalizedName = "Health Insights: European Life Science and Pharma".ToUpper(),
				AuthorId = author.Id,
				ImagePath = "c03a5b96-20c4-4bac-9c92-aea551fc9806.jpg",
				Content = "c03a5b96-20c4-4bac-9c92-aea551fc9806",
				IndustryId = industry2.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 4, 28),
				Summary = "Digital transformation is key in the European life science industry's ongoing efforts to transform itself as it seeks to find new paths to innovation, optimize operational efficiency and effectiveness, and regain long-term sustainability. The transition toward new care delivery ecosystems and outcome-based reimbursement models enhances life science organizations' needs to implement new approaches to information management and use. The digital mission in the life sciences is to integrate new and existing data, information, and knowledge into a more knowledge-centric approach to new drug and medical devices development, to product commercialization and management, and to treatment delivery. The European life science digital transformation service provides a forward-looking analysis of IT and technologies that are being adopted all along the value chain of the European life science industry. The service focuses on how European life science organizations can leverage the new range of technologies underpinned by the 3rd Platform technologies and innovation accelerators to support efforts to improve the operational efficiency of the industry and better engage with the ultimate end user of life science innovations, namely patients. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports.",
			};
			builder.Entity<Report>().HasData(report2);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report2.Id,
				 TagId = tag5.Id
			 },
			 new ReportTags
			 {
				 ReportId = report2.Id,
				 TagId = tag6.Id
			 }
			);
			var report3 = new Report
			{
				Id = Guid.Parse("698b544c-d07c-41d1-9fad-20dabb68c7b0"),
				Name = "Health systems and universal health coverage",
				NormalizedName = "Health systems and universal health coverage".ToUpper(),
				AuthorId = author1.Id,
				ImagePath = "698b544c-d07c-41d1-9fad-20dabb68c7b0.jpg",
				Content = "698b544c-d07c-41d1-9fad-20dabb68c7b0",
				IndustryId = industry2.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 14),
				Summary = "In the SDG monitoring framework, progress towards universal health coverage (UHC) is tracked with two indicators: (i) a service coverage index (which measures coverage of selected essential health services on a scale of 0 to 100); and (ii) the proportion of the population with large out-of-pocket expenditures on health care (which measures the incidence of catastrophic health spending, rendered as percentage). The service coverage index improved from 45 globally in 2000 to 66 in 2017, with the strongest increase in low-and lower-middle-income countries, where the baseline at 2000 was lowest. However, the pace of that progress has slowed since 2010. The improvements are especially notable for infectious disease interventions and, to a lesser extent, for reproductive, maternal and child health services. Within countries, coverage of the latter services is typically lower in poorer households than in richer households. Overall, between one third and one half the world’s population (33% to 49%) was covered by essential health services in 2017 . Service coverage continued to be lower in low- and middle-income countries than in wealthier ones; the same held for health workforce densities and immunization coverage (Figure 1.2). Available data indicate that over 40% of all countries have fewer than 10 medical doctors per 10 000 people, over 55% have fewer.",
			};
			builder.Entity<Report>().HasData(report3);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report3.Id,
				 TagId = tag4.Id
			 },
			 new ReportTags
			 {
				 ReportId = report3.Id,
				 TagId = tag6.Id
			 }
			);
			var report4 = new Report
			{
				Id = Guid.Parse("180360e0-6db8-412e-a6f4-3d68a939c583"),
				Name = "Investing in strengthening country health information systems",
				NormalizedName = "Investing in strengthening country health information systems".ToUpper(),
				AuthorId = author.Id,
				ImagePath = "180360e0-6db8-412e-a6f4-3d68a939c583.jpg",
				Content = "180360e0-6db8-412e-a6f4-3d68a939c583",
				IndustryId = industry2.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 2, 19),
				Summary = "Accurate, timely, and comparable health-related statistics are essential for understanding population health trends. Decision-makers need the information to develop appropriate policies, allocate resources and prioritize interventions. For almost a fifth of countries, over half of the indicators have no recent primary or direct underlying data. Data gaps and lags prevent from truly understanding who is being included or left aside and take timely and appropriate action. The existing SDG indicators address a broad range of health aspects but do not capture the breadth of population health outcomes and determinants. Monitoring and evaluating population health thus goes beyond the indicators covered in this report and often requires additional and improved measurements. WHO is committed to supporting Member States to make improvements in surveillance and health information systems. These improvements will enhance the scope and quality of health information and standardize processes to generate comparable estimates at the global level. Getting accurate data on COVID-19 related deaths has been a challenge. The COVID-19 pandemic underscores the serious gaps in timely, reliable, accessible and actionable data and measurements that compromise preparedness, prevention and response to health emergencies. The International Health Regulations (IHR) (2005) monitoring framework is one of the data collection tools that have demonstrated value in evaluating and building country capacities to prevent, detect, assess, report and respond to public health emergencies. From self-assessment of the 13 core capacities in 2019, countries have shown steady progress across almost all capacities including surveillance, laboratory and coordination. As the pandemic progresses, objective and comparable data are crucial to determine the effectiveness of different national strategies used to mitigate and suppress, and thus to better prepare for the probable continuation of the epidemic over the next year or more.",
			};
			builder.Entity<Report>().HasData(report4);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report4.Id,
				 TagId = tag4.Id
			 },
			 new ReportTags
			 {
				 ReportId = report4.Id,
				 TagId = tag19.Id
			 }
			);
			var report5 = new Report
			{
				Id = Guid.Parse("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2"),
				Name = "Maternal mortality has declined but progress is uneven across regions",
				NormalizedName = "Maternal mortality has declined but progress is uneven across regions".ToUpper(),
				AuthorId = author1.Id,
				ImagePath = "044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2.jpg",
				Content = "044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2",
				IndustryId = industry2.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 3, 14),
				Summary = "A total of 295 000 [UI 1 80%: 279 000–340 000] women worldwide lost their lives during and following pregnancy and childbirth in 2017, with sub-Saharan Africa and South Asia accounting for approximately 86% of all maternal deaths worldwide. The global maternal mortality ratio (MMR, the number of maternal deaths per 100 000 live births) was estimated at 211 [UI 80%: 199–243], representing a 38% reduction since 2000. On average, global MMR declined by 2.9% every year between 2000 1 UI = uncertainty interval. and 2017. If the pace of progress accelerates enough to achieve the SDG target (reducing global MMR to less than 70 per 100 000 live births), it would save the lives of at least one million women. The majority of maternal deaths are preventable through appropriate management of pregnancy and care at birth, including antenatal care by trained health providers, assistance during childbirth by skilled health personnel, and care and support in the weeks after childbirth. Data from 2014 to 2019 indicate that approximately 81% of all births globally took place in the presence of skilled health personnel, an increase from 64% in the 2000–2006 period. In sub-Saharan Africa, where roughly 66% of the world’s maternal deaths occur, only 60% of births were assisted by skilled health personnel during the 2014–2019 period. ",
			};
			builder.Entity<Report>().HasData(report5);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report5.Id,
				 TagId = tag7.Id
			 },
			 new ReportTags
			 {
				 ReportId = report5.Id,
				 TagId = tag19.Id
			 }
			);
			// ---------------------------- ENERGY

			var report6 = new Report
			{
				Id = Guid.Parse("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0"),
				Name = "North America Utilities Digital Transformation Tactics",
				NormalizedName = "Energy Insights: North America Utilities Digital Transformation Tactics".ToUpper(),
				AuthorId = author2.Id,
				ImagePath = "8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0.jpg",
				Content = "8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0",
				IndustryId = industry.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 3),
				Summary = "Digital transformation is driving utilities to change their business and operating and information models. The Energy Insights: North America Utilities Digital Transformation Strategies service is designed to help North American utility IT and business management understand the disruptions that are transforming the energy and utility value chains and develop the strategies and programs to capitalize on the evolving opportunities. The service provides deep research and insight on the key topics that matter to North American utility decision makers and IT executives affected by the digital transformation. Through an integrated set of deliverables, subscribers gain access to analysis and insights on the impact of new technologies, IT strategies and best practices, and the industry, governmental, and regulatory forces that are shaping the evolution to new industry business models in the industry's generation, transmission and distribution/pipelines, and retail sectors. This service delivers research, insight, and guidance on IT strategy and investments to meet the utility's most critical corporate objectives.",
			};
			builder.Entity<Report>().HasData(report6);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report6.Id,
				 TagId = tag8.Id
			 },
			 new ReportTags
			 {
				 ReportId = report6.Id,
				 TagId = tag9.Id
			 }
			);
			var report7 = new Report
			{
				Id = Guid.Parse("d2b8c016-2bec-4a0a-acbe-c2fe03b55069"),
				Name = "Worldwide Oil and Gas Downstream IT Blueprint",
				NormalizedName = "Worldwide Oil and Gas Downstream IT Blueprint".ToUpper(),
				AuthorId = author.Id,
				ImagePath = "d2b8c016-2bec-4a0a-acbe-c2fe03b55069.jpg",
				Content = "d2b8c016-2bec-4a0a-acbe-c2fe03b55069",
				IndustryId = industry.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 2),
				Summary = "Energy Insights: Worldwide Oil and Gas Downstream IT Strategies advisory research service takes the worldwide oil and gas IT strategies research and applies it to the downstream operations in oil and gas. The research focuses on companies that are involved in the production for sale of all petroleum products, from incoming crude to outgoing product. It will also encompass the technology companies that provide the technology foundation for the operation of the downstream business. Energy Insights: Worldwide Oil and Gas Downstream IT Strategies will focus its analysis on the digital transformation (DX) use cases, priorities, innovation technology, and road map for all aspect of companies that produce and distribute petroleum products. It will provide technology vendors a road map for product development and messaging in oil and gas downstream. Throughout the year, this service will address the following topics: Digital transformation – Investment priorities and road maps for transformation of the downstream business operation Operations technology — IT/OT convergence investments that help downstream organizations transform how they operate the production lines Asset management – Technology changes driving asset management organization and transformation Transformation to scale – Development of a transformed infrastructure to scale to market volatility Our research addresses the following issues that are critical to your success: What is the impact of transformational technologies on the oil and gas downstream DX road map? How does an oil and gas downstream company organize itself around the Future of Work? What are the market trends in oil and gas downstream that affect the use case prioritization for DX? How should trading partners be integrated into the DX road map? Who are the technology vendors that can support DX in oil and gas downstream?",
			};
			builder.Entity<Report>().HasData(report7);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report7.Id,
				 TagId = tag8.Id
			 },
			 new ReportTags
			 {
				 ReportId = report7.Id,
				 TagId = tag10.Id
			 }
			);
			var report8 = new Report
			{
				Id = Guid.Parse("e4b77a8a-ff72-4851-94f5-a1faff00731a"),
				Name = "Global Utilities Customer Experience Scheme",
				NormalizedName = "Global Utilities Customer Experience Scheme".ToUpper(),
				AuthorId = author2.Id,
				ImagePath = "e4b77a8a-ff72-4851-94f5-a1faff00731a.jpg",
				Content = "e4b77a8a-ff72-4851-94f5-a1faff00731a",
				IndustryId = industry.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 1, 14),
				Summary = "With the evolution of energy and water markets and the possibilities offered by digital transformation, utilities are developing new business models and are focusing on providing customers more interactive experiences. Smart metering, analytics, social media, mobility, cloud, and IoT are fundamentally impacting customer operation practices in both competitive and regulated markets. The Energy Insights: Worldwide Utilities Customer Experience Strategies service is designed to help utilities and energy retailers servicing customers in competitive and regulated markets at the worldwide level (including electricity, gas, and water). The service provides exclusive research and direct access to experts providing guidance to make the right IT investments and meet corporate objectives of customer satisfaction, reduction of cost to serve, and innovation. This service develops unique analysis and comprehensive data through Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from Energy Insights. Research reports elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports.",
			};
			builder.Entity<Report>().HasData(report8);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report8.Id,
				 TagId = tag9.Id
			 },
			 new ReportTags
			 {
				 ReportId = report8.Id,
				 TagId = tag11.Id
			 }
			);
			var report9 = new Report
			{
				Id = Guid.Parse("76352896-e547-485f-ae20-73b4f05b664f"),
				Name = "Asia/Pacific Oil and Gas Digital Transformation Procedure",
				NormalizedName = "Asia/Pacific Oil and Gas Digital Transformation Procedure".ToUpper(),
				AuthorId = author3.Id,
				ImagePath = "76352896-e547-485f-ae20-73b4f05b664f.jpg",
				Content = "76352896-e547-485f-ae20-73b4f05b664f",
				IndustryId = industry.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 3, 16),
				Summary = "Asia/Pacific Oil and Gas Digital Transformation Strategies examines the business environment of the oil and gas industry in the Asia/Pacific region, excluding Japan. The service covers the whole value chain from upstream, midstream, and downstream but is focused on upstream. It seeks to support companies working to deliver against efficiency objectives through technology-led change that will deliver improved decision support and insight and flexibility to adjust to changing technology, markets, and political pressures. The Energy Insights: Asia/Pacific Oil and Gas Digital Transformation Strategies program provides advice on best practices and technology priorities that will support technology decision making, particularly, in relation to innovation, data management, and greater integration across silos and processes. The service is designed to support oil and gas companies with market insights and road mapping advice relating to technology decision making, particularly delivering insight to how increased value from investments can be delivered through better understanding of new metric, organization, talent, and process change. Key technology focus areas of the program include IT/OT integration, operational cybersecurity, IoT strategies, and asset management priorities. This service is built on the foundations of extensive engagement and research by the Energy Insights team across the oil and gas industry in this region. Our analysts work with industry experts, staff from the oil and gas business, and technology vendors to ensure that the research service is relevant and prioritizes areas that are of importance to them and the industry at large.",
			};
			builder.Entity<Report>().HasData(report9);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report9.Id,
				 TagId = tag5.Id
			 },
			 new ReportTags
			 {
				 ReportId = report9.Id,
				 TagId = tag10.Id
			 }
			);
			var report10 = new Report
			{
				Id = Guid.Parse("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d"),
				Name = "Worldwide Mining Policy",
				NormalizedName = "Worldwide Mining Policy".ToUpper(),
				AuthorId = author2.Id,
				ImagePath = "a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d.jpg",
				Content = "a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d",
				IndustryId = industry.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 4, 3),
				Summary = "Worldwide Mining Strategies examines the business environment across the mining sector value chain from exploration through to operations, processing, supply chain, and trading globally. Mining companies are operating in an increasingly competitive commodity market environment, driven by pressures relating to accessing funding, assets, and talent. Technology and data are playing an increasingly critical role across the operation to enable decision support, automation, integration, and control. This service provides comprehensive insights into the best practices that show how mining companies are responding and what road maps these companies will need to build the information technology (IT) capabilities required to create integrated, agile, and responsive operations. Mining companies are changing the way they buy technology, how they collaborate to frame the problems to be solved, how they engage with technology suppliers, and how they innovate across their businesses bringing together the capabilities of IT and operational technology (OT). This service tracks the IT investment priorities for organizations seeking to scale value creation across their organization and the impact on decision making and best practices relating to technology, process, and organizational change. This service distills market and industry data into incisive analysis drawn from in-depth interviews with industry experts, mining staff from across the business, and technology vendors. Insight and analysis are further supported and validated through rigorous research methodologies in quantitative market analysis.  Energy Insights' analysts develop unique and comprehensive analyses of this data, focused on providing actionable recommendations. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports.",
			};
			builder.Entity<Report>().HasData(report10);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report10.Id,
				 TagId = tag10.Id
			 },
			 new ReportTags
			 {
				 ReportId = report10.Id,
				 TagId = tag11.Id
			 }
			);
			var report11 = new Report
			{
				Id = Guid.Parse("3d360ea3-0587-4e87-9eeb-07f7a429b6cb"),
				Name = "Worldwide Utilities Connected Asset",
				NormalizedName = "Worldwide Utilities Connected Asset".ToUpper(),
				AuthorId = author4.Id,
				ImagePath = "3d360ea3-0587-4e87-9eeb-07f7a429b6cb.jpg",
				Content = "3d360ea3-0587-4e87-9eeb-07f7a429b6cb",
				IndustryId = industry.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 3, 26),
				Summary = "Energy and digital technologies are radically transforming utilities asset operations and strategies. Renewables and decentralized power generation, energy delivery networks (electricity, gas, and heat), and water and waste water management benefit from the developments and adoption of IoT, AI, and digital twins.  Energy Insights: Worldwide Utilities Connected Asset Strategies service focuses on all these. It provides guidance to end users in terms of digital transformation (DX) use cases road map, analysis of emerging IT trends, and evaluation of technology providers in this space. It looks into the future of work in the utilities value chain context. At the same time, it provides technology vendors a view on utilities' operational excellence asset strategies addressing their go to market.This service develops unique analysis and comprehensive data through  Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from both  and  Energy Insights. Research documents elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research documents.",
			};
			builder.Entity<Report>().HasData(report11);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report11.Id,
				 TagId = tag5.Id
			 },
			 new ReportTags
			 {
				 ReportId = report11.Id,
				 TagId = tag8.Id
			 },
			 new ReportTags
			 {
				 ReportId = report11.Id,
				 TagId = tag9.Id
			 }
			);
			// ---------------------- TECH

			var report12 = new Report
			{
				Id = Guid.Parse("40758cb7-f705-424a-9dac-db2bac62c9ae"),
				Name = "Canadian Internet of Things Ecosystem and Trends",
				NormalizedName = "Canadian Internet of Things Ecosystem and Trends".ToUpper(),
				AuthorId = author.Id,
				ImagePath = "40758cb7-f705-424a-9dac-db2bac62c9ae.jpg",
				Content = "40758cb7-f705-424a-9dac-db2bac62c9ae",
				IndustryId = industry1.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 2, 17),
				Summary = "The Internet of Things (IoT) market is poised for exponential growth over the next several years. However, the IoT ecosystem is a complex segment with multiple layers and hundreds of players, including device and module manufacturers, communication service providers (CSPs), IoT platform players, applications, analytics and security software vendors, and professional services providers. 's Canadian Internet of Things Ecosystem and Trends service analyzes the regionally specific growth of this market from the autonomously connected units in the enterprise world to the applications and services that will demonstrate the power of a world of connected .Markets and Subjects Analyzed Market size and forecast for the Canadian IoT market Canadian IoT revenue forecast by industry Canadian IoT revenue forecast by use cases Taxonomy of the IoT ecosystem: How is the market organized, and what are the key segments and technologies? Canadian-specific demand-side data from various segments (e.g., IT and LOB) Analysis of Canadian-specific vendor readiness and go-to-market strategies for IoT Vertical use cases/case studies of IoT Core Research Taxonomy of the IoT Ecosystem Forecast for the Canadian IoT Market by Industry Forecast for the Canadian IoT Market by Use Cases Profiles of Canadian Vendors Leading IoT Evolution Case Studies of Successful Canadian IoT Implementations Canadian Internet of Things Decision-Maker Survey In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment."

			};
			builder.Entity<Report>().HasData(report12);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report12.Id,
				 TagId = tag.Id
			 },
			 new ReportTags
			 {
				 ReportId = report12.Id,
				 TagId = tag1.Id
			 },
			  new ReportTags
			  {
				  ReportId = report12.Id,
				  TagId = tag3.Id
			  }
			);
			var report13 = new Report
			{
				Id = Guid.Parse("cffed078-aa51-4fe2-913e-59e6e2624828"),
				Name = "Agile Application Life-Cycle, Quality and Portfolio Strategies",
				NormalizedName = "Agile Application Life-Cycle, Quality and Portfolio Strategies".ToUpper(),
				AuthorId = author1.Id,
				ImagePath = "cffed078-aa51-4fe2-913e-59e6e2624828.jpg",
				Content = "cffed078-aa51-4fe2-913e-59e6e2624828",
				IndustryId = industry1.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 4, 29),
				Summary = "Agile Application Life-Cycle, Quality and Portfolio Strategies service provides insight into business and IT alignment through end-to-end application life-cycle management (ALM) and DevOps strategies including agile, IT portfolio management, software quality, testing and verification tools, software change and configuration, and continuous releases for digital transformation. This service provides insights on how product, organizational, and process transitions can help address regulatory compliance, security, and licensing issues covering cloud, SaaS, and open source software. It also analyzes trends related to ALM and software deployment with applications that leverage and target artificial intelligence (AI) and machine learning (ML), collaboration, mobile and embedded apps, IoT, virtualization, and complex sourcing and evolve to adaptive work, project and portfolio management (PPM), and agile adoption with organizational and process strategies (e.g., Scaled Agile Framework [SAFe]). Markets and Subjects Analyzed Software life-cycle process, requirements, configuration management and collaboration, and agile development and projects Software quality, including mobile, cloud, and embedded strategies PPM and work management software trends and strategies IT project and portfolio management software Developer, business, and operations trends Tools enabling DevOps and governance solutions to align complex deployment, financial management, infrastructure, and business priorities Trends in software configuration management (SCM), including connecting business to IT via requirements management and web services demands on agile life-cycle management solutions Automated software quality (ASQ) tools evolution: Hosted testing, web services, API and cloud testing, service virtualization, vulnerability, risk and value-based testing, and other emerging technologies (e.g., mobile, analysis, and metrics) Trends in collaborative software development with social media"
			};
			builder.Entity<Report>().HasData(report13);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report13.Id,
				 TagId = tag.Id
			 },
			 new ReportTags
			 {
				 ReportId = report13.Id,
				 TagId = tag2.Id
			 }
			);
			var report14 = new Report
			{
				Id = Guid.Parse("4a817d53-485b-4634-9c09-22eb56d367fc"),
				Name = "Software Channels and Ecosystems",
				NormalizedName = "Software Channels and Ecosystems".ToUpper(),
				AuthorId = author2.Id,
				ImagePath = "4a817d53-485b-4634-9c09-22eb56d367fc.jpg",
				Content = "4a817d53-485b-4634-9c09-22eb56d367fc",
				IndustryId = industry1.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 6, 2),
				Summary = "Software Channels and Ecosystems research service offers intelligence and expertise to help channel executives and program managers develop, implement, support, and manage effective channel strategies and programs to drive successful relationships with the spectrum of partner activities (e.g., resale, managed/cloud services, consulting professional services, and software development). In addition, it provides a comprehensive view of the value of the indirect software market and its leading vendor proponents. This service also identifies and analyzes key industry trends and their impact on channel relationship drivers and channel partner business models. Subscribers are invited to 's semiannual Software Channel Leadership Council where and channel executives present and discuss key industry issues. Markets and Subjects Analyzed Multinational software vendors Digital transformation in the partner ecosystem Software and SaaS channel revenue flow Digital marketplaces Cloud and channels Partner-to-partner networking Key channel trends Software partner program profiles and innovative practices Core Research Channel Revenue Forecast and Analysis Partner Marketing and Communications Ecosystem Digital Transformation Cloud and Channels Emerging Partner Business Models Partner Collaboration.",
			};
			builder.Entity<Report>().HasData(report14);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report14.Id,
				 TagId = tag1.Id
			 },
			 new ReportTags
			 {
				 ReportId = report14.Id,
				 TagId = tag3.Id
			 }
			);
			var report15 = new Report
			{
				Id = Guid.Parse("3bec79ef-9a4a-41e4-9195-0a5c024cb193"),
				Name = "Multicloud Data Management and Protection",
				NormalizedName = "Multicloud Data Management and Protection".ToUpper(),
				AuthorId = author2.Id,
				ImagePath = "3bec79ef-9a4a-41e4-9195-0a5c024cb193.jpg",
				Content = "3bec79ef-9a4a-41e4-9195-0a5c024cb193",
				IndustryId = industry1.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 17),
				Summary = "Multicloud Data Management and Protection service enables storage vendors and IT users to have a more comprehensive view of the evolving data protection, availability, and recovery market. Integrating cloud technology with traditional hardware and software components, the report series will include market forecasts and provide a combination of timely tactical market information and long-term strategic analysis. Markets and Subjects Analyzed Cloud-based solutions, including backup as a service (BaaS) and disaster recovery as a service (DRaaS) Public, private, and hybrid cloud data protection technologies Disk-based data protection and recovery solutions packaged as software, appliance, or gateway system with a focus on technology evolution Market forecast based on total terabytes shipped and revenue for disk-based data protection and recovery (No vendor shares will be published.) End-user adoption of different data protection and recovery technologies and solutions Cloud service providers delivering cloud-based data protection Data protection for nontraditional data types (e.g., Mongo DB, Cassandra, Hadoop) Vendors delivering disk-based data protection and recovery solutions with focus on architecture based on use cases and applications Technologies and processes, including backup, snapshots, replication, continuous data protection, and data deduplication Impact of purpose-built backup appliances (PBBAs) and hyperconverged systems on overall market growth and customer adoption Implications of copy data management and impact on data availability, capacity management, cost, governance, and security The evolutionary impact of virtual infrastructure and object storage on data protection schemes Implications of flash technology for data protection Data protection best practice guidelines and adoption Core Research Market Analysis, Sizing, and Vendor Shares of Backup as a Service and Disaster Recovery as a Service Use of Disk-Based Technologies for Protection and Recovery Adoption Patterns and Role of PBBAs in Customer Environments End-User Needs and Requirements Data Protection and Recovery Software Market Size, Shares, and Forecasts Disk-Based Data Protection Market Size and Forecast Virtual Backup Appliance Solutions Intersection of Data Protection Software and Management with Cloud Services In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment.",
			};
			builder.Entity<Report>().HasData(report15);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report15.Id,
				 TagId = tag2.Id
			 },
			 new ReportTags
			 {
				 ReportId = report15.Id,
				 TagId = tag3.Id
			 }
			);
			var report16 = new Report
			{
				Id = Guid.Parse("aba89aa6-e637-4518-83c1-125b625add12"),
				Name = "DevOps Analytics, Automation and Security",
				NormalizedName = "DevOps Analytics, Automation and Security".ToUpper(),
				AuthorId = author3.Id,
				ImagePath = "aba89aa6-e637-4518-83c1-125b625add12.jpg",
				Content = "aba89aa6-e637-4518-83c1-125b625add12",
				IndustryId = industry1.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 23),
				Summary = "DevOps is a powerful modern approach to unifying the business strategy, development, testing, deployment, and life-cycle operation of software. This approach is accomplished by improving business, IT, and development collaboration while taking full advantage of automation technologies, end-to-end processes, microservices architecture, and cloud infrastructure to accelerate development and delivery and enable innovation. This subscription service provides insight, forecasts, and thought leadership to assist IT management, IT professionals, IT vendors, and service providers in creating compelling DevOps strategies and solutions. Markets and Subjects Analyzed DevOps adoption drivers, benefits, and use cases Identification of DevOps innovators and best practices Identification of critical DevOps tools enabling automation, open source technologies, and market leaders Analysis of the impact DevOps is having on IT infrastructure hardware and software purchasing and deployment priorities across on-premises and cloud service platforms Major transformation and impact DevOps is having on staffing, skills, and internal processes Core Research Worldwide DevOps Software Forecast, 2018-2022 Worldwide DevOps Software Market Share, 2018-2022 The Role of Cloud-Based IDEs to Drive DevOps Adoption Changing Drivers of DevOps: Role of Serverless and Microservices/Cloud-Native Architectures Market Analysis Perspective: Worldwide Developer, Operations, and DevOps Evolving Approaches and Practices in DevOps The Shift to Continuous Delivery and Deployment Tools and Frameworks for Supporting DevOps Workflows In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment.",
			};
			builder.Entity<Report>().HasData(report16);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report16.Id,
				 TagId = tag.Id
			 },
			 new ReportTags
			 {
				 ReportId = report16.Id,
				 TagId = tag3.Id
			 }
			);
			var report17 = new Report
			{
				Id = Guid.Parse("765337c9-8382-4498-9314-8b0cb77eb8dd"),
				Name = "Cybersecurity Analytics, Intelligence, Response and Orchestration",
				NormalizedName = "Cybersecurity Analytics, Intelligence, Response and Orchestration".ToUpper(),
				AuthorId = author4.Id,
				ImagePath = "765337c9-8382-4498-9314-8b0cb77eb8dd.jpg",
				Content = "765337c9-8382-4498-9314-8b0cb77eb8dd",
				IndustryId = industry1.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 6),
				Summary = "Cybersecurity Analytics, Intelligence, Response and Orchestration service covers security software and hardware products related to analytic security platforms, security and vulnerability management (SVM), and security orchestration platforms. Specific functions covered include vulnerability management and intelligence, SIEM, security analytics, threat hunting, incident detection and response, and orchestration. The service is designed to create in-depth coverage of the analytic-based and platform security markets. Markets and Subjects Analyzed Vulnerability management SIEM Security analytics Incident detection and response Threat analytics Automation Orchestration Core Research Security AIRO Forecast Security AIRO Market Share SIEM Market Share and Forecast Market MAP In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. Key Questions Answered What is the size and market opportunity for security analytics solutions? Who are the major players in the security analytics space? What is the size and market opportunity for security orchestration solutions? What is the size and market opportunity for threat analytics solutions? How has the competitive landscape changed through digital transformation and adoption of cloud and enabling technologies?",
			};
			builder.Entity<Report>().HasData(report17);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report17.Id,
				 TagId = tag1.Id
			 },
			 new ReportTags
			 {
				 ReportId = report17.Id,
				 TagId = tag3.Id
			 }
			);
			//------------------ FINANCE

			var report18 = new Report
			{
				Id = Guid.Parse("3a4b1c09-e5b7-4246-843c-bca16a106b97"),
				Name = "Consumer Banking Engagement Strategies",
				NormalizedName = "Consumer Banking Engagement Strategies".ToUpper(),
				AuthorId = author1.Id,
				ImagePath = "3a4b1c09-e5b7-4246-843c-bca16a106b97.jpg",
				Content = "3a4b1c09-e5b7-4246-843c-bca16a106b97",
				IndustryId = industry3.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 3, 18),
				Summary = "Being successful in banking will be determined by how well institutions manage the transformation in both digital and physical channels. Customers have seemingly ubiquitous access to their accounts on their terms and on their devices as banks continue to strategize about the future of the branch network and new channels emerge. Unfortunately, many banks are still looking at their channel strategy in a silo without fully understanding that customer engagement is the key to a profitable relationship. Today’s technology has fostered this customer-led revolution, yet there are many more changes yet to be realized as new technology is introduced. Advances in how we engage the customer are pushing the limits and skill sets of business units, marketers, and IT personnel as customers demand more from their retail bank. The Financial Insights: Consumer Banking Engagement Strategies provides critical analysis of the opportunities and options facing banks as they wrestle with their technology plans and investment decisions in alignment with their strategic goals. This research delivers key insights regarding the business drivers of and value delivered from customer-facing banking technology investments. Topics Addressed Throughout the year, this service will address the following topics: Digital transformation: Strategies and use cases are developing as banks transform all channels, including account opening and onboarding, ATM and ITM, augmented and virtual reality, branch banking, call center, chatbot services, contextualized marketing, conversational banking, digital banking (online and mobile), and social business. Whether these are first-generation offerings or have been around for decades, strategies need to be developed to implement, support, and upgrade these channels to stay with the times. Engagement strategies: Financial institutions are realizing that the number of engagements a customer has is an important factor in profitability. Using big data and analytics to properly measure the number of engagements is a start, but most institutions need to go beyond a prescriptive approach to customer behavior to a more cognitive approach. Omni-experience: The customer life-cycle process offers multiple channels that define the experience and dictate current and future relationships. Customer trends and strategies: This includes topics from level of interaction today to what is likely to be future behavior.",
			};
			builder.Entity<Report>().HasData(report18);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report18.Id,
				 TagId = tag12.Id
			 },
			 new ReportTags
			 {
				 ReportId = report18.Id,
				 TagId = tag13.Id
			 }
			);
			var report19 = new Report
			{
				Id = Guid.Parse("d1b04963-e698-41bf-aded-25bb755c2877"),
				Name = "Asia/Pacific Financial Services IT",
				NormalizedName = "Asia/Pacific Financial Services IT".ToUpper(),
				AuthorId = author4.Id,
				ImagePath = "d1b04963-e698-41bf-aded-25bb755c2877.jpg",
				Content = "d1b04963-e698-41bf-aded-25bb755c2877",
				IndustryId = industry3.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 2, 13),
				Summary = "Financial Insights: Asia/Pacific Banking Customer Centricity program provides insights into the evolving needs of Asia/Pacific retail/consumer banking retail customers and guidelines on how banks are to respond to these trends. The program will give advice to technology buyers on the technology that supports the customer centricity agenda of the financial institution – particularly in the areas of customer relationship management (CRM), omni-experience and omni-channel solutions, and in loyalty management. The service will be backed by a comprehensive consumer survey on the preferences of Asia/Pacific retail/consumer banking customers in various aspects of customer experience. Financial Insights will then undertake on how financial institutions are to develop an effective customer management agenda, delving into the concept, approach, strategy, use cases, and enabling technologies for customer centricity. Throughout the year, this service will address the following topics: Customer Experience Trends in Asia/Pacific Retail Banking How Asia/Pacific Consumers Pay - Evolving Trends in Retail Payments Customer Centricity in Retail Banking IT and Operations Customer Centricity in Marketing and Product Development Customer Centricity Technology Providers for Asia/Pacific Retail Banking.",
			};
			builder.Entity<Report>().HasData(report19);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report19.Id,
				 TagId = tag13.Id
			 },
			 new ReportTags
			 {
				 ReportId = report19.Id,
				 TagId = tag15.Id
			 }
			);
			var report20 = new Report
			{
				Id = Guid.Parse("bcaede74-79bf-4147-8d5b-46f161f4d015"),
				Name = "Universal Payment Strategies",
				NormalizedName = "Universal Payment Strategies".ToUpper(),
				AuthorId = author1.Id,
				ImagePath = "bcaede74-79bf-4147-8d5b-46f161f4d015.jpg",
				Content = "bcaede74-79bf-4147-8d5b-46f161f4d015",
				IndustryId = industry3.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 4, 18),
				Summary = "The payment industry has seen drastic changes in the past decade. New technologies, entrants, and business models have forced incumbent vendors and their financial institution customers to rethink how they move money. Stakeholders across the payment value chain — card-issuing banks, merchant acquirers, payment networks, and payment processors — face increasingly complex decisions. In this turbulent market, the players need more than facts and figures; they need critical analysis and insightful opinions. Markets and Subjects Analyzed Throughout the year, this service will address the following topics: Developing trends in payments such as omni-channel and alternative payment networks Evaluation and integration of new payment channels like voice commerce and IoT Enterprise risk, compliance, and fraud issues affecting payment products Legal and regulatory issues around the world that will affect how payments develop Middle- and back-office technologies that will affect the payment strategies of financial institutions Emerging technologies such as blockchain, AI, and next-generation security and their potential for altering the payment landscape Core Research MarketScape: Gateways for Integrated Payments B2B Payments: Digital Transformation in Transactions Retail Revolution: Beyond Card Payments Blockchain Payments: Short-Term Realities Persistent Payments: Automated Transactions in a Connected World Real-Time Payment Productization: Overlays in Action Retail Fraud: Protecting a Multi-Payment Environment",
			};
			builder.Entity<Report>().HasData(report20);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report20.Id,
				 TagId = tag14.Id
			 },
			 new ReportTags
			 {
				 ReportId = report20.Id,
				 TagId = tag15.Id
			 }

			);
			var report21 = new Report
			{
				Id = Guid.Parse("3dc823d3-91e3-40c6-b141-2c25ed9cd64a"),
				Name = "Worldwide Banking IT Spending Guide",
				NormalizedName = "Worldwide Banking IT Spending Guide".ToUpper(),
				AuthorId = author4.Id,
				ImagePath = "3dc823d3-91e3-40c6-b141-2c25ed9cd64a.jpg",
				Content = "3dc823d3-91e3-40c6-b141-2c25ed9cd64a",
				IndustryId = industry3.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 1, 3),
				Summary = "The Financial Insights: Worldwide Banking IT Spending Guide examines the banking industry opportunity from a technology, functional process, and geography perspective. This comprehensive database delivered via 's Customer Insights query tool allows the user to easily extract meaningful information about the banking technology market by viewing data trends and relationships and making data comparisons. Markets Covered This product covers the following segments of the banking market: 9 regions: USA, Canada, Japan, Western Europe, Central and Eastern Europe, Middle East and Africa, Latin America, PRC, and Asia/Pacific 4 technologies: Hardware, software, services, and internal IT spend 5 banking segments: Consumer banking, corporate and institutional banking, corporate administration, enterprise utilities, and shared services 30+ functional processes: Channels, payments, core processing, and more 4 company size tiers: Institution size by tier 1 through tier 4 2 institution types: Banks and credit unions 6 years of data Data Deliverables This spending guide is delivered on a semiannual basis via a web-based interface for online querying and downloads. For a complete delivery schedule, please contact an sales representative. The following are the deliverables for this spending guide: Annual five-year forecasts by regions, technologies, banking segments, functional processes, tiers, and institution types; delivered twice a year About This Spending Guide Financial Insights: Worldwide Banking IT Spending Guide provides guidance on the expected technology opportunity around this market at a regional and total worldwide level. Segmented by functional process, institution type, company size tier, region, and technology component, this guide provides IT vendors with insights into both large and rapidly growing segments of the banking technology market and how the market will develop over the coming years.",
			};
			builder.Entity<Report>().HasData(report21);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report21.Id,
				 TagId = tag12.Id
			 },
			 new ReportTags
			 {
				 ReportId = report21.Id,
				 TagId = tag13.Id
			 },
			 new ReportTags
			 {
				 ReportId = report21.Id,
				 TagId = tag15.Id
			 }
			);
			var report22 = new Report
			{
				Id = Guid.Parse("daaf7eab-fc1c-4a84-a8bc-3d857def3f82"),
				Name = "Insurance Digital Transformation Approach",
				NormalizedName = "Insurance Digital Transformation Approach".ToUpper(),
				AuthorId = author2.Id,
				ImagePath = "daaf7eab-fc1c-4a84-a8bc-3d857def3f82.jpg",
				Content = "daaf7eab-fc1c-4a84-a8bc-3d857def3f82",
				IndustryId = industry3.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 3, 13),
				Summary = "Customer experience is high on the agenda for most insurers and intermediaries as customers expect true value for the premiums they pay, above and beyond the traditional products and services delivered to them. To continue to be relevant in a changing marketplace, insurance organizations across the globe need to deliver contextual and value-centric insurance that is rooted on the principles of proactive risk management and secure, transparent, seamless, and contextual engagements across the customer journey. To achieve this, the industry needs to accelerate its effort to transform from a traditional, product-centric mindset to a customer-centric mindset enabled by digital technologies and the power of data and analytics. Insurance organizations need to increasingly play the role of true risk advisors rather than mere product sellers. As they progress in their transformation journey, they need advice and guidance to understand the opportunities and possibilities with the new technologies and to build and deploy digital capabilities while also tackling existing barriers to change. The Financial Insights: Worldwide Insurance Digital Transformation Strategies advisory service provides clients with insightful information and analysis of global insurance trends. It also provides coverage of how digital technologies like Big Data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies impact the life and annuity, accident and health, and property and casualty insurance markets. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of insurance organizations as well as technology vendors.",
			};
			builder.Entity<Report>().HasData(report22);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report22.Id,
				 TagId = tag5.Id
			 },
			 new ReportTags
			 {
				 ReportId = report22.Id,
				 TagId = tag13.Id
			 }
			);
			var report23 = new Report
			{
				Id = Guid.Parse("0daebcfa-c6a5-44f3-8193-25c442f9ec37"),
				Name = "International Corporate Banking Digital Transformation",
				NormalizedName = "International Corporate Banking Digital Transformation".ToUpper(),
				AuthorId = author.Id,
				ImagePath = "0daebcfa-c6a5-44f3-8193-25c442f9ec37.jpg",
				Content = "0daebcfa-c6a5-44f3-8193-25c442f9ec37",
				IndustryId = industry3.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 14),
				Summary = "With growing competition from nonbank platforms and the commoditization of products, the ability to onboard, connect, and deliver services to corporate customers is becoming paramount. With PSD2 in Europe democratizing data access and payment services, corporate banks have to transform their value proposition to deliver new data-driven services to their clients and add value, rather than their traditional, commoditized product proposition. Reducing the connectivity cost, increasing speed of data delivery toward real time, and integrating bank systems with corporate enterprise resource planning (ERP), treasury management systems (TSM), payment factories, and in-house banks will be paramount to help corporate clients manage capital, monitor cash flows, and optimize their liquidity. These infrastructure upgrades will also be crucial to exploit the evolving ecosystems brought about by distributed ledger technology (DLT) and the internet of things (IoT), where data discovery, analysis, and sharing will accelerate trade and reduce risk. Sensors attached to goods in transit — from the manufacturing plant to the retail outlet — could offer opportunities to banks' cash management and trade services businesses, better matching flows of payments and goods between seller and buyer. The Financial Insights: Worldwide Corporate Banking Digital Transformation Strategies advisory service provides clients with insightful information and analysis of corporate and commercial banking trends, including cash and treasury, trade finance, commercial lending, and payments. It also provides coverage of how the impact of emerging 3rd Platform technologies like big data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies transforms the sector and how their convergence unlocks new business and operating models. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of financial institutions as well as technology vendors.",
			};
			builder.Entity<Report>().HasData(report23);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report23.Id,
				 TagId = tag12.Id
			 },
			 new ReportTags
			 {
				 ReportId = report23.Id,
				 TagId = tag14.Id
			 }
			);
			//-----------------MARKETING

			var report24 = new Report
			{
				Id = Guid.Parse("67966830-655d-42f7-9424-ef5cf441ff8d"),
				Name = "Canadian Sales Accelerator: Datacenter Infrastructure",
				NormalizedName = "Canadian Sales Accelerator: Datacenter Infrastructure".ToUpper(),
				AuthorId = author3.Id,
				ImagePath = "67966830-655d-42f7-9424-ef5cf441ff8d.png",
				Content = "67966830-655d-42f7-9424-ef5cf441ff8d",
				IndustryId = industry4.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 21),
				Summary = "Canadian Sales Accelerator: Datacenter Infrastructure research program analyzes the Canadian datacenter (DC) infrastructure market in Canada. Designed to provide intelligence and strategic frameworks to technology sales professionals, field marketing teams, and channel managers to take action on key responsibilities related to the sales cycle, this service provides market sizing, vendor performance, forecasts, and market opportunities, as well as a variety of adjacent markets and segmentations from a quantitative standpoint. This advisory service will allow subscribers to identify opportunities tied to the future of infrastructure, tailor go-to-market strategies as well as product and services road maps to meet the growing demand of end users for infrastructure solutions. At the same time, it looks at vendors, partners, and customers from a qualitative standpoint, concerning needs and requirements, pain points and buying intentions, maturity levels, and adoption in and of new technologies. Markets and Subjects Analyzed Datacenter technologies (including software defined); server and storage; converged systems. Infrastructure ecosystem including partners and channels. Datacenter budget trends and dynamics and vendor selection/buying criteria. Technology investment expectations for legacy and next-gen DC infrastructure. In addition to the core research documents, clients will receive briefings and concise sales executive email alerts throughout the year. Every client will have a Sales Accelerator service launch integration meeting to kick-off the program. Core Research. Brand Perceptions on Enterprise Storage and Service Vendors. Market Forecasts. Vendor Dashboard: Market Shares. Infrastructure Ecosystem Barometer. Executive Market Insights",
			};
			builder.Entity<Report>().HasData(report24);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report24.Id,
				 TagId = tag17.Id
			 },
			 new ReportTags
			 {
				 ReportId = report24.Id,
				 TagId = tag18.Id
			 }
			);
			var report25 = new Report
			{
				Id = Guid.Parse("11dcaf06-8a35-403a-8262-ebd98344039a"),
				Name = "Digital Commerce Trends",
				NormalizedName = "Digital Commerce Trends".ToUpper(),
				AuthorId = author4.Id,
				ImagePath = "11dcaf06-8a35-403a-8262-ebd98344039a.jpg",
				Content = "11dcaf06-8a35-403a-8262-ebd98344039a",
				IndustryId = industry4.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 20),
				Summary = "The Digital Commerce subscription marketing intelligence service examines the competitive landscape, key trends, and differentiating factors of digital commerce, PIM, and CPQ application vendors; digital marketplaces; and business commerce networks. This includes the buying behavior of end users while purchasing products and services on digital commerce platforms. The service provides a worldwide perspective by looking at all forms of digital commerce transactions — including B2B, B2C, C2C, G2B, and B2B2C — across all vertical industries and regional markets. Markets and Subjects Analyzed. Digital commerce applications targeting businesses of all sizes and industries. Best-of-breed digital commerce applications in areas such as CPQ, payments and billing, order management, web content management, merchandising, site search, fulfillment, product information management, and inventory management. Business commerce networks that bring together functional applications for enterprise asset management, procurement, financials, sales, and human capital management.Enterprise partnership/integration strategies among digital commerce and marketing/content management vendors. Experience management — across channels (includes content management, commerce platforms, integration, and advanced analytics) for B2B, B2C, and B2B2C. Impact of cloud, social, mobile, and Big Data technologies on vendor strategies for employee, customer, supplier, partner, and asset engagement. Proliferation of public cloud microservice architectures. Mobile commerce (mobile app marketplaces, built-for-mobile commerce capabilities, and differentiating technology). Cognitive technologies and intelligent workflow impact on digital commerce applications",
			};

			builder.Entity<Report>().HasData(report25);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report25.Id,
				 TagId = tag5.Id
			 },
			 new ReportTags
			 {
				 ReportId = report25.Id,
				 TagId = tag16.Id
			 },
			 new ReportTags
			 {
				 ReportId = report25.Id,
				 TagId = tag17.Id
			 }
			);
			var report26 = new Report
			{
				Id = Guid.Parse("9062907e-6a91-4737-95f9-8db332df47f4"),
				Name = "CMO Advisory Service",
				NormalizedName = "CMO Advisory Service".ToUpper(),
				AuthorId = author3.Id,
				ImagePath = "9062907e-6a91-4737-95f9-8db332df47f4.jpg",
				Content = "9062907e-6a91-4737-95f9-8db332df47f4",
				IndustryId = industry4.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 21),
				Summary = "CMO Advisory Service guides marketing leaders as they master the science of marketing. Digital transformation offers CMOs the opportunity to become revenue drivers and architects of the customer experience. Leaders leverage 's deep industry knowledge, powerful quantitative models, peer-tested practices, and personalized guidance to advance their operations. Whether seeking fresh perspectives on core challenges or counsel on emerging developments, offers a trusted source of insight.Markets and Subjects Analyzed. Marketing Investment and Transformational Operations. Planning and budgeting investments in the marketing mix, marketing technology, and marketing operations, accountability, and attribution. Application of New Technology for Marketing. Guidance on the implications of new technologies such as artificial intelligence (AI), collaboration, and marketing automation solutions. Delivering the New Customer Experience. Mapping and responding to the B2B customer decision journey. The Future Marketing Organization. Organizational design, staff allocation benchmarks, role definition, talent development, shared services, organizational alignment, and change management. Developing core competencies: Content marketing, customer intelligence and analytics, integrated digital and social engagement, sales enablement, and loyalty and advocacy",
			};
			builder.Entity<Report>().HasData(report26);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report26.Id,
				 TagId = tag15.Id
			 },
			 new ReportTags
			 {
				 ReportId = report26.Id,
				 TagId = tag16.Id
			 }
			);
			var report27 = new Report
			{
				Id = Guid.Parse("90c09b29-4745-484f-b5d9-f24696e4acc7"),
				Name = "Marketing and Sales Solutions",
				NormalizedName = "Marketing and Sales Solutions".ToUpper(),
				AuthorId = author4.Id,
				ImagePath = "90c09b29-4745-484f-b5d9-f24696e4acc7.png",
				Content = "90c09b29-4745-484f-b5d9-f24696e4acc7",
				IndustryId = industry4.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 19),
				Summary = "Marketing and sales technologies are driving forces for all companies as customers move to online, mobile-first, and collaborative relationship models. In reaching and selling to customers, it is essential that organizations aggressively develop the necessary operational and analytic skills, collaborative cultures, and creative problem solving needed to truly add value to the customer relationship. 's Marketing and Sales Solutions service provides strategic frameworks for thinking about the individual and aligned areas of marketing and sales technology as parts of a holistic business strategy. This program delivers insight, information, and data on the main drivers for the adoption of these technologies in the broader context of customer experience (CX) and networked business strategies.Markets and Subjects Analyzed. Marketing automation, campaign management, and go to market execution applications. Sales force automation applications. Predictive analytics and business KPIs. Mobile and digital applications and strategies. Customer data and analytics. CX strategies. Core Research. Reports on how 670 U.S. large enterprises use more than 300 vendors in 15 martech categories across 5 vertical markets:Consumer banking; Retail; CPG manufacturing; Securities and investment services; Travel and hospitality; MarketScape(s) on related solutions areas such as marketing clouds and artificial intelligence; TechScapes and PlanScapes on various topics such as GDPR, account-based marketing, personalization, and mobile marketing; Marketing software forecast and vendor shares; Sales force automation forecast and vendor shares. In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. ",
			};
			builder.Entity<Report>().HasData(report27);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report27.Id,
				 TagId = tag16.Id
			 },
			 new ReportTags
			 {
				 ReportId = report27.Id,
				 TagId = tag18.Id
			 }
			);
			var report28 = new Report
			{
				Id = Guid.Parse("7af1f20b-db8a-4eb0-b484-eb03487efb02"),
				Name = "Customer Experience Management Strategies",
				NormalizedName = "Customer Experience Management Strategies".ToUpper(),
				AuthorId = author3.Id,
				ImagePath = "7af1f20b-db8a-4eb0-b484-eb03487efb02.jpg",
				Content = "7af1f20b-db8a-4eb0-b484-eb03487efb02",
				IndustryId = industry4.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 30),
				Summary = "Customer Experience Management Strategies SIS provides a framework and critical knowledge for understanding the changing nature of the customer experience (CX) and guides chief experience officers and their organizations as they master the digital transformation of the customer experience. This product covers the concepts of experience management and customer experience, looking at how digital transformation is driving change to customer expectations, preferences, and behavior and how enterprises must adopt new technologies to meet these urgent challenges. Markets and Subjects Analyzed: Experience management; Customer-centric engagement Customer-centric operations; Customer-centric solution design; Impact of 3rd Platform technologies on customer experience; Intelligence and analytics-driven customer experience; Customer experience along the customer journey; Customer experience benchmarks Core Research: Customer Experience Taxonomy; Digital Transformation of the Customer Experience; Redefining Experience Management; PeerScape: Customer Experience; Customer Experience in an Algorithm Economy; Technology-Enabled Storytelling; MaturityScape: Customer Experience; Customer Intelligence and Analytics; Innovation Accelerators in Customer Experience: Artificial Intelligence; In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment.",
			};
			builder.Entity<Report>().HasData(report28);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report28.Id,
				 TagId = tag16.Id
			 },
			 new ReportTags
			 {
				 ReportId = report28.Id,
				 TagId = tag17.Id
			 }
			);
			var report29 = new Report
			{
				Id = Guid.Parse("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef"),
				Name = "Retail Insights: European Retail Digital Transformation Strategies",
				NormalizedName = "Retail Insights: European Retail Digital Transformation Strategies".ToUpper(),
				AuthorId = author4.Id,
				ImagePath = "3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef.jpg",
				Content = "3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef",
				IndustryId = industry4.Id,
				IsApproved = true,
				CreatedOn = new DateTime(2020, 5, 18),
				Summary = "European retailing is in a state of change since digital technologies have become so pervasive in the retail journey and consumer expectations, in terms of customer experience, with their preferred brands rapidly evolving. European retailers are adapting to these changes through the adoption of commerce everywhere business models and technology. Retailers from across Europe are in the process of determining how digital impacts them and what their digital transformation approach and strategy should be. This reveals a more and more complex and highly differentiated European region, where differences exist and persist across countries as well as subsegments (fashion and apparel, department stores, food and grocery, etc.). Retail Insights is witnessing among European retailers, at varying degrees of maturity, a race to digitize because of the potential new revenue streams and retail operational efficiencies that can be derived. Retail Insights: European Retail Digital Transformation Strategies advisory service examines the impact of digital transformation on the European retailers' business, technology, and organizational areas. Specific coverage is given to provide valuable insights into the European retail industry, with a specific focus on digital transformation strategies applied by retail companies to improve the omni-channel customer experience. This advisory service develops unique analysis and comprehensive data through Retail Insights' proprietary research projects, along with ongoing communications with industry experts, retail CIOs, and line-of-business executives, and ICT product and service vendors. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports. Our analysts are also available to provide personalized advice for retail executives and ICT vendors to help them make better-informed decisions.",
			};
			builder.Entity<Report>().HasData(report29);

			builder.Entity<ReportTags>().HasData(
			 new ReportTags
			 {
				 ReportId = report29.Id,
				 TagId = tag15.Id
			 },
			 new ReportTags
			 {
				 ReportId = report29.Id,
				 TagId = tag18.Id
			 }
			);

		}
	}
}
