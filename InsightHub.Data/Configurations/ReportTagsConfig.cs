﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
	public class ReportTagsConfig : IEntityTypeConfiguration<ReportTags>
    {
        public void Configure(EntityTypeBuilder<ReportTags> builder)
        {
            builder.HasKey(rt => new { rt.TagId, rt.ReportId });

            builder.HasOne(rt => rt.Report)
                   .WithMany(r => r.Tags)
                   .HasForeignKey(rt => rt.ReportId);

            builder.HasOne(rt => rt.Tag)
                  .WithMany(t => t.ReportTags)
                  .HasForeignKey(rt => rt.TagId);
        }
    }
}
