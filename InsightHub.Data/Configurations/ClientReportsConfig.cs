﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
	public class ClientReportsConfig : IEntityTypeConfiguration<ClientReports>
    {
        public void Configure(EntityTypeBuilder<ClientReports> builder)
        {
            builder.HasKey(cr => new { cr.ReportId, cr.UserId });

            builder.HasOne(cr => cr.Client)
                   .WithMany(c => c.Downloads)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(cr => cr.Report)
                   .WithMany(r => r.Downloads)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
