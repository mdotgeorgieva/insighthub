﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
	public class ReportsConfig : IEntityTypeConfiguration<Report>
    {
        public void Configure(EntityTypeBuilder<Report> builder)
        {
            builder.HasOne(r => r.Author)
                   .WithMany(a => a.WrittenReports)
                   .HasForeignKey(r => r.AuthorId);

            builder.HasMany(r => r.Downloads)
                   .WithOne(cr => cr.Report)
                   .HasForeignKey(cr => cr.ReportId);

            builder.HasOne(r => r.Industry)
                   .WithMany(i => i.Reports)
                   .HasForeignKey(r => r.IndustryId);

            builder.Property(u => u.CreatedOn).HasDefaultValueSql("getutcdate()");
        }
    }
}
