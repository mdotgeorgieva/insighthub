﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
	public class UserIndustriesConfig : IEntityTypeConfiguration<UserIndustry>
    {
        public void Configure(EntityTypeBuilder<UserIndustry> builder)
        {
            builder.HasKey(ui => new { ui.IndustryId, ui.ClientId });

            builder.HasOne(ui => ui.Client)
                   .WithMany(c => c.Subscriptions)
                   .HasForeignKey(ui => ui.ClientId);

            builder.HasOne(ui => ui.Industry)
                   .WithMany(i => i.Subscribers)
                   .HasForeignKey(ui => ui.IndustryId);
        }
    }
}
