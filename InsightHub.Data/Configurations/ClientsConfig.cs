﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InsightHub.Data.Configurations
{
	public class ClientsConfig : IEntityTypeConfiguration<Client>
    {
        public void Configure(EntityTypeBuilder<Client> builder)
        {
            builder.HasMany(c => c.Downloads)
                   .WithOne(cr => cr.Client)
                   .HasForeignKey(cr => cr.UserId);

            builder.HasOne(c => c.User)
                   .WithOne(u => u.Client)
                   .HasForeignKey<Client>(c => c.UserId);
        }
    }
}
