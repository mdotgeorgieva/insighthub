﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace InsightHub.Data.Migrations
{
    public partial class AddValidation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3604c205-28eb-45e2-a029-f38e1c206c8a"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3604c205-28eb-45e2-a029-f38e1c206c8a"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3604c205-28eb-45e2-a029-f38e1c206c8a"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("36533151-9264-41de-836e-b58d3dbfb364"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("36533151-9264-41de-836e-b58d3dbfb364"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("36533151-9264-41de-836e-b58d3dbfb364"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("36533151-9264-41de-836e-b58d3dbfb364"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("36533151-9264-41de-836e-b58d3dbfb364"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5179e8fb-52d0-49b4-841f-aae3c0bd086e"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5179e8fb-52d0-49b4-841f-aae3c0bd086e"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5179e8fb-52d0-49b4-841f-aae3c0bd086e"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5522d757-5cb0-4f10-adb2-5d472bbd79b0"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5522d757-5cb0-4f10-adb2-5d472bbd79b0"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5522d757-5cb0-4f10-adb2-5d472bbd79b0"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5522d757-5cb0-4f10-adb2-5d472bbd79b0"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("5522d757-5cb0-4f10-adb2-5d472bbd79b0"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("55553fa1-4dfd-48c7-abbb-346f30d9d47e"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("55553fa1-4dfd-48c7-abbb-346f30d9d47e"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("649c608d-e658-4a0c-ab24-7e35f36e409a"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("649c608d-e658-4a0c-ab24-7e35f36e409a"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("649c608d-e658-4a0c-ab24-7e35f36e409a"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("7ab98bef-40c1-4a1b-bf1a-88911ae38852"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("7ab98bef-40c1-4a1b-bf1a-88911ae38852"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("7ab98bef-40c1-4a1b-bf1a-88911ae38852"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("7ab98bef-40c1-4a1b-bf1a-88911ae38852"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("7c0fdd96-f11f-4fe8-b811-90577d20ff2f"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("7c0fdd96-f11f-4fe8-b811-90577d20ff2f"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("8726cd04-1394-4f69-a016-2a80f15c6238"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("8726cd04-1394-4f69-a016-2a80f15c6238"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("91893923-a104-4a3c-ad98-a9487896f066"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("91893923-a104-4a3c-ad98-a9487896f066"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("91893923-a104-4a3c-ad98-a9487896f066"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("9282060a-bca0-4d10-8546-601862379082"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("9282060a-bca0-4d10-8546-601862379082"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a73af8ad-cd30-411e-bf70-f7af80b91018"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a73af8ad-cd30-411e-bf70-f7af80b91018"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("bf6b1c50-38fc-4bb4-8a5b-09f075f46d17"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("bf6b1c50-38fc-4bb4-8a5b-09f075f46d17"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("bf6b1c50-38fc-4bb4-8a5b-09f075f46d17"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cd64cb0b-f2a3-4b4b-9284-d6badceedcdb"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cd64cb0b-f2a3-4b4b-9284-d6badceedcdb"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cd64cb0b-f2a3-4b4b-9284-d6badceedcdb"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ce01eae4-256c-46e9-b42d-98f7e7602218"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ce01eae4-256c-46e9-b42d-98f7e7602218"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ce01eae4-256c-46e9-b42d-98f7e7602218"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ce36a51a-9b79-45a9-beeb-40fa09a77d4f"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ce36a51a-9b79-45a9-beeb-40fa09a77d4f"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ce36a51a-9b79-45a9-beeb-40fa09a77d4f"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d54acd8a-4af1-4f65-9e6b-1ad68315c987"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d54acd8a-4af1-4f65-9e6b-1ad68315c987"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d6f523c1-8247-400e-abf3-5ca85575ccfd"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d6f523c1-8247-400e-abf3-5ca85575ccfd"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("d6f523c1-8247-400e-abf3-5ca85575ccfd"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e7c3cd21-b001-4f84-b724-04ab0ce10a9d"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e7c3cd21-b001-4f84-b724-04ab0ce10a9d"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e7c3cd21-b001-4f84-b724-04ab0ce10a9d"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e7c3cd21-b001-4f84-b724-04ab0ce10a9d"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") });

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("0070a7dc-c760-4a34-8c87-d4747824191b"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("11dcaf06-8a35-403a-8262-ebd98344039a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("180360e0-6db8-412e-a6f4-3d68a939c583"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("4a817d53-485b-4634-9c09-22eb56d367fc"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("67966830-655d-42f7-9424-ef5cf441ff8d"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("76352896-e547-485f-ae20-73b4f05b664f"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("9062907e-6a91-4737-95f9-8db332df47f4"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("aba89aa6-e637-4518-83c1-125b625add12"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("cffed078-aa51-4fe2-913e-59e6e2624828"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("d1b04963-e698-41bf-aded-25bb755c2877"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("3604c205-28eb-45e2-a029-f38e1c206c8a"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("36533151-9264-41de-836e-b58d3dbfb364"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("5179e8fb-52d0-49b4-841f-aae3c0bd086e"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("5522d757-5cb0-4f10-adb2-5d472bbd79b0"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("55553fa1-4dfd-48c7-abbb-346f30d9d47e"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("649c608d-e658-4a0c-ab24-7e35f36e409a"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("7ab98bef-40c1-4a1b-bf1a-88911ae38852"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("7c0fdd96-f11f-4fe8-b811-90577d20ff2f"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("8726cd04-1394-4f69-a016-2a80f15c6238"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("91893923-a104-4a3c-ad98-a9487896f066"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("9282060a-bca0-4d10-8546-601862379082"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("a73af8ad-cd30-411e-bf70-f7af80b91018"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("bf6b1c50-38fc-4bb4-8a5b-09f075f46d17"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("cd64cb0b-f2a3-4b4b-9284-d6badceedcdb"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("ce01eae4-256c-46e9-b42d-98f7e7602218"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("ce36a51a-9b79-45a9-beeb-40fa09a77d4f"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("d54acd8a-4af1-4f65-9e6b-1ad68315c987"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("d6f523c1-8247-400e-abf3-5ca85575ccfd"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("e7c3cd21-b001-4f84-b724-04ab0ce10a9d"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("8d223ce3-389a-4c03-8a5d-51c2d3e79bde"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("b13ece8a-9eb4-49e0-b4eb-6bbf7cd6e9e6"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("c4417156-6782-43a7-9d3c-127699043dec"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("c94d7746-8c70-4b7b-8910-489f643ea7f4"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("dbdd3bd0-5cac-496c-8133-49ac43b94e47"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("1665f048-f08c-47a7-a044-e7122b79e93e"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("32a66306-6e1d-4ac6-bc93-a29c78a1efbc"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("73cd65a0-dedd-4b14-80a0-9766d5808165"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("a6f963a9-15bd-4c72-903c-ea64adfe34f2"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("eda7936e-987b-4c67-ac6d-242b38c45e7f"));

            migrationBuilder.AlterColumn<string>(
                name: "Summary",
                table: "Reports",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Reports",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Notifications",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Industries",
                maxLength: 30,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "Bio",
                table: "Authors",
                maxLength: 450,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "AspNetUsers",
                maxLength: 40,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "AspNetUsers",
                maxLength: 30,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("297d06e6-c058-486f-a18a-06a971ebfcd7"),
                column: "ConcurrencyStamp",
                value: "7307ef7a-f132-43af-a87f-c55f6695ee28");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("6c8fcd7e-62f6-4f3e-a73d-acbfd60b97ab"),
                column: "ConcurrencyStamp",
                value: "dd8ac280-5665-44d7-a002-df4429fa8a3c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7f66990c-36bc-4381-9f81-32e06e168319"),
                column: "ConcurrencyStamp",
                value: "56160629-5e66-4d73-99e1-043947cdb0a6");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb93"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "515e3d96-d8c7-46f6-8cbb-5a4d38da1df8", new DateTime(2020, 6, 3, 20, 7, 37, 327, DateTimeKind.Utc).AddTicks(8634), "AQAAAAEAACcQAAAAED2S3143VA7R9thcDZf4zI9vkbamVkm33TuyvfZBxoQDf5bYaVI0ulT22XrHVmZv0w==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb94"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "1f6b5f45-33a0-48e6-92b9-ce77f6c84d41", new DateTime(2020, 6, 3, 20, 7, 37, 328, DateTimeKind.Utc).AddTicks(469), "AQAAAAEAACcQAAAAEE5Fqs94skQq4wNv62cQkcxoIpRCo+V6GKsbdZBWSu+TcnUPTd0+UpE+j1i5Hj6h6g==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb95"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "8ee50e9c-d1e0-4b22-90f8-74b80905d740", new DateTime(2020, 6, 3, 20, 7, 37, 328, DateTimeKind.Utc).AddTicks(493), "AQAAAAEAACcQAAAAECsLpqoCe+YKcrQMiTkBxvueo0GwHyjMzz5ug6MgjnvHXSek6JFM91UjK5h0rAFOgQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb96"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "e1c952d0-08c0-44a9-85cc-447b01f703e6", new DateTime(2020, 6, 3, 20, 7, 37, 328, DateTimeKind.Utc).AddTicks(502), "AQAAAAEAACcQAAAAEC9PL6pN/rey2H2GmNsLXH+NhdmCjeVQmaF7QvLmPzieHXdJeFdPLfCS1ClutP+WoQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb97"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "1e73f509-c423-48dd-a1c3-24aee8df173d", new DateTime(2020, 6, 3, 20, 7, 37, 328, DateTimeKind.Utc).AddTicks(520), "AQAAAAEAACcQAAAAELQK3mWEF9bPq5LD81owSXEFiQkx4TGgdElly5IE9x3oZGM/xmjisMe6ZSNN0GnrRQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb98"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "a4483175-98a2-4bee-a8c5-fce24dec37d7", new DateTime(2020, 6, 3, 20, 7, 37, 328, DateTimeKind.Utc).AddTicks(526), "AQAAAAEAACcQAAAAEEbSrFDGoFYNOTVfTX+s5rJOySjuq6WQTHa3nKmOBJog2DW/aZBoJkZaa7j9eknxPQ==" });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "Bio", "ImagePath", "UserId" },
                values: new object[,]
                {
                    { new Guid("0f55f9f5-377b-4c4e-b719-202f42e362ed"), "Jim Doe has been involved with ICT research in Africa since 1997 and has participated in diverse research projects in 14 African countries. He started his career in 1994 as a public relations officer with Software Technologies Limited (an East African Oracle distributor), before serving as a research analyst/project officer for Telecom Forum Africa in 1997.", "3.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb94") },
                    { new Guid("a0753d98-6ad6-4d34-a6f0-b11d097ba22a"), "Rosa James has a background in journalism and has been published in various local, regional, and international magazines and newspapers. She has spoken at various industry events across sub-Saharan Africa including countries like Ethiopia, Ghana, Kenya, Nigeria, Rwanda, Tanzania and Zambia as well as leading and facilitating at IDC’s events in the region. Rose is also a published author of a children’s novel.", "1.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb95") },
                    { new Guid("888bf762-2fbf-4e0c-8b0d-17e0e958c8d9"), "Cate Williams has been an analyst InsightHub for several years. Early on as an analyst she covered CRM applications, but more recently her work has been in integration middleware covering markets such as API management, file sync and share, and B2B integration.", "2.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb96") },
                    { new Guid("ab8b06b4-b690-465e-b5d6-335844c6c2e4"), "Max Upton has more than 15 years of experience in business research, focusing on financial services and business innovation. His consulting work for InsightHub has allowed him to work closely with leading banks and regulators in the region for their technology and innovation strategies. Mr. Upton is concurrently head of InsightHub's business and operations in Thailand.", "7.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb97") },
                    { new Guid("85c355c7-7026-495d-b585-3ea9d41eb256"), "Previously, David Scott served for ten years as Enterprise Architect at CareFirst Blue Cross/Blue Shield, responsible for building business and technology alignment roadmaps for the ACA/HIX Health Reform, Mandated Implementations, PCMH, and Provider Network domains to ensure alignment of annual and tactical IT project planning with business goals.", "8.jpg", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb98") }
                });

            migrationBuilder.InsertData(
                table: "Industries",
                columns: new[] { "Id", "DeletedOn", "ImagePath", "IsDeleted", "ModifiedOn", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("fef5d942-d69f-4dc5-9571-659c0efa7cc5"), null, null, false, null, "Energy", "ENERGY" },
                    { new Guid("370779d0-f7d8-4414-9b1b-d0693026d864"), null, null, false, null, "Information Technology", "INFORMATION TECHNOLOGY" },
                    { new Guid("6febb9bc-fe8d-4fd4-9886-05f33623a7e3"), null, null, false, null, "Healthcare", "HEALTHCARE" },
                    { new Guid("65d09ef9-75cf-4dba-8696-b4cd6cca0d80"), null, null, false, null, "Finance", "FINANCE" },
                    { new Guid("8d00adfa-b32e-45c7-9a66-e70b8024c89f"), null, null, false, null, "Marketing", "MARKETING" }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "DeletedOn", "IsDeleted", "ModifiedOn", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("626d10b3-5fb7-4d7d-b217-2c4068432415"), null, false, null, "computers", "COMPUTERS" },
                    { new Guid("cee94eb5-b53a-4c62-85c5-ca30d4b8168a"), null, false, null, "hospital", "HOSPITAL" },
                    { new Guid("4311b6f9-7c6b-47ab-b087-505491437ab1"), null, false, null, "influence", "INFLUENCE" },
                    { new Guid("a3695e8c-9ae2-425b-9894-0c5598fec5e2"), null, false, null, "trend", "TREND" },
                    { new Guid("6838a923-7a12-45a7-b236-77c914fc1e48"), null, false, null, "sales", "SALES" },
                    { new Guid("a95eedd0-bc4d-41cd-919d-e6b253528f91"), null, false, null, "profit", "PROFIT" },
                    { new Guid("580099be-781d-4263-823a-c49ca66f5fc7"), null, false, null, "insurance", "INSURANCE" },
                    { new Guid("a2970e11-3441-4928-b1ca-6db1ddd80ddc"), null, false, null, "stock", "STOCK" },
                    { new Guid("b1909bc6-3cff-473e-92ca-1592204f1c84"), null, false, null, "nuclear", "NUCLEAR" },
                    { new Guid("19427f4e-ec5c-4df9-8dcf-78596c8b4a52"), null, false, null, "solar", "SOLAR" },
                    { new Guid("23ea4c07-3ccb-49f1-8bda-608aab47e77a"), null, false, null, "oil", "OIL" },
                    { new Guid("71e41f22-2692-4aa8-9bca-28a9e3bcce58"), null, false, null, "laboratory", "LABORATORY" },
                    { new Guid("ea028850-416b-4f14-8204-950fcc54745d"), null, false, null, "vaccine", "VACCINE" },
                    { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), null, false, null, "research", "RESEARCH" },
                    { new Guid("887504de-d923-48a3-b8a9-09f9674bc08b"), null, false, null, "medicine", "MEDICINE" },
                    { new Guid("72a0d8ed-736e-4e71-a11a-e0d3cbc1f98f"), null, false, null, "web", "WEB" },
                    { new Guid("feee373a-6385-4bda-9593-ca45d8e51e4e"), null, false, null, "hardware", "HARDWARE" },
                    { new Guid("3779d426-b772-483c-8a2e-c0a5f8776dbd"), null, false, null, "software", "SOFTWARE" },
                    { new Guid("b0497d09-1bba-4970-afa7-db24ec1c3b30"), null, false, null, "investment", "INVESTMENT" },
                    { new Guid("299ba278-1365-4221-b2c8-4fb573ce946d"), null, false, null, "gas", "GAS" }
                });

            migrationBuilder.InsertData(
                table: "Reports",
                columns: new[] { "Id", "AuthorId", "Content", "CreatedOn", "DeletedOn", "ImagePath", "IndustryId", "IsApproved", "IsDeleted", "IsFeatured", "ModifiedOn", "Name", "NormalizedName", "Summary" },
                values: new object[,]
                {
                    { new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb"), new Guid("85c355c7-7026-495d-b585-3ea9d41eb256"), "3d360ea3-0587-4e87-9eeb-07f7a429b6cb", new DateTime(2020, 3, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "3d360ea3-0587-4e87-9eeb-07f7a429b6cb.jpg", new Guid("fef5d942-d69f-4dc5-9571-659c0efa7cc5"), true, false, false, null, "Worldwide Utilities Connected Asset", "WORLDWIDE UTILITIES CONNECTED ASSET", "Energy and digital technologies are radically transforming utilities asset operations and strategies. Renewables and decentralized power generation, energy delivery networks (electricity, gas, and heat), and water and waste water management benefit from the developments and adoption of IoT, AI, and digital twins.  Energy Insights: Worldwide Utilities Connected Asset Strategies service focuses on all these. It provides guidance to end users in terms of digital transformation (DX) use cases road map, analysis of emerging IT trends, and evaluation of technology providers in this space. It looks into the future of work in the utilities value chain context. At the same time, it provides technology vendors a view on utilities' operational excellence asset strategies addressing their go to market.This service develops unique analysis and comprehensive data through  Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from both  and  Energy Insights. Research documents elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research documents." },
                    { new Guid("9062907e-6a91-4737-95f9-8db332df47f4"), new Guid("ab8b06b4-b690-465e-b5d6-335844c6c2e4"), "9062907e-6a91-4737-95f9-8db332df47f4", new DateTime(2020, 5, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "9062907e-6a91-4737-95f9-8db332df47f4.jpg", new Guid("8d00adfa-b32e-45c7-9a66-e70b8024c89f"), true, false, false, null, "CMO Advisory Service", "CMO ADVISORY SERVICE", "CMO Advisory Service guides marketing leaders as they master the science of marketing. Digital transformation offers CMOs the opportunity to become revenue drivers and architects of the customer experience. Leaders leverage 's deep industry knowledge, powerful quantitative models, peer-tested practices, and personalized guidance to advance their operations. Whether seeking fresh perspectives on core challenges or counsel on emerging developments, offers a trusted source of insight.Markets and Subjects Analyzed. Marketing Investment and Transformational Operations. Planning and budgeting investments in the marketing mix, marketing technology, and marketing operations, accountability, and attribution. Application of New Technology for Marketing. Guidance on the implications of new technologies such as artificial intelligence (AI), collaboration, and marketing automation solutions. Delivering the New Customer Experience. Mapping and responding to the B2B customer decision journey. The Future Marketing Organization. Organizational design, staff allocation benchmarks, role definition, talent development, shared services, organizational alignment, and change management. Developing core competencies: Content marketing, customer intelligence and analytics, integrated digital and social engagement, sales enablement, and loyalty and advocacy" },
                    { new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7"), new Guid("85c355c7-7026-495d-b585-3ea9d41eb256"), "90c09b29-4745-484f-b5d9-f24696e4acc7", new DateTime(2020, 5, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "90c09b29-4745-484f-b5d9-f24696e4acc7.png", new Guid("8d00adfa-b32e-45c7-9a66-e70b8024c89f"), true, false, false, null, "Marketing and Sales Solutions", "MARKETING AND SALES SOLUTIONS", "Marketing and sales technologies are driving forces for all companies as customers move to online, mobile-first, and collaborative relationship models. In reaching and selling to customers, it is essential that organizations aggressively develop the necessary operational and analytic skills, collaborative cultures, and creative problem solving needed to truly add value to the customer relationship. 's Marketing and Sales Solutions service provides strategic frameworks for thinking about the individual and aligned areas of marketing and sales technology as parts of a holistic business strategy. This program delivers insight, information, and data on the main drivers for the adoption of these technologies in the broader context of customer experience (CX) and networked business strategies.Markets and Subjects Analyzed. Marketing automation, campaign management, and go to market execution applications. Sales force automation applications. Predictive analytics and business KPIs. Mobile and digital applications and strategies. Customer data and analytics. CX strategies. Core Research. Reports on how 670 U.S. large enterprises use more than 300 vendors in 15 martech categories across 5 vertical markets:Consumer banking; Retail; CPG manufacturing; Securities and investment services; Travel and hospitality; MarketScape(s) on related solutions areas such as marketing clouds and artificial intelligence; TechScapes and PlanScapes on various topics such as GDPR, account-based marketing, personalization, and mobile marketing; Marketing software forecast and vendor shares; Sales force automation forecast and vendor shares. In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. " },
                    { new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02"), new Guid("ab8b06b4-b690-465e-b5d6-335844c6c2e4"), "7af1f20b-db8a-4eb0-b484-eb03487efb02", new DateTime(2020, 5, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "7af1f20b-db8a-4eb0-b484-eb03487efb02.jpg", new Guid("8d00adfa-b32e-45c7-9a66-e70b8024c89f"), true, false, false, null, "Customer Experience Management Strategies", "CUSTOMER EXPERIENCE MANAGEMENT STRATEGIES", "Customer Experience Management Strategies SIS provides a framework and critical knowledge for understanding the changing nature of the customer experience (CX) and guides chief experience officers and their organizations as they master the digital transformation of the customer experience. This product covers the concepts of experience management and customer experience, looking at how digital transformation is driving change to customer expectations, preferences, and behavior and how enterprises must adopt new technologies to meet these urgent challenges. Markets and Subjects Analyzed: Experience management; Customer-centric engagement Customer-centric operations; Customer-centric solution design; Impact of 3rd Platform technologies on customer experience; Intelligence and analytics-driven customer experience; Customer experience along the customer journey; Customer experience benchmarks Core Research: Customer Experience Taxonomy; Digital Transformation of the Customer Experience; Redefining Experience Management; PeerScape: Customer Experience; Customer Experience in an Algorithm Economy; Technology-Enabled Storytelling; MaturityScape: Customer Experience; Customer Intelligence and Analytics; Innovation Accelerators in Customer Experience: Artificial Intelligence; In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef"), new Guid("85c355c7-7026-495d-b585-3ea9d41eb256"), "3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef", new DateTime(2020, 5, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef.jpg", new Guid("8d00adfa-b32e-45c7-9a66-e70b8024c89f"), true, false, false, null, "Retail Insights: European Retail Digital Transformation Strategies", "RETAIL INSIGHTS: EUROPEAN RETAIL DIGITAL TRANSFORMATION STRATEGIES", "European retailing is in a state of change since digital technologies have become so pervasive in the retail journey and consumer expectations, in terms of customer experience, with their preferred brands rapidly evolving. European retailers are adapting to these changes through the adoption of commerce everywhere business models and technology. Retailers from across Europe are in the process of determining how digital impacts them and what their digital transformation approach and strategy should be. This reveals a more and more complex and highly differentiated European region, where differences exist and persist across countries as well as subsegments (fashion and apparel, department stores, food and grocery, etc.). Retail Insights is witnessing among European retailers, at varying degrees of maturity, a race to digitize because of the potential new revenue streams and retail operational efficiencies that can be derived. Retail Insights: European Retail Digital Transformation Strategies advisory service examines the impact of digital transformation on the European retailers' business, technology, and organizational areas. Specific coverage is given to provide valuable insights into the European retail industry, with a specific focus on digital transformation strategies applied by retail companies to improve the omni-channel customer experience. This advisory service develops unique analysis and comprehensive data through Retail Insights' proprietary research projects, along with ongoing communications with industry experts, retail CIOs, and line-of-business executives, and ICT product and service vendors. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports. Our analysts are also available to provide personalized advice for retail executives and ICT vendors to help them make better-informed decisions." },
                    { new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97"), new Guid("a0753d98-6ad6-4d34-a6f0-b11d097ba22a"), "3a4b1c09-e5b7-4246-843c-bca16a106b97", new DateTime(2020, 3, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "3a4b1c09-e5b7-4246-843c-bca16a106b97.jpg", new Guid("65d09ef9-75cf-4dba-8696-b4cd6cca0d80"), true, false, false, null, "Consumer Banking Engagement Strategies", "CONSUMER BANKING ENGAGEMENT STRATEGIES", "Being successful in banking will be determined by how well institutions manage the transformation in both digital and physical channels. Customers have seemingly ubiquitous access to their accounts on their terms and on their devices as banks continue to strategize about the future of the branch network and new channels emerge. Unfortunately, many banks are still looking at their channel strategy in a silo without fully understanding that customer engagement is the key to a profitable relationship. Today’s technology has fostered this customer-led revolution, yet there are many more changes yet to be realized as new technology is introduced. Advances in how we engage the customer are pushing the limits and skill sets of business units, marketers, and IT personnel as customers demand more from their retail bank. The Financial Insights: Consumer Banking Engagement Strategies provides critical analysis of the opportunities and options facing banks as they wrestle with their technology plans and investment decisions in alignment with their strategic goals. This research delivers key insights regarding the business drivers of and value delivered from customer-facing banking technology investments. Topics Addressed Throughout the year, this service will address the following topics: Digital transformation: Strategies and use cases are developing as banks transform all channels, including account opening and onboarding, ATM and ITM, augmented and virtual reality, branch banking, call center, chatbot services, contextualized marketing, conversational banking, digital banking (online and mobile), and social business. Whether these are first-generation offerings or have been around for decades, strategies need to be developed to implement, support, and upgrade these channels to stay with the times. Engagement strategies: Financial institutions are realizing that the number of engagements a customer has is an important factor in profitability. Using big data and analytics to properly measure the number of engagements is a start, but most institutions need to go beyond a prescriptive approach to customer behavior to a more cognitive approach. Omni-experience: The customer life-cycle process offers multiple channels that define the experience and dictate current and future relationships. Customer trends and strategies: This includes topics from level of interaction today to what is likely to be future behavior." },
                    { new Guid("d1b04963-e698-41bf-aded-25bb755c2877"), new Guid("85c355c7-7026-495d-b585-3ea9d41eb256"), "d1b04963-e698-41bf-aded-25bb755c2877", new DateTime(2020, 2, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "d1b04963-e698-41bf-aded-25bb755c2877.jpg", new Guid("65d09ef9-75cf-4dba-8696-b4cd6cca0d80"), true, false, false, null, "Asia/Pacific Financial Services IT", "ASIA/PACIFIC FINANCIAL SERVICES IT", "Financial Insights: Asia/Pacific Banking Customer Centricity program provides insights into the evolving needs of Asia/Pacific retail/consumer banking retail customers and guidelines on how banks are to respond to these trends. The program will give advice to technology buyers on the technology that supports the customer centricity agenda of the financial institution – particularly in the areas of customer relationship management (CRM), omni-experience and omni-channel solutions, and in loyalty management. The service will be backed by a comprehensive consumer survey on the preferences of Asia/Pacific retail/consumer banking customers in various aspects of customer experience. Financial Insights will then undertake on how financial institutions are to develop an effective customer management agenda, delving into the concept, approach, strategy, use cases, and enabling technologies for customer centricity. Throughout the year, this service will address the following topics: Customer Experience Trends in Asia/Pacific Retail Banking How Asia/Pacific Consumers Pay - Evolving Trends in Retail Payments Customer Centricity in Retail Banking IT and Operations Customer Centricity in Marketing and Product Development Customer Centricity Technology Providers for Asia/Pacific Retail Banking." },
                    { new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015"), new Guid("a0753d98-6ad6-4d34-a6f0-b11d097ba22a"), "bcaede74-79bf-4147-8d5b-46f161f4d015", new DateTime(2020, 4, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "bcaede74-79bf-4147-8d5b-46f161f4d015.jpg", new Guid("65d09ef9-75cf-4dba-8696-b4cd6cca0d80"), true, false, false, null, "Universal Payment Strategies", "UNIVERSAL PAYMENT STRATEGIES", "The payment industry has seen drastic changes in the past decade. New technologies, entrants, and business models have forced incumbent vendors and their financial institution customers to rethink how they move money. Stakeholders across the payment value chain — card-issuing banks, merchant acquirers, payment networks, and payment processors — face increasingly complex decisions. In this turbulent market, the players need more than facts and figures; they need critical analysis and insightful opinions. Markets and Subjects Analyzed Throughout the year, this service will address the following topics: Developing trends in payments such as omni-channel and alternative payment networks Evaluation and integration of new payment channels like voice commerce and IoT Enterprise risk, compliance, and fraud issues affecting payment products Legal and regulatory issues around the world that will affect how payments develop Middle- and back-office technologies that will affect the payment strategies of financial institutions Emerging technologies such as blockchain, AI, and next-generation security and their potential for altering the payment landscape Core Research MarketScape: Gateways for Integrated Payments B2B Payments: Digital Transformation in Transactions Retail Revolution: Beyond Card Payments Blockchain Payments: Short-Term Realities Persistent Payments: Automated Transactions in a Connected World Real-Time Payment Productization: Overlays in Action Retail Fraud: Protecting a Multi-Payment Environment" },
                    { new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a"), new Guid("85c355c7-7026-495d-b585-3ea9d41eb256"), "3dc823d3-91e3-40c6-b141-2c25ed9cd64a", new DateTime(2020, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "3dc823d3-91e3-40c6-b141-2c25ed9cd64a.jpg", new Guid("65d09ef9-75cf-4dba-8696-b4cd6cca0d80"), true, false, false, null, "Worldwide Banking IT Spending Guide", "WORLDWIDE BANKING IT SPENDING GUIDE", "The Financial Insights: Worldwide Banking IT Spending Guide examines the banking industry opportunity from a technology, functional process, and geography perspective. This comprehensive database delivered via 's Customer Insights query tool allows the user to easily extract meaningful information about the banking technology market by viewing data trends and relationships and making data comparisons. Markets Covered This product covers the following segments of the banking market: 9 regions: USA, Canada, Japan, Western Europe, Central and Eastern Europe, Middle East and Africa, Latin America, PRC, and Asia/Pacific 4 technologies: Hardware, software, services, and internal IT spend 5 banking segments: Consumer banking, corporate and institutional banking, corporate administration, enterprise utilities, and shared services 30+ functional processes: Channels, payments, core processing, and more 4 company size tiers: Institution size by tier 1 through tier 4 2 institution types: Banks and credit unions 6 years of data Data Deliverables This spending guide is delivered on a semiannual basis via a web-based interface for online querying and downloads. For a complete delivery schedule, please contact an sales representative. The following are the deliverables for this spending guide: Annual five-year forecasts by regions, technologies, banking segments, functional processes, tiers, and institution types; delivered twice a year About This Spending Guide Financial Insights: Worldwide Banking IT Spending Guide provides guidance on the expected technology opportunity around this market at a regional and total worldwide level. Segmented by functional process, institution type, company size tier, region, and technology component, this guide provides IT vendors with insights into both large and rapidly growing segments of the banking technology market and how the market will develop over the coming years." },
                    { new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82"), new Guid("888bf762-2fbf-4e0c-8b0d-17e0e958c8d9"), "daaf7eab-fc1c-4a84-a8bc-3d857def3f82", new DateTime(2020, 3, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "daaf7eab-fc1c-4a84-a8bc-3d857def3f82.jpg", new Guid("65d09ef9-75cf-4dba-8696-b4cd6cca0d80"), true, false, false, null, "Insurance Digital Transformation Approach", "INSURANCE DIGITAL TRANSFORMATION APPROACH", "Customer experience is high on the agenda for most insurers and intermediaries as customers expect true value for the premiums they pay, above and beyond the traditional products and services delivered to them. To continue to be relevant in a changing marketplace, insurance organizations across the globe need to deliver contextual and value-centric insurance that is rooted on the principles of proactive risk management and secure, transparent, seamless, and contextual engagements across the customer journey. To achieve this, the industry needs to accelerate its effort to transform from a traditional, product-centric mindset to a customer-centric mindset enabled by digital technologies and the power of data and analytics. Insurance organizations need to increasingly play the role of true risk advisors rather than mere product sellers. As they progress in their transformation journey, they need advice and guidance to understand the opportunities and possibilities with the new technologies and to build and deploy digital capabilities while also tackling existing barriers to change. The Financial Insights: Worldwide Insurance Digital Transformation Strategies advisory service provides clients with insightful information and analysis of global insurance trends. It also provides coverage of how digital technologies like Big Data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies impact the life and annuity, accident and health, and property and casualty insurance markets. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of insurance organizations as well as technology vendors." },
                    { new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37"), new Guid("0f55f9f5-377b-4c4e-b719-202f42e362ed"), "0daebcfa-c6a5-44f3-8193-25c442f9ec37", new DateTime(2020, 5, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "0daebcfa-c6a5-44f3-8193-25c442f9ec37.jpg", new Guid("65d09ef9-75cf-4dba-8696-b4cd6cca0d80"), true, false, false, null, "International Corporate Banking Digital Transformation", "INTERNATIONAL CORPORATE BANKING DIGITAL TRANSFORMATION", "With growing competition from nonbank platforms and the commoditization of products, the ability to onboard, connect, and deliver services to corporate customers is becoming paramount. With PSD2 in Europe democratizing data access and payment services, corporate banks have to transform their value proposition to deliver new data-driven services to their clients and add value, rather than their traditional, commoditized product proposition. Reducing the connectivity cost, increasing speed of data delivery toward real time, and integrating bank systems with corporate enterprise resource planning (ERP), treasury management systems (TSM), payment factories, and in-house banks will be paramount to help corporate clients manage capital, monitor cash flows, and optimize their liquidity. These infrastructure upgrades will also be crucial to exploit the evolving ecosystems brought about by distributed ledger technology (DLT) and the internet of things (IoT), where data discovery, analysis, and sharing will accelerate trade and reduce risk. Sensors attached to goods in transit — from the manufacturing plant to the retail outlet — could offer opportunities to banks' cash management and trade services businesses, better matching flows of payments and goods between seller and buyer. The Financial Insights: Worldwide Corporate Banking Digital Transformation Strategies advisory service provides clients with insightful information and analysis of corporate and commercial banking trends, including cash and treasury, trade finance, commercial lending, and payments. It also provides coverage of how the impact of emerging 3rd Platform technologies like big data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies transforms the sector and how their convergence unlocks new business and operating models. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of financial institutions as well as technology vendors." },
                    { new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0"), new Guid("a0753d98-6ad6-4d34-a6f0-b11d097ba22a"), "698b544c-d07c-41d1-9fad-20dabb68c7b0", new DateTime(2020, 5, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "698b544c-d07c-41d1-9fad-20dabb68c7b0.jpg", new Guid("6febb9bc-fe8d-4fd4-9886-05f33623a7e3"), true, false, false, null, "Health systems and universal health coverage", "HEALTH SYSTEMS AND UNIVERSAL HEALTH COVERAGE", "In the SDG monitoring framework, progress towards universal health coverage (UHC) is tracked with two indicators: (i) a service coverage index (which measures coverage of selected essential health services on a scale of 0 to 100); and (ii) the proportion of the population with large out-of-pocket expenditures on health care (which measures the incidence of catastrophic health spending, rendered as percentage). The service coverage index improved from 45 globally in 2000 to 66 in 2017, with the strongest increase in low-and lower-middle-income countries, where the baseline at 2000 was lowest. However, the pace of that progress has slowed since 2010. The improvements are especially notable for infectious disease interventions and, to a lesser extent, for reproductive, maternal and child health services. Within countries, coverage of the latter services is typically lower in poorer households than in richer households. Overall, between one third and one half the world’s population (33% to 49%) was covered by essential health services in 2017 . Service coverage continued to be lower in low- and middle-income countries than in wealthier ones; the same held for health workforce densities and immunization coverage (Figure 1.2). Available data indicate that over 40% of all countries have fewer than 10 medical doctors per 10 000 people, over 55% have fewer." },
                    { new Guid("180360e0-6db8-412e-a6f4-3d68a939c583"), new Guid("0f55f9f5-377b-4c4e-b719-202f42e362ed"), "180360e0-6db8-412e-a6f4-3d68a939c583", new DateTime(2020, 2, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "180360e0-6db8-412e-a6f4-3d68a939c583.jpg", new Guid("6febb9bc-fe8d-4fd4-9886-05f33623a7e3"), true, false, false, null, "Investing in strengthening country health information systems", "INVESTING IN STRENGTHENING COUNTRY HEALTH INFORMATION SYSTEMS", "Accurate, timely, and comparable health-related statistics are essential for understanding population health trends. Decision-makers need the information to develop appropriate policies, allocate resources and prioritize interventions. For almost a fifth of countries, over half of the indicators have no recent primary or direct underlying data. Data gaps and lags prevent from truly understanding who is being included or left aside and take timely and appropriate action. The existing SDG indicators address a broad range of health aspects but do not capture the breadth of population health outcomes and determinants. Monitoring and evaluating population health thus goes beyond the indicators covered in this report and often requires additional and improved measurements. WHO is committed to supporting Member States to make improvements in surveillance and health information systems. These improvements will enhance the scope and quality of health information and standardize processes to generate comparable estimates at the global level. Getting accurate data on COVID-19 related deaths has been a challenge. The COVID-19 pandemic underscores the serious gaps in timely, reliable, accessible and actionable data and measurements that compromise preparedness, prevention and response to health emergencies. The International Health Regulations (IHR) (2005) monitoring framework is one of the data collection tools that have demonstrated value in evaluating and building country capacities to prevent, detect, assess, report and respond to public health emergencies. From self-assessment of the 13 core capacities in 2019, countries have shown steady progress across almost all capacities including surveillance, laboratory and coordination. As the pandemic progresses, objective and comparable data are crucial to determine the effectiveness of different national strategies used to mitigate and suppress, and thus to better prepare for the probable continuation of the epidemic over the next year or more." },
                    { new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2"), new Guid("a0753d98-6ad6-4d34-a6f0-b11d097ba22a"), "044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2", new DateTime(2020, 3, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2.jpg", new Guid("6febb9bc-fe8d-4fd4-9886-05f33623a7e3"), true, false, false, null, "Maternal mortality has declined but progress is uneven across regions", "MATERNAL MORTALITY HAS DECLINED BUT PROGRESS IS UNEVEN ACROSS REGIONS", "A total of 295 000 [UI 1 80%: 279 000–340 000] women worldwide lost their lives during and following pregnancy and childbirth in 2017, with sub-Saharan Africa and South Asia accounting for approximately 86% of all maternal deaths worldwide. The global maternal mortality ratio (MMR, the number of maternal deaths per 100 000 live births) was estimated at 211 [UI 80%: 199–243], representing a 38% reduction since 2000. On average, global MMR declined by 2.9% every year between 2000 1 UI = uncertainty interval. and 2017. If the pace of progress accelerates enough to achieve the SDG target (reducing global MMR to less than 70 per 100 000 live births), it would save the lives of at least one million women. The majority of maternal deaths are preventable through appropriate management of pregnancy and care at birth, including antenatal care by trained health providers, assistance during childbirth by skilled health personnel, and care and support in the weeks after childbirth. Data from 2014 to 2019 indicate that approximately 81% of all births globally took place in the presence of skilled health personnel, an increase from 64% in the 2000–2006 period. In sub-Saharan Africa, where roughly 66% of the world’s maternal deaths occur, only 60% of births were assisted by skilled health personnel during the 2014–2019 period. " },
                    { new Guid("0070a7dc-c760-4a34-8c87-d4747824191b"), new Guid("0f55f9f5-377b-4c4e-b719-202f42e362ed"), "0070a7dc-c760-4a34-8c87-d4747824191b", new DateTime(2020, 5, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "0070a7dc-c760-4a34-8c87-d4747824191b.jpg", new Guid("6febb9bc-fe8d-4fd4-9886-05f33623a7e3"), true, false, false, null, "European Value-Based Healthcare Digital Transformation", "EUROPEAN VALUE-BASED HEALTHCARE DIGITAL TRANSFORMATION", " With the advent of the value-based paradigm, healthcare organizations are today tasked to ensure equitable access and financial sustainability while improving and reducing the variation of clinical outcomes in healthcare systems. Increasing collaboration across the healthcare value chain, adopting a more personalized approach to treatment, championing the patient experience, and adopting outcome-based business models are fast emerging as necessary for the healthcare industry to succeed. The IDC Health Insights: European Value-Based Healthcare Digital Transformation Strategies service focuses on analyzing European healthcare providers, payers, and public health policy makers' digital strategy and best practices in supporting the adoption of value-based healthcare to deliver the maximum value to patients. Value-based healthcare is a key driver of digital transformation as it requires an unprecedented level of availability of patient information. Key solutions as HIE, population health management, smart patient engagement tools, advanced analytics and cognitive, and cloud and mobility will fundamentally impact care providers and payers' value-based healthcare strategies execution and success. Approach. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244"), new Guid("a0753d98-6ad6-4d34-a6f0-b11d097ba22a"), "e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244", new DateTime(2020, 5, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244.jpg", new Guid("6febb9bc-fe8d-4fd4-9886-05f33623a7e3"), true, false, false, null, "European Digital Hospital Evolution", "EUROPEAN DIGITAL HOSPITAL EVOLUTION", "European Digital Hospital service provides detailed information about the evolution of digital hospital transformation and technology adoption. It offers valuable insights into IT solution trends within the hospital sector, including healthcare-specific technologies such as electronic health records; admission, discharge, and transfer; digital medical imaging systems; VNA and archiving; and business intelligence and patient management systems. This service focuses on a vital component of healthcare — the digital hospital. Healthcare systems across the world are facing care quality and sustainability challenges that require a new approach to care delivery and reimbursement models. The fully digital hospital is in the center of that journey, being a key enabler of change. The hospital CIOs are facing new technology but, at the same time, a growing legacy IT burden that must be managed alongside of new digital initiatives. In healthcare, management of legacy systems is therefore a growing challenge — when looking at not only obsolete digital applications but also infrastructure and, in the end, how healthcare providers purchase digital applications and platforms. When IT fails to deliver because of legacy and lack of agility, the business model and clinical processes become legacy as well. Key solutions such as EHR, RIS, PACS, VNA, ECM, advanced analytics, cloud, and mobility will fundamentally impact hospitals' digital strategies execution and success.This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806"), new Guid("0f55f9f5-377b-4c4e-b719-202f42e362ed"), "c03a5b96-20c4-4bac-9c92-aea551fc9806", new DateTime(2020, 4, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "c03a5b96-20c4-4bac-9c92-aea551fc9806.jpg", new Guid("6febb9bc-fe8d-4fd4-9886-05f33623a7e3"), true, false, false, null, "Health Insights: European Life Science and Pharma", "HEALTH INSIGHTS: EUROPEAN LIFE SCIENCE AND PHARMA", "Digital transformation is key in the European life science industry's ongoing efforts to transform itself as it seeks to find new paths to innovation, optimize operational efficiency and effectiveness, and regain long-term sustainability. The transition toward new care delivery ecosystems and outcome-based reimbursement models enhances life science organizations' needs to implement new approaches to information management and use. The digital mission in the life sciences is to integrate new and existing data, information, and knowledge into a more knowledge-centric approach to new drug and medical devices development, to product commercialization and management, and to treatment delivery. The European life science digital transformation service provides a forward-looking analysis of IT and technologies that are being adopted all along the value chain of the European life science industry. The service focuses on how European life science organizations can leverage the new range of technologies underpinned by the 3rd Platform technologies and innovation accelerators to support efforts to improve the operational efficiency of the industry and better engage with the ultimate end user of life science innovations, namely patients. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae"), new Guid("0f55f9f5-377b-4c4e-b719-202f42e362ed"), "40758cb7-f705-424a-9dac-db2bac62c9ae", new DateTime(2020, 2, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "40758cb7-f705-424a-9dac-db2bac62c9ae.jpg", new Guid("370779d0-f7d8-4414-9b1b-d0693026d864"), true, false, false, null, "Canadian Internet of Things Ecosystem and Trends", "CANADIAN INTERNET OF THINGS ECOSYSTEM AND TRENDS", "The Internet of Things (IoT) market is poised for exponential growth over the next several years. However, the IoT ecosystem is a complex segment with multiple layers and hundreds of players, including device and module manufacturers, communication service providers (CSPs), IoT platform players, applications, analytics and security software vendors, and professional services providers. 's Canadian Internet of Things Ecosystem and Trends service analyzes the regionally specific growth of this market from the autonomously connected units in the enterprise world to the applications and services that will demonstrate the power of a world of connected .Markets and Subjects Analyzed Market size and forecast for the Canadian IoT market Canadian IoT revenue forecast by industry Canadian IoT revenue forecast by use cases Taxonomy of the IoT ecosystem: How is the market organized, and what are the key segments and technologies? Canadian-specific demand-side data from various segments (e.g., IT and LOB) Analysis of Canadian-specific vendor readiness and go-to-market strategies for IoT Vertical use cases/case studies of IoT Core Research Taxonomy of the IoT Ecosystem Forecast for the Canadian IoT Market by Industry Forecast for the Canadian IoT Market by Use Cases Profiles of Canadian Vendors Leading IoT Evolution Case Studies of Successful Canadian IoT Implementations Canadian Internet of Things Decision-Maker Survey In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("cffed078-aa51-4fe2-913e-59e6e2624828"), new Guid("a0753d98-6ad6-4d34-a6f0-b11d097ba22a"), "cffed078-aa51-4fe2-913e-59e6e2624828", new DateTime(2020, 4, 29, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "cffed078-aa51-4fe2-913e-59e6e2624828.jpg", new Guid("370779d0-f7d8-4414-9b1b-d0693026d864"), true, false, false, null, "Agile Application Life-Cycle, Quality and Portfolio Strategies", "AGILE APPLICATION LIFE-CYCLE, QUALITY AND PORTFOLIO STRATEGIES", "Agile Application Life-Cycle, Quality and Portfolio Strategies service provides insight into business and IT alignment through end-to-end application life-cycle management (ALM) and DevOps strategies including agile, IT portfolio management, software quality, testing and verification tools, software change and configuration, and continuous releases for digital transformation. This service provides insights on how product, organizational, and process transitions can help address regulatory compliance, security, and licensing issues covering cloud, SaaS, and open source software. It also analyzes trends related to ALM and software deployment with applications that leverage and target artificial intelligence (AI) and machine learning (ML), collaboration, mobile and embedded apps, IoT, virtualization, and complex sourcing and evolve to adaptive work, project and portfolio management (PPM), and agile adoption with organizational and process strategies (e.g., Scaled Agile Framework [SAFe]). Markets and Subjects Analyzed Software life-cycle process, requirements, configuration management and collaboration, and agile development and projects Software quality, including mobile, cloud, and embedded strategies PPM and work management software trends and strategies IT project and portfolio management software Developer, business, and operations trends Tools enabling DevOps and governance solutions to align complex deployment, financial management, infrastructure, and business priorities Trends in software configuration management (SCM), including connecting business to IT via requirements management and web services demands on agile life-cycle management solutions Automated software quality (ASQ) tools evolution: Hosted testing, web services, API and cloud testing, service virtualization, vulnerability, risk and value-based testing, and other emerging technologies (e.g., mobile, analysis, and metrics) Trends in collaborative software development with social media" },
                    { new Guid("4a817d53-485b-4634-9c09-22eb56d367fc"), new Guid("888bf762-2fbf-4e0c-8b0d-17e0e958c8d9"), "4a817d53-485b-4634-9c09-22eb56d367fc", new DateTime(2020, 6, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "4a817d53-485b-4634-9c09-22eb56d367fc.jpg", new Guid("370779d0-f7d8-4414-9b1b-d0693026d864"), true, false, false, null, "Software Channels and Ecosystems", "SOFTWARE CHANNELS AND ECOSYSTEMS", "Software Channels and Ecosystems research service offers intelligence and expertise to help channel executives and program managers develop, implement, support, and manage effective channel strategies and programs to drive successful relationships with the spectrum of partner activities (e.g., resale, managed/cloud services, consulting professional services, and software development). In addition, it provides a comprehensive view of the value of the indirect software market and its leading vendor proponents. This service also identifies and analyzes key industry trends and their impact on channel relationship drivers and channel partner business models. Subscribers are invited to 's semiannual Software Channel Leadership Council where and channel executives present and discuss key industry issues. Markets and Subjects Analyzed Multinational software vendors Digital transformation in the partner ecosystem Software and SaaS channel revenue flow Digital marketplaces Cloud and channels Partner-to-partner networking Key channel trends Software partner program profiles and innovative practices Core Research Channel Revenue Forecast and Analysis Partner Marketing and Communications Ecosystem Digital Transformation Cloud and Channels Emerging Partner Business Models Partner Collaboration." },
                    { new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193"), new Guid("888bf762-2fbf-4e0c-8b0d-17e0e958c8d9"), "3bec79ef-9a4a-41e4-9195-0a5c024cb193", new DateTime(2020, 5, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "3bec79ef-9a4a-41e4-9195-0a5c024cb193.jpg", new Guid("370779d0-f7d8-4414-9b1b-d0693026d864"), true, false, false, null, "Multicloud Data Management and Protection", "MULTICLOUD DATA MANAGEMENT AND PROTECTION", "Multicloud Data Management and Protection service enables storage vendors and IT users to have a more comprehensive view of the evolving data protection, availability, and recovery market. Integrating cloud technology with traditional hardware and software components, the report series will include market forecasts and provide a combination of timely tactical market information and long-term strategic analysis. Markets and Subjects Analyzed Cloud-based solutions, including backup as a service (BaaS) and disaster recovery as a service (DRaaS) Public, private, and hybrid cloud data protection technologies Disk-based data protection and recovery solutions packaged as software, appliance, or gateway system with a focus on technology evolution Market forecast based on total terabytes shipped and revenue for disk-based data protection and recovery (No vendor shares will be published.) End-user adoption of different data protection and recovery technologies and solutions Cloud service providers delivering cloud-based data protection Data protection for nontraditional data types (e.g., Mongo DB, Cassandra, Hadoop) Vendors delivering disk-based data protection and recovery solutions with focus on architecture based on use cases and applications Technologies and processes, including backup, snapshots, replication, continuous data protection, and data deduplication Impact of purpose-built backup appliances (PBBAs) and hyperconverged systems on overall market growth and customer adoption Implications of copy data management and impact on data availability, capacity management, cost, governance, and security The evolutionary impact of virtual infrastructure and object storage on data protection schemes Implications of flash technology for data protection Data protection best practice guidelines and adoption Core Research Market Analysis, Sizing, and Vendor Shares of Backup as a Service and Disaster Recovery as a Service Use of Disk-Based Technologies for Protection and Recovery Adoption Patterns and Role of PBBAs in Customer Environments End-User Needs and Requirements Data Protection and Recovery Software Market Size, Shares, and Forecasts Disk-Based Data Protection Market Size and Forecast Virtual Backup Appliance Solutions Intersection of Data Protection Software and Management with Cloud Services In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("aba89aa6-e637-4518-83c1-125b625add12"), new Guid("ab8b06b4-b690-465e-b5d6-335844c6c2e4"), "aba89aa6-e637-4518-83c1-125b625add12", new DateTime(2020, 5, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "aba89aa6-e637-4518-83c1-125b625add12.jpg", new Guid("370779d0-f7d8-4414-9b1b-d0693026d864"), true, false, false, null, "DevOps Analytics, Automation and Security", "DEVOPS ANALYTICS, AUTOMATION AND SECURITY", "DevOps is a powerful modern approach to unifying the business strategy, development, testing, deployment, and life-cycle operation of software. This approach is accomplished by improving business, IT, and development collaboration while taking full advantage of automation technologies, end-to-end processes, microservices architecture, and cloud infrastructure to accelerate development and delivery and enable innovation. This subscription service provides insight, forecasts, and thought leadership to assist IT management, IT professionals, IT vendors, and service providers in creating compelling DevOps strategies and solutions. Markets and Subjects Analyzed DevOps adoption drivers, benefits, and use cases Identification of DevOps innovators and best practices Identification of critical DevOps tools enabling automation, open source technologies, and market leaders Analysis of the impact DevOps is having on IT infrastructure hardware and software purchasing and deployment priorities across on-premises and cloud service platforms Major transformation and impact DevOps is having on staffing, skills, and internal processes Core Research Worldwide DevOps Software Forecast, 2018-2022 Worldwide DevOps Software Market Share, 2018-2022 The Role of Cloud-Based IDEs to Drive DevOps Adoption Changing Drivers of DevOps: Role of Serverless and Microservices/Cloud-Native Architectures Market Analysis Perspective: Worldwide Developer, Operations, and DevOps Evolving Approaches and Practices in DevOps The Shift to Continuous Delivery and Deployment Tools and Frameworks for Supporting DevOps Workflows In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd"), new Guid("85c355c7-7026-495d-b585-3ea9d41eb256"), "765337c9-8382-4498-9314-8b0cb77eb8dd", new DateTime(2020, 5, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "765337c9-8382-4498-9314-8b0cb77eb8dd.jpg", new Guid("370779d0-f7d8-4414-9b1b-d0693026d864"), true, false, false, null, "Cybersecurity Analytics, Intelligence, Response and Orchestration", "CYBERSECURITY ANALYTICS, INTELLIGENCE, RESPONSE AND ORCHESTRATION", "Cybersecurity Analytics, Intelligence, Response and Orchestration service covers security software and hardware products related to analytic security platforms, security and vulnerability management (SVM), and security orchestration platforms. Specific functions covered include vulnerability management and intelligence, SIEM, security analytics, threat hunting, incident detection and response, and orchestration. The service is designed to create in-depth coverage of the analytic-based and platform security markets. Markets and Subjects Analyzed Vulnerability management SIEM Security analytics Incident detection and response Threat analytics Automation Orchestration Core Research Security AIRO Forecast Security AIRO Market Share SIEM Market Share and Forecast Market MAP In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. Key Questions Answered What is the size and market opportunity for security analytics solutions? Who are the major players in the security analytics space? What is the size and market opportunity for security orchestration solutions? What is the size and market opportunity for threat analytics solutions? How has the competitive landscape changed through digital transformation and adoption of cloud and enabling technologies?" },
                    { new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0"), new Guid("888bf762-2fbf-4e0c-8b0d-17e0e958c8d9"), "8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0", new DateTime(2020, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0.jpg", new Guid("fef5d942-d69f-4dc5-9571-659c0efa7cc5"), true, false, false, null, "North America Utilities Digital Transformation Tactics", "ENERGY INSIGHTS: NORTH AMERICA UTILITIES DIGITAL TRANSFORMATION TACTICS", "Digital transformation is driving utilities to change their business and operating and information models. The Energy Insights: North America Utilities Digital Transformation Strategies service is designed to help North American utility IT and business management understand the disruptions that are transforming the energy and utility value chains and develop the strategies and programs to capitalize on the evolving opportunities. The service provides deep research and insight on the key topics that matter to North American utility decision makers and IT executives affected by the digital transformation. Through an integrated set of deliverables, subscribers gain access to analysis and insights on the impact of new technologies, IT strategies and best practices, and the industry, governmental, and regulatory forces that are shaping the evolution to new industry business models in the industry's generation, transmission and distribution/pipelines, and retail sectors. This service delivers research, insight, and guidance on IT strategy and investments to meet the utility's most critical corporate objectives." },
                    { new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069"), new Guid("0f55f9f5-377b-4c4e-b719-202f42e362ed"), "d2b8c016-2bec-4a0a-acbe-c2fe03b55069", new DateTime(2020, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "d2b8c016-2bec-4a0a-acbe-c2fe03b55069.jpg", new Guid("fef5d942-d69f-4dc5-9571-659c0efa7cc5"), true, false, false, null, "Worldwide Oil and Gas Downstream IT Blueprint", "WORLDWIDE OIL AND GAS DOWNSTREAM IT BLUEPRINT", "Energy Insights: Worldwide Oil and Gas Downstream IT Strategies advisory research service takes the worldwide oil and gas IT strategies research and applies it to the downstream operations in oil and gas. The research focuses on companies that are involved in the production for sale of all petroleum products, from incoming crude to outgoing product. It will also encompass the technology companies that provide the technology foundation for the operation of the downstream business. Energy Insights: Worldwide Oil and Gas Downstream IT Strategies will focus its analysis on the digital transformation (DX) use cases, priorities, innovation technology, and road map for all aspect of companies that produce and distribute petroleum products. It will provide technology vendors a road map for product development and messaging in oil and gas downstream. Throughout the year, this service will address the following topics: Digital transformation – Investment priorities and road maps for transformation of the downstream business operation Operations technology — IT/OT convergence investments that help downstream organizations transform how they operate the production lines Asset management – Technology changes driving asset management organization and transformation Transformation to scale – Development of a transformed infrastructure to scale to market volatility Our research addresses the following issues that are critical to your success: What is the impact of transformational technologies on the oil and gas downstream DX road map? How does an oil and gas downstream company organize itself around the Future of Work? What are the market trends in oil and gas downstream that affect the use case prioritization for DX? How should trading partners be integrated into the DX road map? Who are the technology vendors that can support DX in oil and gas downstream?" },
                    { new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a"), new Guid("888bf762-2fbf-4e0c-8b0d-17e0e958c8d9"), "e4b77a8a-ff72-4851-94f5-a1faff00731a", new DateTime(2020, 1, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "e4b77a8a-ff72-4851-94f5-a1faff00731a.jpg", new Guid("fef5d942-d69f-4dc5-9571-659c0efa7cc5"), true, false, false, null, "Global Utilities Customer Experience Scheme", "GLOBAL UTILITIES CUSTOMER EXPERIENCE SCHEME", "With the evolution of energy and water markets and the possibilities offered by digital transformation, utilities are developing new business models and are focusing on providing customers more interactive experiences. Smart metering, analytics, social media, mobility, cloud, and IoT are fundamentally impacting customer operation practices in both competitive and regulated markets. The Energy Insights: Worldwide Utilities Customer Experience Strategies service is designed to help utilities and energy retailers servicing customers in competitive and regulated markets at the worldwide level (including electricity, gas, and water). The service provides exclusive research and direct access to experts providing guidance to make the right IT investments and meet corporate objectives of customer satisfaction, reduction of cost to serve, and innovation. This service develops unique analysis and comprehensive data through Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from Energy Insights. Research reports elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("76352896-e547-485f-ae20-73b4f05b664f"), new Guid("ab8b06b4-b690-465e-b5d6-335844c6c2e4"), "76352896-e547-485f-ae20-73b4f05b664f", new DateTime(2020, 3, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "76352896-e547-485f-ae20-73b4f05b664f.jpg", new Guid("fef5d942-d69f-4dc5-9571-659c0efa7cc5"), true, false, false, null, "Asia/Pacific Oil and Gas Digital Transformation Procedure", "ASIA/PACIFIC OIL AND GAS DIGITAL TRANSFORMATION PROCEDURE", "Asia/Pacific Oil and Gas Digital Transformation Strategies examines the business environment of the oil and gas industry in the Asia/Pacific region, excluding Japan. The service covers the whole value chain from upstream, midstream, and downstream but is focused on upstream. It seeks to support companies working to deliver against efficiency objectives through technology-led change that will deliver improved decision support and insight and flexibility to adjust to changing technology, markets, and political pressures. The Energy Insights: Asia/Pacific Oil and Gas Digital Transformation Strategies program provides advice on best practices and technology priorities that will support technology decision making, particularly, in relation to innovation, data management, and greater integration across silos and processes. The service is designed to support oil and gas companies with market insights and road mapping advice relating to technology decision making, particularly delivering insight to how increased value from investments can be delivered through better understanding of new metric, organization, talent, and process change. Key technology focus areas of the program include IT/OT integration, operational cybersecurity, IoT strategies, and asset management priorities. This service is built on the foundations of extensive engagement and research by the Energy Insights team across the oil and gas industry in this region. Our analysts work with industry experts, staff from the oil and gas business, and technology vendors to ensure that the research service is relevant and prioritizes areas that are of importance to them and the industry at large." },
                    { new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d"), new Guid("888bf762-2fbf-4e0c-8b0d-17e0e958c8d9"), "a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d", new DateTime(2020, 4, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d.jpg", new Guid("fef5d942-d69f-4dc5-9571-659c0efa7cc5"), true, false, false, null, "Worldwide Mining Policy", "WORLDWIDE MINING POLICY", "Worldwide Mining Strategies examines the business environment across the mining sector value chain from exploration through to operations, processing, supply chain, and trading globally. Mining companies are operating in an increasingly competitive commodity market environment, driven by pressures relating to accessing funding, assets, and talent. Technology and data are playing an increasingly critical role across the operation to enable decision support, automation, integration, and control. This service provides comprehensive insights into the best practices that show how mining companies are responding and what road maps these companies will need to build the information technology (IT) capabilities required to create integrated, agile, and responsive operations. Mining companies are changing the way they buy technology, how they collaborate to frame the problems to be solved, how they engage with technology suppliers, and how they innovate across their businesses bringing together the capabilities of IT and operational technology (OT). This service tracks the IT investment priorities for organizations seeking to scale value creation across their organization and the impact on decision making and best practices relating to technology, process, and organizational change. This service distills market and industry data into incisive analysis drawn from in-depth interviews with industry experts, mining staff from across the business, and technology vendors. Insight and analysis are further supported and validated through rigorous research methodologies in quantitative market analysis.  Energy Insights' analysts develop unique and comprehensive analyses of this data, focused on providing actionable recommendations. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("11dcaf06-8a35-403a-8262-ebd98344039a"), new Guid("85c355c7-7026-495d-b585-3ea9d41eb256"), "11dcaf06-8a35-403a-8262-ebd98344039a", new DateTime(2020, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "11dcaf06-8a35-403a-8262-ebd98344039a.jpg", new Guid("8d00adfa-b32e-45c7-9a66-e70b8024c89f"), true, false, false, null, "Digital Commerce Trends", "DIGITAL COMMERCE TRENDS", "The Digital Commerce subscription marketing intelligence service examines the competitive landscape, key trends, and differentiating factors of digital commerce, PIM, and CPQ application vendors; digital marketplaces; and business commerce networks. This includes the buying behavior of end users while purchasing products and services on digital commerce platforms. The service provides a worldwide perspective by looking at all forms of digital commerce transactions — including B2B, B2C, C2C, G2B, and B2B2C — across all vertical industries and regional markets. Markets and Subjects Analyzed. Digital commerce applications targeting businesses of all sizes and industries. Best-of-breed digital commerce applications in areas such as CPQ, payments and billing, order management, web content management, merchandising, site search, fulfillment, product information management, and inventory management. Business commerce networks that bring together functional applications for enterprise asset management, procurement, financials, sales, and human capital management.Enterprise partnership/integration strategies among digital commerce and marketing/content management vendors. Experience management — across channels (includes content management, commerce platforms, integration, and advanced analytics) for B2B, B2C, and B2B2C. Impact of cloud, social, mobile, and Big Data technologies on vendor strategies for employee, customer, supplier, partner, and asset engagement. Proliferation of public cloud microservice architectures. Mobile commerce (mobile app marketplaces, built-for-mobile commerce capabilities, and differentiating technology). Cognitive technologies and intelligent workflow impact on digital commerce applications" },
                    { new Guid("67966830-655d-42f7-9424-ef5cf441ff8d"), new Guid("ab8b06b4-b690-465e-b5d6-335844c6c2e4"), "67966830-655d-42f7-9424-ef5cf441ff8d", new DateTime(2020, 5, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "67966830-655d-42f7-9424-ef5cf441ff8d.png", new Guid("8d00adfa-b32e-45c7-9a66-e70b8024c89f"), true, false, false, null, "Canadian Sales Accelerator: Datacenter Infrastructure", "CANADIAN SALES ACCELERATOR: DATACENTER INFRASTRUCTURE", "Canadian Sales Accelerator: Datacenter Infrastructure research program analyzes the Canadian datacenter (DC) infrastructure market in Canada. Designed to provide intelligence and strategic frameworks to technology sales professionals, field marketing teams, and channel managers to take action on key responsibilities related to the sales cycle, this service provides market sizing, vendor performance, forecasts, and market opportunities, as well as a variety of adjacent markets and segmentations from a quantitative standpoint. This advisory service will allow subscribers to identify opportunities tied to the future of infrastructure, tailor go-to-market strategies as well as product and services road maps to meet the growing demand of end users for infrastructure solutions. At the same time, it looks at vendors, partners, and customers from a qualitative standpoint, concerning needs and requirements, pain points and buying intentions, maturity levels, and adoption in and of new technologies. Markets and Subjects Analyzed Datacenter technologies (including software defined); server and storage; converged systems. Infrastructure ecosystem including partners and channels. Datacenter budget trends and dynamics and vendor selection/buying criteria. Technology investment expectations for legacy and next-gen DC infrastructure. In addition to the core research documents, clients will receive briefings and concise sales executive email alerts throughout the year. Every client will have a Sales Accelerator service launch integration meeting to kick-off the program. Core Research. Brand Perceptions on Enterprise Storage and Service Vendors. Market Forecasts. Vendor Dashboard: Market Shares. Infrastructure Ecosystem Barometer. Executive Market Insights" }
                });

            migrationBuilder.InsertData(
                table: "ReportTags",
                columns: new[] { "TagId", "ReportId" },
                values: new object[,]
                {
                    { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") },
                    { new Guid("887504de-d923-48a3-b8a9-09f9674bc08b"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") },
                    { new Guid("cee94eb5-b53a-4c62-85c5-ca30d4b8168a"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") },
                    { new Guid("887504de-d923-48a3-b8a9-09f9674bc08b"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") },
                    { new Guid("ea028850-416b-4f14-8204-950fcc54745d"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") },
                    { new Guid("a2970e11-3441-4928-b1ca-6db1ddd80ddc"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") },
                    { new Guid("580099be-781d-4263-823a-c49ca66f5fc7"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") },
                    { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") },
                    { new Guid("b0497d09-1bba-4970-afa7-db24ec1c3b30"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") },
                    { new Guid("a2970e11-3441-4928-b1ca-6db1ddd80ddc"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") },
                    { new Guid("b0497d09-1bba-4970-afa7-db24ec1c3b30"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") },
                    { new Guid("a95eedd0-bc4d-41cd-919d-e6b253528f91"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") },
                    { new Guid("580099be-781d-4263-823a-c49ca66f5fc7"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") },
                    { new Guid("a95eedd0-bc4d-41cd-919d-e6b253528f91"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") },
                    { new Guid("cee94eb5-b53a-4c62-85c5-ca30d4b8168a"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") },
                    { new Guid("b0497d09-1bba-4970-afa7-db24ec1c3b30"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") },
                    { new Guid("a2970e11-3441-4928-b1ca-6db1ddd80ddc"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") },
                    { new Guid("b0497d09-1bba-4970-afa7-db24ec1c3b30"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") },
                    { new Guid("a95eedd0-bc4d-41cd-919d-e6b253528f91"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") },
                    { new Guid("4311b6f9-7c6b-47ab-b087-505491437ab1"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") },
                    { new Guid("6838a923-7a12-45a7-b236-77c914fc1e48"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") },
                    { new Guid("a3695e8c-9ae2-425b-9894-0c5598fec5e2"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") },
                    { new Guid("6838a923-7a12-45a7-b236-77c914fc1e48"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") },
                    { new Guid("4311b6f9-7c6b-47ab-b087-505491437ab1"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") },
                    { new Guid("a95eedd0-bc4d-41cd-919d-e6b253528f91"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") },
                    { new Guid("6838a923-7a12-45a7-b236-77c914fc1e48"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") },
                    { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") },
                    { new Guid("6838a923-7a12-45a7-b236-77c914fc1e48"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") },
                    { new Guid("a3695e8c-9ae2-425b-9894-0c5598fec5e2"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") },
                    { new Guid("a95eedd0-bc4d-41cd-919d-e6b253528f91"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") },
                    { new Guid("71e41f22-2692-4aa8-9bca-28a9e3bcce58"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") },
                    { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") },
                    { new Guid("887504de-d923-48a3-b8a9-09f9674bc08b"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") },
                    { new Guid("23ea4c07-3ccb-49f1-8bda-608aab47e77a"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") },
                    { new Guid("299ba278-1365-4221-b2c8-4fb573ce946d"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") },
                    { new Guid("19427f4e-ec5c-4df9-8dcf-78596c8b4a52"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") },
                    { new Guid("b1909bc6-3cff-473e-92ca-1592204f1c84"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") },
                    { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") },
                    { new Guid("19427f4e-ec5c-4df9-8dcf-78596c8b4a52"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") },
                    { new Guid("299ba278-1365-4221-b2c8-4fb573ce946d"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") },
                    { new Guid("b1909bc6-3cff-473e-92ca-1592204f1c84"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") },
                    { new Guid("23ea4c07-3ccb-49f1-8bda-608aab47e77a"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") },
                    { new Guid("19427f4e-ec5c-4df9-8dcf-78596c8b4a52"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") },
                    { new Guid("23ea4c07-3ccb-49f1-8bda-608aab47e77a"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") },
                    { new Guid("299ba278-1365-4221-b2c8-4fb573ce946d"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") },
                    { new Guid("3779d426-b772-483c-8a2e-c0a5f8776dbd"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") },
                    { new Guid("72a0d8ed-736e-4e71-a11a-e0d3cbc1f98f"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") },
                    { new Guid("626d10b3-5fb7-4d7d-b217-2c4068432415"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") },
                    { new Guid("72a0d8ed-736e-4e71-a11a-e0d3cbc1f98f"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") },
                    { new Guid("feee373a-6385-4bda-9593-ca45d8e51e4e"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") },
                    { new Guid("72a0d8ed-736e-4e71-a11a-e0d3cbc1f98f"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") },
                    { new Guid("3779d426-b772-483c-8a2e-c0a5f8776dbd"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") },
                    { new Guid("72a0d8ed-736e-4e71-a11a-e0d3cbc1f98f"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") },
                    { new Guid("626d10b3-5fb7-4d7d-b217-2c4068432415"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") },
                    { new Guid("feee373a-6385-4bda-9593-ca45d8e51e4e"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") },
                    { new Guid("626d10b3-5fb7-4d7d-b217-2c4068432415"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") },
                    { new Guid("3779d426-b772-483c-8a2e-c0a5f8776dbd"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") },
                    { new Guid("72a0d8ed-736e-4e71-a11a-e0d3cbc1f98f"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") },
                    { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") },
                    { new Guid("ea028850-416b-4f14-8204-950fcc54745d"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") },
                    { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") },
                    { new Guid("71e41f22-2692-4aa8-9bca-28a9e3bcce58"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") },
                    { new Guid("a3695e8c-9ae2-425b-9894-0c5598fec5e2"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") },
                    { new Guid("4311b6f9-7c6b-47ab-b087-505491437ab1"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("19427f4e-ec5c-4df9-8dcf-78596c8b4a52"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("19427f4e-ec5c-4df9-8dcf-78596c8b4a52"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("19427f4e-ec5c-4df9-8dcf-78596c8b4a52"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("23ea4c07-3ccb-49f1-8bda-608aab47e77a"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("23ea4c07-3ccb-49f1-8bda-608aab47e77a"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("23ea4c07-3ccb-49f1-8bda-608aab47e77a"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("299ba278-1365-4221-b2c8-4fb573ce946d"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("299ba278-1365-4221-b2c8-4fb573ce946d"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("299ba278-1365-4221-b2c8-4fb573ce946d"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3779d426-b772-483c-8a2e-c0a5f8776dbd"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3779d426-b772-483c-8a2e-c0a5f8776dbd"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("3779d426-b772-483c-8a2e-c0a5f8776dbd"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("4311b6f9-7c6b-47ab-b087-505491437ab1"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("4311b6f9-7c6b-47ab-b087-505491437ab1"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("4311b6f9-7c6b-47ab-b087-505491437ab1"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("580099be-781d-4263-823a-c49ca66f5fc7"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("580099be-781d-4263-823a-c49ca66f5fc7"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("626d10b3-5fb7-4d7d-b217-2c4068432415"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("626d10b3-5fb7-4d7d-b217-2c4068432415"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("626d10b3-5fb7-4d7d-b217-2c4068432415"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("6838a923-7a12-45a7-b236-77c914fc1e48"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("6838a923-7a12-45a7-b236-77c914fc1e48"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("6838a923-7a12-45a7-b236-77c914fc1e48"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("6838a923-7a12-45a7-b236-77c914fc1e48"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("71e41f22-2692-4aa8-9bca-28a9e3bcce58"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("71e41f22-2692-4aa8-9bca-28a9e3bcce58"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("72a0d8ed-736e-4e71-a11a-e0d3cbc1f98f"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("72a0d8ed-736e-4e71-a11a-e0d3cbc1f98f"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("72a0d8ed-736e-4e71-a11a-e0d3cbc1f98f"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("72a0d8ed-736e-4e71-a11a-e0d3cbc1f98f"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("72a0d8ed-736e-4e71-a11a-e0d3cbc1f98f"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("887504de-d923-48a3-b8a9-09f9674bc08b"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("887504de-d923-48a3-b8a9-09f9674bc08b"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("887504de-d923-48a3-b8a9-09f9674bc08b"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a2970e11-3441-4928-b1ca-6db1ddd80ddc"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a2970e11-3441-4928-b1ca-6db1ddd80ddc"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a2970e11-3441-4928-b1ca-6db1ddd80ddc"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a3695e8c-9ae2-425b-9894-0c5598fec5e2"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a3695e8c-9ae2-425b-9894-0c5598fec5e2"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a3695e8c-9ae2-425b-9894-0c5598fec5e2"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a95eedd0-bc4d-41cd-919d-e6b253528f91"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a95eedd0-bc4d-41cd-919d-e6b253528f91"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a95eedd0-bc4d-41cd-919d-e6b253528f91"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a95eedd0-bc4d-41cd-919d-e6b253528f91"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("a95eedd0-bc4d-41cd-919d-e6b253528f91"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b0497d09-1bba-4970-afa7-db24ec1c3b30"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b0497d09-1bba-4970-afa7-db24ec1c3b30"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b0497d09-1bba-4970-afa7-db24ec1c3b30"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b0497d09-1bba-4970-afa7-db24ec1c3b30"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b1909bc6-3cff-473e-92ca-1592204f1c84"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("b1909bc6-3cff-473e-92ca-1592204f1c84"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cee94eb5-b53a-4c62-85c5-ca30d4b8168a"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("cee94eb5-b53a-4c62-85c5-ca30d4b8168a"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ea028850-416b-4f14-8204-950fcc54745d"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("ea028850-416b-4f14-8204-950fcc54745d"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("feee373a-6385-4bda-9593-ca45d8e51e4e"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") });

            migrationBuilder.DeleteData(
                table: "ReportTags",
                keyColumns: new[] { "TagId", "ReportId" },
                keyValues: new object[] { new Guid("feee373a-6385-4bda-9593-ca45d8e51e4e"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") });

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("0070a7dc-c760-4a34-8c87-d4747824191b"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("11dcaf06-8a35-403a-8262-ebd98344039a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("180360e0-6db8-412e-a6f4-3d68a939c583"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("4a817d53-485b-4634-9c09-22eb56d367fc"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("67966830-655d-42f7-9424-ef5cf441ff8d"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("76352896-e547-485f-ae20-73b4f05b664f"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("9062907e-6a91-4737-95f9-8db332df47f4"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("aba89aa6-e637-4518-83c1-125b625add12"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("cffed078-aa51-4fe2-913e-59e6e2624828"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("d1b04963-e698-41bf-aded-25bb755c2877"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a"));

            migrationBuilder.DeleteData(
                table: "Reports",
                keyColumn: "Id",
                keyValue: new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("19427f4e-ec5c-4df9-8dcf-78596c8b4a52"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("23ea4c07-3ccb-49f1-8bda-608aab47e77a"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("299ba278-1365-4221-b2c8-4fb573ce946d"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("3779d426-b772-483c-8a2e-c0a5f8776dbd"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("4311b6f9-7c6b-47ab-b087-505491437ab1"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("580099be-781d-4263-823a-c49ca66f5fc7"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("626d10b3-5fb7-4d7d-b217-2c4068432415"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("6838a923-7a12-45a7-b236-77c914fc1e48"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("71e41f22-2692-4aa8-9bca-28a9e3bcce58"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("72a0d8ed-736e-4e71-a11a-e0d3cbc1f98f"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("887504de-d923-48a3-b8a9-09f9674bc08b"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("a2970e11-3441-4928-b1ca-6db1ddd80ddc"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("a3695e8c-9ae2-425b-9894-0c5598fec5e2"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("a95eedd0-bc4d-41cd-919d-e6b253528f91"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("b0497d09-1bba-4970-afa7-db24ec1c3b30"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("b1909bc6-3cff-473e-92ca-1592204f1c84"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("cee94eb5-b53a-4c62-85c5-ca30d4b8168a"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("e4824e54-8a38-4ad8-8135-29a7980bfa5e"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("ea028850-416b-4f14-8204-950fcc54745d"));

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: new Guid("feee373a-6385-4bda-9593-ca45d8e51e4e"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("0f55f9f5-377b-4c4e-b719-202f42e362ed"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("85c355c7-7026-495d-b585-3ea9d41eb256"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("888bf762-2fbf-4e0c-8b0d-17e0e958c8d9"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("a0753d98-6ad6-4d34-a6f0-b11d097ba22a"));

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: new Guid("ab8b06b4-b690-465e-b5d6-335844c6c2e4"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("370779d0-f7d8-4414-9b1b-d0693026d864"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("65d09ef9-75cf-4dba-8696-b4cd6cca0d80"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("6febb9bc-fe8d-4fd4-9886-05f33623a7e3"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("8d00adfa-b32e-45c7-9a66-e70b8024c89f"));

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: new Guid("fef5d942-d69f-4dc5-9571-659c0efa7cc5"));

            migrationBuilder.AlterColumn<string>(
                name: "Summary",
                table: "Reports",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Reports",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Notifications",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Industries",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 30);

            migrationBuilder.AlterColumn<string>(
                name: "Bio",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 450,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 40);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 30);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("297d06e6-c058-486f-a18a-06a971ebfcd7"),
                column: "ConcurrencyStamp",
                value: "61ea6201-5a93-4e8f-801c-10d39a2b195d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("6c8fcd7e-62f6-4f3e-a73d-acbfd60b97ab"),
                column: "ConcurrencyStamp",
                value: "6ceb6a69-8c71-49fb-b74e-241e21fb197e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7f66990c-36bc-4381-9f81-32e06e168319"),
                column: "ConcurrencyStamp",
                value: "d7501165-fe55-4cce-9998-a2b43099d710");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb93"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "ceb80441-f201-480c-92e6-9a34b684f2d6", new DateTime(2020, 6, 2, 16, 21, 0, 49, DateTimeKind.Utc).AddTicks(2702), "AQAAAAEAACcQAAAAEML8dECQ7E9omuEvvByApBOjYOTQpuDZFTqa6MfgeLGJ9pwcwWBJJnUXAt/udP4/cg==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb94"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "6f22af66-1d3f-4494-9b16-c2a32fb8fec4", new DateTime(2020, 6, 2, 16, 21, 0, 49, DateTimeKind.Utc).AddTicks(4769), "AQAAAAEAACcQAAAAEPqtK52nOxfhBnTd2s1JZOvQd3dMXamw8JratdMW+SGX1G8m7pJjrGN+vIkU7Z6xuA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb95"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "97387552-37b5-4ca8-899f-bfcd670e9936", new DateTime(2020, 6, 2, 16, 21, 0, 49, DateTimeKind.Utc).AddTicks(4796), "AQAAAAEAACcQAAAAEEcBEXs8rGXVVk0v4yAv73LN6ZPI+e2nVSt9eHfQ8Dvw+m0HyRCAUGweEnAdnJvOjA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb96"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "aafc66d1-a044-44a3-a1c2-3cad73b3d1a9", new DateTime(2020, 6, 2, 16, 21, 0, 49, DateTimeKind.Utc).AddTicks(4803), "AQAAAAEAACcQAAAAEFH2yORKYd7p33IB7WK8ZlmYtwOoxyLudxf9m1W/vR7GQKBfL5TY4zX0B6tlBneFrQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb97"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "0707b5a0-c1f1-46f7-8e68-70fa8c47e93d", new DateTime(2020, 6, 2, 16, 21, 0, 49, DateTimeKind.Utc).AddTicks(4824), "AQAAAAEAACcQAAAAEDA49Km4LIcA54qt0BWLvVWqzf8P40hbnsLPriIMUqqCeEJx3EcMLELkmbGJjNYpGQ==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb98"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "a68309db-12a4-4635-987e-a13d3a24f3c0", new DateTime(2020, 6, 2, 16, 21, 0, 49, DateTimeKind.Utc).AddTicks(4830), "AQAAAAEAACcQAAAAEBRXcqk/zZ59Q505mPglO7LgE5RyHyXpZ49xjZVBGxsm7xTDfC/gKG6/BLHIP+NfVQ==" });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "Bio", "ImagePath", "UserId" },
                values: new object[,]
                {
                    { new Guid("8d223ce3-389a-4c03-8a5d-51c2d3e79bde"), "Jim Doe has been involved with ICT research in Africa since 1997 and has participated in diverse research projects in 14 African countries. He started his career in 1994 as a public relations officer with Software Technologies Limited (an East African Oracle distributor), before serving as a research analyst/project officer for Telecom Forum Africa in 1997.", "3.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb94") },
                    { new Guid("c4417156-6782-43a7-9d3c-127699043dec"), "Rosa James has a background in journalism and has been published in various local, regional, and international magazines and newspapers. She has spoken at various industry events across sub-Saharan Africa including countries like Ethiopia, Ghana, Kenya, Nigeria, Rwanda, Tanzania and Zambia as well as leading and facilitating at IDC’s events in the region. Rose is also a published author of a children’s novel.", "1.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb95") },
                    { new Guid("c94d7746-8c70-4b7b-8910-489f643ea7f4"), "Cate Williams has been an analyst InsightHub for several years. Early on as an analyst she covered CRM applications, but more recently her work has been in integration middleware covering markets such as API management, file sync and share, and B2B integration.", "2.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb96") },
                    { new Guid("b13ece8a-9eb4-49e0-b4eb-6bbf7cd6e9e6"), "Max Upton has more than 15 years of experience in business research, focusing on financial services and business innovation. His consulting work for InsightHub has allowed him to work closely with leading banks and regulators in the region for their technology and innovation strategies. Mr. Upton is concurrently head of InsightHub's business and operations in Thailand.", "7.png", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb97") },
                    { new Guid("dbdd3bd0-5cac-496c-8133-49ac43b94e47"), "Previously, David Scott served for ten years as Enterprise Architect at CareFirst Blue Cross/Blue Shield, responsible for building business and technology alignment roadmaps for the ACA/HIX Health Reform, Mandated Implementations, PCMH, and Provider Network domains to ensure alignment of annual and tactical IT project planning with business goals.", "8.jpg", new Guid("7bd06fe6-79ca-43a1-862b-446a1466bb98") }
                });

            migrationBuilder.InsertData(
                table: "Industries",
                columns: new[] { "Id", "DeletedOn", "ImagePath", "IsDeleted", "ModifiedOn", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("1665f048-f08c-47a7-a044-e7122b79e93e"), null, null, false, null, "Energy", "ENERGY" },
                    { new Guid("73cd65a0-dedd-4b14-80a0-9766d5808165"), null, null, false, null, "Information Technology", "INFORMATION TECHNOLOGY" },
                    { new Guid("eda7936e-987b-4c67-ac6d-242b38c45e7f"), null, null, false, null, "Healthcare", "HEALTHCARE" },
                    { new Guid("32a66306-6e1d-4ac6-bc93-a29c78a1efbc"), null, null, false, null, "Finance", "FINANCE" },
                    { new Guid("a6f963a9-15bd-4c72-903c-ea64adfe34f2"), null, null, false, null, "Marketing", "MARKETING" }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "DeletedOn", "IsDeleted", "ModifiedOn", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("91893923-a104-4a3c-ad98-a9487896f066"), null, false, null, "computers", "COMPUTERS" },
                    { new Guid("d54acd8a-4af1-4f65-9e6b-1ad68315c987"), null, false, null, "hospital", "HOSPITAL" },
                    { new Guid("3604c205-28eb-45e2-a029-f38e1c206c8a"), null, false, null, "influence", "INFLUENCE" },
                    { new Guid("bf6b1c50-38fc-4bb4-8a5b-09f075f46d17"), null, false, null, "trend", "TREND" },
                    { new Guid("7ab98bef-40c1-4a1b-bf1a-88911ae38852"), null, false, null, "sales", "SALES" },
                    { new Guid("36533151-9264-41de-836e-b58d3dbfb364"), null, false, null, "profit", "PROFIT" },
                    { new Guid("7c0fdd96-f11f-4fe8-b811-90577d20ff2f"), null, false, null, "insurance", "INSURANCE" },
                    { new Guid("d6f523c1-8247-400e-abf3-5ca85575ccfd"), null, false, null, "stock", "STOCK" },
                    { new Guid("8726cd04-1394-4f69-a016-2a80f15c6238"), null, false, null, "nuclear", "NUCLEAR" },
                    { new Guid("5179e8fb-52d0-49b4-841f-aae3c0bd086e"), null, false, null, "solar", "SOLAR" },
                    { new Guid("649c608d-e658-4a0c-ab24-7e35f36e409a"), null, false, null, "oil", "OIL" },
                    { new Guid("55553fa1-4dfd-48c7-abbb-346f30d9d47e"), null, false, null, "laboratory", "LABORATORY" },
                    { new Guid("9282060a-bca0-4d10-8546-601862379082"), null, false, null, "vaccine", "VACCINE" },
                    { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), null, false, null, "research", "RESEARCH" },
                    { new Guid("ce36a51a-9b79-45a9-beeb-40fa09a77d4f"), null, false, null, "medicine", "MEDICINE" },
                    { new Guid("5522d757-5cb0-4f10-adb2-5d472bbd79b0"), null, false, null, "web", "WEB" },
                    { new Guid("a73af8ad-cd30-411e-bf70-f7af80b91018"), null, false, null, "hardware", "HARDWARE" },
                    { new Guid("cd64cb0b-f2a3-4b4b-9284-d6badceedcdb"), null, false, null, "software", "SOFTWARE" },
                    { new Guid("e7c3cd21-b001-4f84-b724-04ab0ce10a9d"), null, false, null, "investment", "INVESTMENT" },
                    { new Guid("ce01eae4-256c-46e9-b42d-98f7e7602218"), null, false, null, "gas", "GAS" }
                });

            migrationBuilder.InsertData(
                table: "Reports",
                columns: new[] { "Id", "AuthorId", "Content", "CreatedOn", "DeletedOn", "ImagePath", "IndustryId", "IsApproved", "IsDeleted", "IsFeatured", "ModifiedOn", "Name", "NormalizedName", "Summary" },
                values: new object[,]
                {
                    { new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb"), new Guid("dbdd3bd0-5cac-496c-8133-49ac43b94e47"), "3d360ea3-0587-4e87-9eeb-07f7a429b6cb", new DateTime(2020, 3, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "3d360ea3-0587-4e87-9eeb-07f7a429b6cb.jpg", new Guid("1665f048-f08c-47a7-a044-e7122b79e93e"), true, false, false, null, "Worldwide Utilities Connected Asset", "WORLDWIDE UTILITIES CONNECTED ASSET", "Energy and digital technologies are radically transforming utilities asset operations and strategies. Renewables and decentralized power generation, energy delivery networks (electricity, gas, and heat), and water and waste water management benefit from the developments and adoption of IoT, AI, and digital twins.  Energy Insights: Worldwide Utilities Connected Asset Strategies service focuses on all these. It provides guidance to end users in terms of digital transformation (DX) use cases road map, analysis of emerging IT trends, and evaluation of technology providers in this space. It looks into the future of work in the utilities value chain context. At the same time, it provides technology vendors a view on utilities' operational excellence asset strategies addressing their go to market.This service develops unique analysis and comprehensive data through  Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from both  and  Energy Insights. Research documents elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research documents." },
                    { new Guid("9062907e-6a91-4737-95f9-8db332df47f4"), new Guid("b13ece8a-9eb4-49e0-b4eb-6bbf7cd6e9e6"), "9062907e-6a91-4737-95f9-8db332df47f4", new DateTime(2020, 5, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "9062907e-6a91-4737-95f9-8db332df47f4.jpg", new Guid("a6f963a9-15bd-4c72-903c-ea64adfe34f2"), true, false, false, null, "CMO Advisory Service", "CMO ADVISORY SERVICE", "CMO Advisory Service guides marketing leaders as they master the science of marketing. Digital transformation offers CMOs the opportunity to become revenue drivers and architects of the customer experience. Leaders leverage 's deep industry knowledge, powerful quantitative models, peer-tested practices, and personalized guidance to advance their operations. Whether seeking fresh perspectives on core challenges or counsel on emerging developments, offers a trusted source of insight.Markets and Subjects Analyzed. Marketing Investment and Transformational Operations. Planning and budgeting investments in the marketing mix, marketing technology, and marketing operations, accountability, and attribution. Application of New Technology for Marketing. Guidance on the implications of new technologies such as artificial intelligence (AI), collaboration, and marketing automation solutions. Delivering the New Customer Experience. Mapping and responding to the B2B customer decision journey. The Future Marketing Organization. Organizational design, staff allocation benchmarks, role definition, talent development, shared services, organizational alignment, and change management. Developing core competencies: Content marketing, customer intelligence and analytics, integrated digital and social engagement, sales enablement, and loyalty and advocacy" },
                    { new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7"), new Guid("dbdd3bd0-5cac-496c-8133-49ac43b94e47"), "90c09b29-4745-484f-b5d9-f24696e4acc7", new DateTime(2020, 5, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "90c09b29-4745-484f-b5d9-f24696e4acc7.png", new Guid("a6f963a9-15bd-4c72-903c-ea64adfe34f2"), true, false, false, null, "Marketing and Sales Solutions", "MARKETING AND SALES SOLUTIONS", "Marketing and sales technologies are driving forces for all companies as customers move to online, mobile-first, and collaborative relationship models. In reaching and selling to customers, it is essential that organizations aggressively develop the necessary operational and analytic skills, collaborative cultures, and creative problem solving needed to truly add value to the customer relationship. 's Marketing and Sales Solutions service provides strategic frameworks for thinking about the individual and aligned areas of marketing and sales technology as parts of a holistic business strategy. This program delivers insight, information, and data on the main drivers for the adoption of these technologies in the broader context of customer experience (CX) and networked business strategies.Markets and Subjects Analyzed. Marketing automation, campaign management, and go to market execution applications. Sales force automation applications. Predictive analytics and business KPIs. Mobile and digital applications and strategies. Customer data and analytics. CX strategies. Core Research. Reports on how 670 U.S. large enterprises use more than 300 vendors in 15 martech categories across 5 vertical markets:Consumer banking; Retail; CPG manufacturing; Securities and investment services; Travel and hospitality; MarketScape(s) on related solutions areas such as marketing clouds and artificial intelligence; TechScapes and PlanScapes on various topics such as GDPR, account-based marketing, personalization, and mobile marketing; Marketing software forecast and vendor shares; Sales force automation forecast and vendor shares. In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. " },
                    { new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02"), new Guid("b13ece8a-9eb4-49e0-b4eb-6bbf7cd6e9e6"), "7af1f20b-db8a-4eb0-b484-eb03487efb02", new DateTime(2020, 5, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "7af1f20b-db8a-4eb0-b484-eb03487efb02.jpg", new Guid("a6f963a9-15bd-4c72-903c-ea64adfe34f2"), true, false, false, null, "Customer Experience Management Strategies", "CUSTOMER EXPERIENCE MANAGEMENT STRATEGIES", "Customer Experience Management Strategies SIS provides a framework and critical knowledge for understanding the changing nature of the customer experience (CX) and guides chief experience officers and their organizations as they master the digital transformation of the customer experience. This product covers the concepts of experience management and customer experience, looking at how digital transformation is driving change to customer expectations, preferences, and behavior and how enterprises must adopt new technologies to meet these urgent challenges. Markets and Subjects Analyzed: Experience management; Customer-centric engagement Customer-centric operations; Customer-centric solution design; Impact of 3rd Platform technologies on customer experience; Intelligence and analytics-driven customer experience; Customer experience along the customer journey; Customer experience benchmarks Core Research: Customer Experience Taxonomy; Digital Transformation of the Customer Experience; Redefining Experience Management; PeerScape: Customer Experience; Customer Experience in an Algorithm Economy; Technology-Enabled Storytelling; MaturityScape: Customer Experience; Customer Intelligence and Analytics; Innovation Accelerators in Customer Experience: Artificial Intelligence; In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef"), new Guid("dbdd3bd0-5cac-496c-8133-49ac43b94e47"), "3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef", new DateTime(2020, 5, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef.jpg", new Guid("a6f963a9-15bd-4c72-903c-ea64adfe34f2"), true, false, false, null, "Retail Insights: European Retail Digital Transformation Strategies", "RETAIL INSIGHTS: EUROPEAN RETAIL DIGITAL TRANSFORMATION STRATEGIES", "European retailing is in a state of change since digital technologies have become so pervasive in the retail journey and consumer expectations, in terms of customer experience, with their preferred brands rapidly evolving. European retailers are adapting to these changes through the adoption of commerce everywhere business models and technology. Retailers from across Europe are in the process of determining how digital impacts them and what their digital transformation approach and strategy should be. This reveals a more and more complex and highly differentiated European region, where differences exist and persist across countries as well as subsegments (fashion and apparel, department stores, food and grocery, etc.). Retail Insights is witnessing among European retailers, at varying degrees of maturity, a race to digitize because of the potential new revenue streams and retail operational efficiencies that can be derived. Retail Insights: European Retail Digital Transformation Strategies advisory service examines the impact of digital transformation on the European retailers' business, technology, and organizational areas. Specific coverage is given to provide valuable insights into the European retail industry, with a specific focus on digital transformation strategies applied by retail companies to improve the omni-channel customer experience. This advisory service develops unique analysis and comprehensive data through Retail Insights' proprietary research projects, along with ongoing communications with industry experts, retail CIOs, and line-of-business executives, and ICT product and service vendors. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports. Our analysts are also available to provide personalized advice for retail executives and ICT vendors to help them make better-informed decisions." },
                    { new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97"), new Guid("c4417156-6782-43a7-9d3c-127699043dec"), "3a4b1c09-e5b7-4246-843c-bca16a106b97", new DateTime(2020, 3, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "3a4b1c09-e5b7-4246-843c-bca16a106b97.jpg", new Guid("32a66306-6e1d-4ac6-bc93-a29c78a1efbc"), true, false, false, null, "Consumer Banking Engagement Strategies", "CONSUMER BANKING ENGAGEMENT STRATEGIES", "Being successful in banking will be determined by how well institutions manage the transformation in both digital and physical channels. Customers have seemingly ubiquitous access to their accounts on their terms and on their devices as banks continue to strategize about the future of the branch network and new channels emerge. Unfortunately, many banks are still looking at their channel strategy in a silo without fully understanding that customer engagement is the key to a profitable relationship. Today’s technology has fostered this customer-led revolution, yet there are many more changes yet to be realized as new technology is introduced. Advances in how we engage the customer are pushing the limits and skill sets of business units, marketers, and IT personnel as customers demand more from their retail bank. The Financial Insights: Consumer Banking Engagement Strategies provides critical analysis of the opportunities and options facing banks as they wrestle with their technology plans and investment decisions in alignment with their strategic goals. This research delivers key insights regarding the business drivers of and value delivered from customer-facing banking technology investments. Topics Addressed Throughout the year, this service will address the following topics: Digital transformation: Strategies and use cases are developing as banks transform all channels, including account opening and onboarding, ATM and ITM, augmented and virtual reality, branch banking, call center, chatbot services, contextualized marketing, conversational banking, digital banking (online and mobile), and social business. Whether these are first-generation offerings or have been around for decades, strategies need to be developed to implement, support, and upgrade these channels to stay with the times. Engagement strategies: Financial institutions are realizing that the number of engagements a customer has is an important factor in profitability. Using big data and analytics to properly measure the number of engagements is a start, but most institutions need to go beyond a prescriptive approach to customer behavior to a more cognitive approach. Omni-experience: The customer life-cycle process offers multiple channels that define the experience and dictate current and future relationships. Customer trends and strategies: This includes topics from level of interaction today to what is likely to be future behavior." },
                    { new Guid("d1b04963-e698-41bf-aded-25bb755c2877"), new Guid("dbdd3bd0-5cac-496c-8133-49ac43b94e47"), "d1b04963-e698-41bf-aded-25bb755c2877", new DateTime(2020, 2, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "d1b04963-e698-41bf-aded-25bb755c2877.jpg", new Guid("32a66306-6e1d-4ac6-bc93-a29c78a1efbc"), true, false, false, null, "Asia/Pacific Financial Services IT", "ASIA/PACIFIC FINANCIAL SERVICES IT", "Financial Insights: Asia/Pacific Banking Customer Centricity program provides insights into the evolving needs of Asia/Pacific retail/consumer banking retail customers and guidelines on how banks are to respond to these trends. The program will give advice to technology buyers on the technology that supports the customer centricity agenda of the financial institution – particularly in the areas of customer relationship management (CRM), omni-experience and omni-channel solutions, and in loyalty management. The service will be backed by a comprehensive consumer survey on the preferences of Asia/Pacific retail/consumer banking customers in various aspects of customer experience. Financial Insights will then undertake on how financial institutions are to develop an effective customer management agenda, delving into the concept, approach, strategy, use cases, and enabling technologies for customer centricity. Throughout the year, this service will address the following topics: Customer Experience Trends in Asia/Pacific Retail Banking How Asia/Pacific Consumers Pay - Evolving Trends in Retail Payments Customer Centricity in Retail Banking IT and Operations Customer Centricity in Marketing and Product Development Customer Centricity Technology Providers for Asia/Pacific Retail Banking." },
                    { new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015"), new Guid("c4417156-6782-43a7-9d3c-127699043dec"), "bcaede74-79bf-4147-8d5b-46f161f4d015", new DateTime(2020, 4, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "bcaede74-79bf-4147-8d5b-46f161f4d015.jpg", new Guid("32a66306-6e1d-4ac6-bc93-a29c78a1efbc"), true, false, false, null, "Universal Payment Strategies", "UNIVERSAL PAYMENT STRATEGIES", "The payment industry has seen drastic changes in the past decade. New technologies, entrants, and business models have forced incumbent vendors and their financial institution customers to rethink how they move money. Stakeholders across the payment value chain — card-issuing banks, merchant acquirers, payment networks, and payment processors — face increasingly complex decisions. In this turbulent market, the players need more than facts and figures; they need critical analysis and insightful opinions. Markets and Subjects Analyzed Throughout the year, this service will address the following topics: Developing trends in payments such as omni-channel and alternative payment networks Evaluation and integration of new payment channels like voice commerce and IoT Enterprise risk, compliance, and fraud issues affecting payment products Legal and regulatory issues around the world that will affect how payments develop Middle- and back-office technologies that will affect the payment strategies of financial institutions Emerging technologies such as blockchain, AI, and next-generation security and their potential for altering the payment landscape Core Research MarketScape: Gateways for Integrated Payments B2B Payments: Digital Transformation in Transactions Retail Revolution: Beyond Card Payments Blockchain Payments: Short-Term Realities Persistent Payments: Automated Transactions in a Connected World Real-Time Payment Productization: Overlays in Action Retail Fraud: Protecting a Multi-Payment Environment" },
                    { new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a"), new Guid("dbdd3bd0-5cac-496c-8133-49ac43b94e47"), "3dc823d3-91e3-40c6-b141-2c25ed9cd64a", new DateTime(2020, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "3dc823d3-91e3-40c6-b141-2c25ed9cd64a.jpg", new Guid("32a66306-6e1d-4ac6-bc93-a29c78a1efbc"), true, false, false, null, "Worldwide Banking IT Spending Guide", "WORLDWIDE BANKING IT SPENDING GUIDE", "The Financial Insights: Worldwide Banking IT Spending Guide examines the banking industry opportunity from a technology, functional process, and geography perspective. This comprehensive database delivered via 's Customer Insights query tool allows the user to easily extract meaningful information about the banking technology market by viewing data trends and relationships and making data comparisons. Markets Covered This product covers the following segments of the banking market: 9 regions: USA, Canada, Japan, Western Europe, Central and Eastern Europe, Middle East and Africa, Latin America, PRC, and Asia/Pacific 4 technologies: Hardware, software, services, and internal IT spend 5 banking segments: Consumer banking, corporate and institutional banking, corporate administration, enterprise utilities, and shared services 30+ functional processes: Channels, payments, core processing, and more 4 company size tiers: Institution size by tier 1 through tier 4 2 institution types: Banks and credit unions 6 years of data Data Deliverables This spending guide is delivered on a semiannual basis via a web-based interface for online querying and downloads. For a complete delivery schedule, please contact an sales representative. The following are the deliverables for this spending guide: Annual five-year forecasts by regions, technologies, banking segments, functional processes, tiers, and institution types; delivered twice a year About This Spending Guide Financial Insights: Worldwide Banking IT Spending Guide provides guidance on the expected technology opportunity around this market at a regional and total worldwide level. Segmented by functional process, institution type, company size tier, region, and technology component, this guide provides IT vendors with insights into both large and rapidly growing segments of the banking technology market and how the market will develop over the coming years." },
                    { new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82"), new Guid("c94d7746-8c70-4b7b-8910-489f643ea7f4"), "daaf7eab-fc1c-4a84-a8bc-3d857def3f82", new DateTime(2020, 3, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "daaf7eab-fc1c-4a84-a8bc-3d857def3f82.jpg", new Guid("32a66306-6e1d-4ac6-bc93-a29c78a1efbc"), true, false, false, null, "Insurance Digital Transformation Approach", "INSURANCE DIGITAL TRANSFORMATION APPROACH", "Customer experience is high on the agenda for most insurers and intermediaries as customers expect true value for the premiums they pay, above and beyond the traditional products and services delivered to them. To continue to be relevant in a changing marketplace, insurance organizations across the globe need to deliver contextual and value-centric insurance that is rooted on the principles of proactive risk management and secure, transparent, seamless, and contextual engagements across the customer journey. To achieve this, the industry needs to accelerate its effort to transform from a traditional, product-centric mindset to a customer-centric mindset enabled by digital technologies and the power of data and analytics. Insurance organizations need to increasingly play the role of true risk advisors rather than mere product sellers. As they progress in their transformation journey, they need advice and guidance to understand the opportunities and possibilities with the new technologies and to build and deploy digital capabilities while also tackling existing barriers to change. The Financial Insights: Worldwide Insurance Digital Transformation Strategies advisory service provides clients with insightful information and analysis of global insurance trends. It also provides coverage of how digital technologies like Big Data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies impact the life and annuity, accident and health, and property and casualty insurance markets. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of insurance organizations as well as technology vendors." },
                    { new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37"), new Guid("8d223ce3-389a-4c03-8a5d-51c2d3e79bde"), "0daebcfa-c6a5-44f3-8193-25c442f9ec37", new DateTime(2020, 5, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "0daebcfa-c6a5-44f3-8193-25c442f9ec37.jpg", new Guid("32a66306-6e1d-4ac6-bc93-a29c78a1efbc"), true, false, false, null, "International Corporate Banking Digital Transformation", "INTERNATIONAL CORPORATE BANKING DIGITAL TRANSFORMATION", "With growing competition from nonbank platforms and the commoditization of products, the ability to onboard, connect, and deliver services to corporate customers is becoming paramount. With PSD2 in Europe democratizing data access and payment services, corporate banks have to transform their value proposition to deliver new data-driven services to their clients and add value, rather than their traditional, commoditized product proposition. Reducing the connectivity cost, increasing speed of data delivery toward real time, and integrating bank systems with corporate enterprise resource planning (ERP), treasury management systems (TSM), payment factories, and in-house banks will be paramount to help corporate clients manage capital, monitor cash flows, and optimize their liquidity. These infrastructure upgrades will also be crucial to exploit the evolving ecosystems brought about by distributed ledger technology (DLT) and the internet of things (IoT), where data discovery, analysis, and sharing will accelerate trade and reduce risk. Sensors attached to goods in transit — from the manufacturing plant to the retail outlet — could offer opportunities to banks' cash management and trade services businesses, better matching flows of payments and goods between seller and buyer. The Financial Insights: Worldwide Corporate Banking Digital Transformation Strategies advisory service provides clients with insightful information and analysis of corporate and commercial banking trends, including cash and treasury, trade finance, commercial lending, and payments. It also provides coverage of how the impact of emerging 3rd Platform technologies like big data, analytics, cloud, mobility, IoT, blockchain, and cognitive technologies transforms the sector and how their convergence unlocks new business and operating models. The service offers timely, strategic, and actionable business IT and application advice to guide our clients as they undertake digital investment decisions, plan for future technology needs, and benchmark themselves against competitors. It provides ongoing research reports, analyst access, and admission to content-rich conferences and webcasts for the benefit of financial institutions as well as technology vendors." },
                    { new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0"), new Guid("c4417156-6782-43a7-9d3c-127699043dec"), "698b544c-d07c-41d1-9fad-20dabb68c7b0", new DateTime(2020, 5, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "698b544c-d07c-41d1-9fad-20dabb68c7b0.jpg", new Guid("eda7936e-987b-4c67-ac6d-242b38c45e7f"), true, false, false, null, "Health systems and universal health coverage", "HEALTH SYSTEMS AND UNIVERSAL HEALTH COVERAGE", "In the SDG monitoring framework, progress towards universal health coverage (UHC) is tracked with two indicators: (i) a service coverage index (which measures coverage of selected essential health services on a scale of 0 to 100); and (ii) the proportion of the population with large out-of-pocket expenditures on health care (which measures the incidence of catastrophic health spending, rendered as percentage). The service coverage index improved from 45 globally in 2000 to 66 in 2017, with the strongest increase in low-and lower-middle-income countries, where the baseline at 2000 was lowest. However, the pace of that progress has slowed since 2010. The improvements are especially notable for infectious disease interventions and, to a lesser extent, for reproductive, maternal and child health services. Within countries, coverage of the latter services is typically lower in poorer households than in richer households. Overall, between one third and one half the world’s population (33% to 49%) was covered by essential health services in 2017 . Service coverage continued to be lower in low- and middle-income countries than in wealthier ones; the same held for health workforce densities and immunization coverage (Figure 1.2). Available data indicate that over 40% of all countries have fewer than 10 medical doctors per 10 000 people, over 55% have fewer." },
                    { new Guid("180360e0-6db8-412e-a6f4-3d68a939c583"), new Guid("8d223ce3-389a-4c03-8a5d-51c2d3e79bde"), "180360e0-6db8-412e-a6f4-3d68a939c583", new DateTime(2020, 2, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "180360e0-6db8-412e-a6f4-3d68a939c583.jpg", new Guid("eda7936e-987b-4c67-ac6d-242b38c45e7f"), true, false, false, null, "Investing in strengthening country health information systems", "INVESTING IN STRENGTHENING COUNTRY HEALTH INFORMATION SYSTEMS", "Accurate, timely, and comparable health-related statistics are essential for understanding population health trends. Decision-makers need the information to develop appropriate policies, allocate resources and prioritize interventions. For almost a fifth of countries, over half of the indicators have no recent primary or direct underlying data. Data gaps and lags prevent from truly understanding who is being included or left aside and take timely and appropriate action. The existing SDG indicators address a broad range of health aspects but do not capture the breadth of population health outcomes and determinants. Monitoring and evaluating population health thus goes beyond the indicators covered in this report and often requires additional and improved measurements. WHO is committed to supporting Member States to make improvements in surveillance and health information systems. These improvements will enhance the scope and quality of health information and standardize processes to generate comparable estimates at the global level. Getting accurate data on COVID-19 related deaths has been a challenge. The COVID-19 pandemic underscores the serious gaps in timely, reliable, accessible and actionable data and measurements that compromise preparedness, prevention and response to health emergencies. The International Health Regulations (IHR) (2005) monitoring framework is one of the data collection tools that have demonstrated value in evaluating and building country capacities to prevent, detect, assess, report and respond to public health emergencies. From self-assessment of the 13 core capacities in 2019, countries have shown steady progress across almost all capacities including surveillance, laboratory and coordination. As the pandemic progresses, objective and comparable data are crucial to determine the effectiveness of different national strategies used to mitigate and suppress, and thus to better prepare for the probable continuation of the epidemic over the next year or more." },
                    { new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2"), new Guid("c4417156-6782-43a7-9d3c-127699043dec"), "044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2", new DateTime(2020, 3, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2.jpg", new Guid("eda7936e-987b-4c67-ac6d-242b38c45e7f"), true, false, false, null, "Maternal mortality has declined but progress is uneven across regions", "MATERNAL MORTALITY HAS DECLINED BUT PROGRESS IS UNEVEN ACROSS REGIONS", "A total of 295 000 [UI 1 80%: 279 000–340 000] women worldwide lost their lives during and following pregnancy and childbirth in 2017, with sub-Saharan Africa and South Asia accounting for approximately 86% of all maternal deaths worldwide. The global maternal mortality ratio (MMR, the number of maternal deaths per 100 000 live births) was estimated at 211 [UI 80%: 199–243], representing a 38% reduction since 2000. On average, global MMR declined by 2.9% every year between 2000 1 UI = uncertainty interval. and 2017. If the pace of progress accelerates enough to achieve the SDG target (reducing global MMR to less than 70 per 100 000 live births), it would save the lives of at least one million women. The majority of maternal deaths are preventable through appropriate management of pregnancy and care at birth, including antenatal care by trained health providers, assistance during childbirth by skilled health personnel, and care and support in the weeks after childbirth. Data from 2014 to 2019 indicate that approximately 81% of all births globally took place in the presence of skilled health personnel, an increase from 64% in the 2000–2006 period. In sub-Saharan Africa, where roughly 66% of the world’s maternal deaths occur, only 60% of births were assisted by skilled health personnel during the 2014–2019 period. " },
                    { new Guid("0070a7dc-c760-4a34-8c87-d4747824191b"), new Guid("8d223ce3-389a-4c03-8a5d-51c2d3e79bde"), "0070a7dc-c760-4a34-8c87-d4747824191b", new DateTime(2020, 5, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "0070a7dc-c760-4a34-8c87-d4747824191b.jpg", new Guid("eda7936e-987b-4c67-ac6d-242b38c45e7f"), true, false, false, null, "European Value-Based Healthcare Digital Transformation", "EUROPEAN VALUE-BASED HEALTHCARE DIGITAL TRANSFORMATION", " With the advent of the value-based paradigm, healthcare organizations are today tasked to ensure equitable access and financial sustainability while improving and reducing the variation of clinical outcomes in healthcare systems. Increasing collaboration across the healthcare value chain, adopting a more personalized approach to treatment, championing the patient experience, and adopting outcome-based business models are fast emerging as necessary for the healthcare industry to succeed. The IDC Health Insights: European Value-Based Healthcare Digital Transformation Strategies service focuses on analyzing European healthcare providers, payers, and public health policy makers' digital strategy and best practices in supporting the adoption of value-based healthcare to deliver the maximum value to patients. Value-based healthcare is a key driver of digital transformation as it requires an unprecedented level of availability of patient information. Key solutions as HIE, population health management, smart patient engagement tools, advanced analytics and cognitive, and cloud and mobility will fundamentally impact care providers and payers' value-based healthcare strategies execution and success. Approach. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244"), new Guid("c4417156-6782-43a7-9d3c-127699043dec"), "e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244", new DateTime(2020, 5, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244.jpg", new Guid("eda7936e-987b-4c67-ac6d-242b38c45e7f"), true, false, false, null, "European Digital Hospital Evolution", "EUROPEAN DIGITAL HOSPITAL EVOLUTION", "European Digital Hospital service provides detailed information about the evolution of digital hospital transformation and technology adoption. It offers valuable insights into IT solution trends within the hospital sector, including healthcare-specific technologies such as electronic health records; admission, discharge, and transfer; digital medical imaging systems; VNA and archiving; and business intelligence and patient management systems. This service focuses on a vital component of healthcare — the digital hospital. Healthcare systems across the world are facing care quality and sustainability challenges that require a new approach to care delivery and reimbursement models. The fully digital hospital is in the center of that journey, being a key enabler of change. The hospital CIOs are facing new technology but, at the same time, a growing legacy IT burden that must be managed alongside of new digital initiatives. In healthcare, management of legacy systems is therefore a growing challenge — when looking at not only obsolete digital applications but also infrastructure and, in the end, how healthcare providers purchase digital applications and platforms. When IT fails to deliver because of legacy and lack of agility, the business model and clinical processes become legacy as well. Key solutions such as EHR, RIS, PACS, VNA, ECM, advanced analytics, cloud, and mobility will fundamentally impact hospitals' digital strategies execution and success.This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806"), new Guid("8d223ce3-389a-4c03-8a5d-51c2d3e79bde"), "c03a5b96-20c4-4bac-9c92-aea551fc9806", new DateTime(2020, 4, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "c03a5b96-20c4-4bac-9c92-aea551fc9806.jpg", new Guid("eda7936e-987b-4c67-ac6d-242b38c45e7f"), true, false, false, null, "Health Insights: European Life Science and Pharma", "HEALTH INSIGHTS: EUROPEAN LIFE SCIENCE AND PHARMA", "Digital transformation is key in the European life science industry's ongoing efforts to transform itself as it seeks to find new paths to innovation, optimize operational efficiency and effectiveness, and regain long-term sustainability. The transition toward new care delivery ecosystems and outcome-based reimbursement models enhances life science organizations' needs to implement new approaches to information management and use. The digital mission in the life sciences is to integrate new and existing data, information, and knowledge into a more knowledge-centric approach to new drug and medical devices development, to product commercialization and management, and to treatment delivery. The European life science digital transformation service provides a forward-looking analysis of IT and technologies that are being adopted all along the value chain of the European life science industry. The service focuses on how European life science organizations can leverage the new range of technologies underpinned by the 3rd Platform technologies and innovation accelerators to support efforts to improve the operational efficiency of the industry and better engage with the ultimate end user of life science innovations, namely patients. This service develops comprehensive data and unique analyses through focused topical surveys, primary and secondary research, and insights from industry experts, practitioners, and vendors. To ensure relevance, IDC Health Insights' analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae"), new Guid("8d223ce3-389a-4c03-8a5d-51c2d3e79bde"), "40758cb7-f705-424a-9dac-db2bac62c9ae", new DateTime(2020, 2, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "40758cb7-f705-424a-9dac-db2bac62c9ae.jpg", new Guid("73cd65a0-dedd-4b14-80a0-9766d5808165"), true, false, false, null, "Canadian Internet of Things Ecosystem and Trends", "CANADIAN INTERNET OF THINGS ECOSYSTEM AND TRENDS", "The Internet of Things (IoT) market is poised for exponential growth over the next several years. However, the IoT ecosystem is a complex segment with multiple layers and hundreds of players, including device and module manufacturers, communication service providers (CSPs), IoT platform players, applications, analytics and security software vendors, and professional services providers. 's Canadian Internet of Things Ecosystem and Trends service analyzes the regionally specific growth of this market from the autonomously connected units in the enterprise world to the applications and services that will demonstrate the power of a world of connected .Markets and Subjects Analyzed Market size and forecast for the Canadian IoT market Canadian IoT revenue forecast by industry Canadian IoT revenue forecast by use cases Taxonomy of the IoT ecosystem: How is the market organized, and what are the key segments and technologies? Canadian-specific demand-side data from various segments (e.g., IT and LOB) Analysis of Canadian-specific vendor readiness and go-to-market strategies for IoT Vertical use cases/case studies of IoT Core Research Taxonomy of the IoT Ecosystem Forecast for the Canadian IoT Market by Industry Forecast for the Canadian IoT Market by Use Cases Profiles of Canadian Vendors Leading IoT Evolution Case Studies of Successful Canadian IoT Implementations Canadian Internet of Things Decision-Maker Survey In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("cffed078-aa51-4fe2-913e-59e6e2624828"), new Guid("c4417156-6782-43a7-9d3c-127699043dec"), "cffed078-aa51-4fe2-913e-59e6e2624828", new DateTime(2020, 4, 29, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "cffed078-aa51-4fe2-913e-59e6e2624828.jpg", new Guid("73cd65a0-dedd-4b14-80a0-9766d5808165"), true, false, false, null, "Agile Application Life-Cycle, Quality and Portfolio Strategies", "AGILE APPLICATION LIFE-CYCLE, QUALITY AND PORTFOLIO STRATEGIES", "Agile Application Life-Cycle, Quality and Portfolio Strategies service provides insight into business and IT alignment through end-to-end application life-cycle management (ALM) and DevOps strategies including agile, IT portfolio management, software quality, testing and verification tools, software change and configuration, and continuous releases for digital transformation. This service provides insights on how product, organizational, and process transitions can help address regulatory compliance, security, and licensing issues covering cloud, SaaS, and open source software. It also analyzes trends related to ALM and software deployment with applications that leverage and target artificial intelligence (AI) and machine learning (ML), collaboration, mobile and embedded apps, IoT, virtualization, and complex sourcing and evolve to adaptive work, project and portfolio management (PPM), and agile adoption with organizational and process strategies (e.g., Scaled Agile Framework [SAFe]). Markets and Subjects Analyzed Software life-cycle process, requirements, configuration management and collaboration, and agile development and projects Software quality, including mobile, cloud, and embedded strategies PPM and work management software trends and strategies IT project and portfolio management software Developer, business, and operations trends Tools enabling DevOps and governance solutions to align complex deployment, financial management, infrastructure, and business priorities Trends in software configuration management (SCM), including connecting business to IT via requirements management and web services demands on agile life-cycle management solutions Automated software quality (ASQ) tools evolution: Hosted testing, web services, API and cloud testing, service virtualization, vulnerability, risk and value-based testing, and other emerging technologies (e.g., mobile, analysis, and metrics) Trends in collaborative software development with social media" },
                    { new Guid("4a817d53-485b-4634-9c09-22eb56d367fc"), new Guid("c94d7746-8c70-4b7b-8910-489f643ea7f4"), "4a817d53-485b-4634-9c09-22eb56d367fc", new DateTime(2020, 6, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "4a817d53-485b-4634-9c09-22eb56d367fc.jpg", new Guid("73cd65a0-dedd-4b14-80a0-9766d5808165"), true, false, false, null, "Software Channels and Ecosystems", "SOFTWARE CHANNELS AND ECOSYSTEMS", "Software Channels and Ecosystems research service offers intelligence and expertise to help channel executives and program managers develop, implement, support, and manage effective channel strategies and programs to drive successful relationships with the spectrum of partner activities (e.g., resale, managed/cloud services, consulting professional services, and software development). In addition, it provides a comprehensive view of the value of the indirect software market and its leading vendor proponents. This service also identifies and analyzes key industry trends and their impact on channel relationship drivers and channel partner business models. Subscribers are invited to 's semiannual Software Channel Leadership Council where and channel executives present and discuss key industry issues. Markets and Subjects Analyzed Multinational software vendors Digital transformation in the partner ecosystem Software and SaaS channel revenue flow Digital marketplaces Cloud and channels Partner-to-partner networking Key channel trends Software partner program profiles and innovative practices Core Research Channel Revenue Forecast and Analysis Partner Marketing and Communications Ecosystem Digital Transformation Cloud and Channels Emerging Partner Business Models Partner Collaboration." },
                    { new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193"), new Guid("c94d7746-8c70-4b7b-8910-489f643ea7f4"), "3bec79ef-9a4a-41e4-9195-0a5c024cb193", new DateTime(2020, 5, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "3bec79ef-9a4a-41e4-9195-0a5c024cb193.jpg", new Guid("73cd65a0-dedd-4b14-80a0-9766d5808165"), true, false, false, null, "Multicloud Data Management and Protection", "MULTICLOUD DATA MANAGEMENT AND PROTECTION", "Multicloud Data Management and Protection service enables storage vendors and IT users to have a more comprehensive view of the evolving data protection, availability, and recovery market. Integrating cloud technology with traditional hardware and software components, the report series will include market forecasts and provide a combination of timely tactical market information and long-term strategic analysis. Markets and Subjects Analyzed Cloud-based solutions, including backup as a service (BaaS) and disaster recovery as a service (DRaaS) Public, private, and hybrid cloud data protection technologies Disk-based data protection and recovery solutions packaged as software, appliance, or gateway system with a focus on technology evolution Market forecast based on total terabytes shipped and revenue for disk-based data protection and recovery (No vendor shares will be published.) End-user adoption of different data protection and recovery technologies and solutions Cloud service providers delivering cloud-based data protection Data protection for nontraditional data types (e.g., Mongo DB, Cassandra, Hadoop) Vendors delivering disk-based data protection and recovery solutions with focus on architecture based on use cases and applications Technologies and processes, including backup, snapshots, replication, continuous data protection, and data deduplication Impact of purpose-built backup appliances (PBBAs) and hyperconverged systems on overall market growth and customer adoption Implications of copy data management and impact on data availability, capacity management, cost, governance, and security The evolutionary impact of virtual infrastructure and object storage on data protection schemes Implications of flash technology for data protection Data protection best practice guidelines and adoption Core Research Market Analysis, Sizing, and Vendor Shares of Backup as a Service and Disaster Recovery as a Service Use of Disk-Based Technologies for Protection and Recovery Adoption Patterns and Role of PBBAs in Customer Environments End-User Needs and Requirements Data Protection and Recovery Software Market Size, Shares, and Forecasts Disk-Based Data Protection Market Size and Forecast Virtual Backup Appliance Solutions Intersection of Data Protection Software and Management with Cloud Services In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("aba89aa6-e637-4518-83c1-125b625add12"), new Guid("b13ece8a-9eb4-49e0-b4eb-6bbf7cd6e9e6"), "aba89aa6-e637-4518-83c1-125b625add12", new DateTime(2020, 5, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "aba89aa6-e637-4518-83c1-125b625add12.jpg", new Guid("73cd65a0-dedd-4b14-80a0-9766d5808165"), true, false, false, null, "DevOps Analytics, Automation and Security", "DEVOPS ANALYTICS, AUTOMATION AND SECURITY", "DevOps is a powerful modern approach to unifying the business strategy, development, testing, deployment, and life-cycle operation of software. This approach is accomplished by improving business, IT, and development collaboration while taking full advantage of automation technologies, end-to-end processes, microservices architecture, and cloud infrastructure to accelerate development and delivery and enable innovation. This subscription service provides insight, forecasts, and thought leadership to assist IT management, IT professionals, IT vendors, and service providers in creating compelling DevOps strategies and solutions. Markets and Subjects Analyzed DevOps adoption drivers, benefits, and use cases Identification of DevOps innovators and best practices Identification of critical DevOps tools enabling automation, open source technologies, and market leaders Analysis of the impact DevOps is having on IT infrastructure hardware and software purchasing and deployment priorities across on-premises and cloud service platforms Major transformation and impact DevOps is having on staffing, skills, and internal processes Core Research Worldwide DevOps Software Forecast, 2018-2022 Worldwide DevOps Software Market Share, 2018-2022 The Role of Cloud-Based IDEs to Drive DevOps Adoption Changing Drivers of DevOps: Role of Serverless and Microservices/Cloud-Native Architectures Market Analysis Perspective: Worldwide Developer, Operations, and DevOps Evolving Approaches and Practices in DevOps The Shift to Continuous Delivery and Deployment Tools and Frameworks for Supporting DevOps Workflows In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment." },
                    { new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd"), new Guid("dbdd3bd0-5cac-496c-8133-49ac43b94e47"), "765337c9-8382-4498-9314-8b0cb77eb8dd", new DateTime(2020, 5, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "765337c9-8382-4498-9314-8b0cb77eb8dd.jpg", new Guid("73cd65a0-dedd-4b14-80a0-9766d5808165"), true, false, false, null, "Cybersecurity Analytics, Intelligence, Response and Orchestration", "CYBERSECURITY ANALYTICS, INTELLIGENCE, RESPONSE AND ORCHESTRATION", "Cybersecurity Analytics, Intelligence, Response and Orchestration service covers security software and hardware products related to analytic security platforms, security and vulnerability management (SVM), and security orchestration platforms. Specific functions covered include vulnerability management and intelligence, SIEM, security analytics, threat hunting, incident detection and response, and orchestration. The service is designed to create in-depth coverage of the analytic-based and platform security markets. Markets and Subjects Analyzed Vulnerability management SIEM Security analytics Incident detection and response Threat analytics Automation Orchestration Core Research Security AIRO Forecast Security AIRO Market Share SIEM Market Share and Forecast Market MAP In addition to the insight provided in this service, may conduct research on specific topics or emerging market segments via research offerings that require additional funding and client investment. Key Questions Answered What is the size and market opportunity for security analytics solutions? Who are the major players in the security analytics space? What is the size and market opportunity for security orchestration solutions? What is the size and market opportunity for threat analytics solutions? How has the competitive landscape changed through digital transformation and adoption of cloud and enabling technologies?" },
                    { new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0"), new Guid("c94d7746-8c70-4b7b-8910-489f643ea7f4"), "8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0", new DateTime(2020, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0.jpg", new Guid("1665f048-f08c-47a7-a044-e7122b79e93e"), true, false, false, null, "North America Utilities Digital Transformation Tactics", "ENERGY INSIGHTS: NORTH AMERICA UTILITIES DIGITAL TRANSFORMATION TACTICS", "Digital transformation is driving utilities to change their business and operating and information models. The Energy Insights: North America Utilities Digital Transformation Strategies service is designed to help North American utility IT and business management understand the disruptions that are transforming the energy and utility value chains and develop the strategies and programs to capitalize on the evolving opportunities. The service provides deep research and insight on the key topics that matter to North American utility decision makers and IT executives affected by the digital transformation. Through an integrated set of deliverables, subscribers gain access to analysis and insights on the impact of new technologies, IT strategies and best practices, and the industry, governmental, and regulatory forces that are shaping the evolution to new industry business models in the industry's generation, transmission and distribution/pipelines, and retail sectors. This service delivers research, insight, and guidance on IT strategy and investments to meet the utility's most critical corporate objectives." },
                    { new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069"), new Guid("8d223ce3-389a-4c03-8a5d-51c2d3e79bde"), "d2b8c016-2bec-4a0a-acbe-c2fe03b55069", new DateTime(2020, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "d2b8c016-2bec-4a0a-acbe-c2fe03b55069.jpg", new Guid("1665f048-f08c-47a7-a044-e7122b79e93e"), true, false, false, null, "Worldwide Oil and Gas Downstream IT Blueprint", "WORLDWIDE OIL AND GAS DOWNSTREAM IT BLUEPRINT", "Energy Insights: Worldwide Oil and Gas Downstream IT Strategies advisory research service takes the worldwide oil and gas IT strategies research and applies it to the downstream operations in oil and gas. The research focuses on companies that are involved in the production for sale of all petroleum products, from incoming crude to outgoing product. It will also encompass the technology companies that provide the technology foundation for the operation of the downstream business. Energy Insights: Worldwide Oil and Gas Downstream IT Strategies will focus its analysis on the digital transformation (DX) use cases, priorities, innovation technology, and road map for all aspect of companies that produce and distribute petroleum products. It will provide technology vendors a road map for product development and messaging in oil and gas downstream. Throughout the year, this service will address the following topics: Digital transformation – Investment priorities and road maps for transformation of the downstream business operation Operations technology — IT/OT convergence investments that help downstream organizations transform how they operate the production lines Asset management – Technology changes driving asset management organization and transformation Transformation to scale – Development of a transformed infrastructure to scale to market volatility Our research addresses the following issues that are critical to your success: What is the impact of transformational technologies on the oil and gas downstream DX road map? How does an oil and gas downstream company organize itself around the Future of Work? What are the market trends in oil and gas downstream that affect the use case prioritization for DX? How should trading partners be integrated into the DX road map? Who are the technology vendors that can support DX in oil and gas downstream?" },
                    { new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a"), new Guid("c94d7746-8c70-4b7b-8910-489f643ea7f4"), "e4b77a8a-ff72-4851-94f5-a1faff00731a", new DateTime(2020, 1, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "e4b77a8a-ff72-4851-94f5-a1faff00731a.jpg", new Guid("1665f048-f08c-47a7-a044-e7122b79e93e"), true, false, false, null, "Global Utilities Customer Experience Scheme", "GLOBAL UTILITIES CUSTOMER EXPERIENCE SCHEME", "With the evolution of energy and water markets and the possibilities offered by digital transformation, utilities are developing new business models and are focusing on providing customers more interactive experiences. Smart metering, analytics, social media, mobility, cloud, and IoT are fundamentally impacting customer operation practices in both competitive and regulated markets. The Energy Insights: Worldwide Utilities Customer Experience Strategies service is designed to help utilities and energy retailers servicing customers in competitive and regulated markets at the worldwide level (including electricity, gas, and water). The service provides exclusive research and direct access to experts providing guidance to make the right IT investments and meet corporate objectives of customer satisfaction, reduction of cost to serve, and innovation. This service develops unique analysis and comprehensive data through Energy Insights' proprietary research projects, along with ongoing communications with industry experts, utility management, ICT vendors, and service providers. With decades of experience in the utility industry, our analysts leverage a broad spectrum of expertise and intellectual property from Energy Insights. Research reports elucidate business strategy, best practices, technology selection, and vendor assessment, along with short perspectives on topical issues. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("76352896-e547-485f-ae20-73b4f05b664f"), new Guid("b13ece8a-9eb4-49e0-b4eb-6bbf7cd6e9e6"), "76352896-e547-485f-ae20-73b4f05b664f", new DateTime(2020, 3, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "76352896-e547-485f-ae20-73b4f05b664f.jpg", new Guid("1665f048-f08c-47a7-a044-e7122b79e93e"), true, false, false, null, "Asia/Pacific Oil and Gas Digital Transformation Procedure", "ASIA/PACIFIC OIL AND GAS DIGITAL TRANSFORMATION PROCEDURE", "Asia/Pacific Oil and Gas Digital Transformation Strategies examines the business environment of the oil and gas industry in the Asia/Pacific region, excluding Japan. The service covers the whole value chain from upstream, midstream, and downstream but is focused on upstream. It seeks to support companies working to deliver against efficiency objectives through technology-led change that will deliver improved decision support and insight and flexibility to adjust to changing technology, markets, and political pressures. The Energy Insights: Asia/Pacific Oil and Gas Digital Transformation Strategies program provides advice on best practices and technology priorities that will support technology decision making, particularly, in relation to innovation, data management, and greater integration across silos and processes. The service is designed to support oil and gas companies with market insights and road mapping advice relating to technology decision making, particularly delivering insight to how increased value from investments can be delivered through better understanding of new metric, organization, talent, and process change. Key technology focus areas of the program include IT/OT integration, operational cybersecurity, IoT strategies, and asset management priorities. This service is built on the foundations of extensive engagement and research by the Energy Insights team across the oil and gas industry in this region. Our analysts work with industry experts, staff from the oil and gas business, and technology vendors to ensure that the research service is relevant and prioritizes areas that are of importance to them and the industry at large." },
                    { new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d"), new Guid("c94d7746-8c70-4b7b-8910-489f643ea7f4"), "a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d", new DateTime(2020, 4, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d.jpg", new Guid("1665f048-f08c-47a7-a044-e7122b79e93e"), true, false, false, null, "Worldwide Mining Policy", "WORLDWIDE MINING POLICY", "Worldwide Mining Strategies examines the business environment across the mining sector value chain from exploration through to operations, processing, supply chain, and trading globally. Mining companies are operating in an increasingly competitive commodity market environment, driven by pressures relating to accessing funding, assets, and talent. Technology and data are playing an increasingly critical role across the operation to enable decision support, automation, integration, and control. This service provides comprehensive insights into the best practices that show how mining companies are responding and what road maps these companies will need to build the information technology (IT) capabilities required to create integrated, agile, and responsive operations. Mining companies are changing the way they buy technology, how they collaborate to frame the problems to be solved, how they engage with technology suppliers, and how they innovate across their businesses bringing together the capabilities of IT and operational technology (OT). This service tracks the IT investment priorities for organizations seeking to scale value creation across their organization and the impact on decision making and best practices relating to technology, process, and organizational change. This service distills market and industry data into incisive analysis drawn from in-depth interviews with industry experts, mining staff from across the business, and technology vendors. Insight and analysis are further supported and validated through rigorous research methodologies in quantitative market analysis.  Energy Insights' analysts develop unique and comprehensive analyses of this data, focused on providing actionable recommendations. To ensure relevance, our analysts work with subscribers to identify and prioritize specific topics to be covered in research reports." },
                    { new Guid("11dcaf06-8a35-403a-8262-ebd98344039a"), new Guid("dbdd3bd0-5cac-496c-8133-49ac43b94e47"), "11dcaf06-8a35-403a-8262-ebd98344039a", new DateTime(2020, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "11dcaf06-8a35-403a-8262-ebd98344039a.jpg", new Guid("a6f963a9-15bd-4c72-903c-ea64adfe34f2"), true, false, false, null, "Digital Commerce Trends", "DIGITAL COMMERCE TRENDS", "The Digital Commerce subscription marketing intelligence service examines the competitive landscape, key trends, and differentiating factors of digital commerce, PIM, and CPQ application vendors; digital marketplaces; and business commerce networks. This includes the buying behavior of end users while purchasing products and services on digital commerce platforms. The service provides a worldwide perspective by looking at all forms of digital commerce transactions — including B2B, B2C, C2C, G2B, and B2B2C — across all vertical industries and regional markets. Markets and Subjects Analyzed. Digital commerce applications targeting businesses of all sizes and industries. Best-of-breed digital commerce applications in areas such as CPQ, payments and billing, order management, web content management, merchandising, site search, fulfillment, product information management, and inventory management. Business commerce networks that bring together functional applications for enterprise asset management, procurement, financials, sales, and human capital management.Enterprise partnership/integration strategies among digital commerce and marketing/content management vendors. Experience management — across channels (includes content management, commerce platforms, integration, and advanced analytics) for B2B, B2C, and B2B2C. Impact of cloud, social, mobile, and Big Data technologies on vendor strategies for employee, customer, supplier, partner, and asset engagement. Proliferation of public cloud microservice architectures. Mobile commerce (mobile app marketplaces, built-for-mobile commerce capabilities, and differentiating technology). Cognitive technologies and intelligent workflow impact on digital commerce applications" },
                    { new Guid("67966830-655d-42f7-9424-ef5cf441ff8d"), new Guid("b13ece8a-9eb4-49e0-b4eb-6bbf7cd6e9e6"), "67966830-655d-42f7-9424-ef5cf441ff8d", new DateTime(2020, 5, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "67966830-655d-42f7-9424-ef5cf441ff8d.png", new Guid("a6f963a9-15bd-4c72-903c-ea64adfe34f2"), true, false, false, null, "Canadian Sales Accelerator: Datacenter Infrastructure", "CANADIAN SALES ACCELERATOR: DATACENTER INFRASTRUCTURE", "Canadian Sales Accelerator: Datacenter Infrastructure research program analyzes the Canadian datacenter (DC) infrastructure market in Canada. Designed to provide intelligence and strategic frameworks to technology sales professionals, field marketing teams, and channel managers to take action on key responsibilities related to the sales cycle, this service provides market sizing, vendor performance, forecasts, and market opportunities, as well as a variety of adjacent markets and segmentations from a quantitative standpoint. This advisory service will allow subscribers to identify opportunities tied to the future of infrastructure, tailor go-to-market strategies as well as product and services road maps to meet the growing demand of end users for infrastructure solutions. At the same time, it looks at vendors, partners, and customers from a qualitative standpoint, concerning needs and requirements, pain points and buying intentions, maturity levels, and adoption in and of new technologies. Markets and Subjects Analyzed Datacenter technologies (including software defined); server and storage; converged systems. Infrastructure ecosystem including partners and channels. Datacenter budget trends and dynamics and vendor selection/buying criteria. Technology investment expectations for legacy and next-gen DC infrastructure. In addition to the core research documents, clients will receive briefings and concise sales executive email alerts throughout the year. Every client will have a Sales Accelerator service launch integration meeting to kick-off the program. Core Research. Brand Perceptions on Enterprise Storage and Service Vendors. Market Forecasts. Vendor Dashboard: Market Shares. Infrastructure Ecosystem Barometer. Executive Market Insights" }
                });

            migrationBuilder.InsertData(
                table: "ReportTags",
                columns: new[] { "TagId", "ReportId" },
                values: new object[,]
                {
                    { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") },
                    { new Guid("ce36a51a-9b79-45a9-beeb-40fa09a77d4f"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") },
                    { new Guid("d54acd8a-4af1-4f65-9e6b-1ad68315c987"), new Guid("180360e0-6db8-412e-a6f4-3d68a939c583") },
                    { new Guid("ce36a51a-9b79-45a9-beeb-40fa09a77d4f"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") },
                    { new Guid("9282060a-bca0-4d10-8546-601862379082"), new Guid("698b544c-d07c-41d1-9fad-20dabb68c7b0") },
                    { new Guid("d6f523c1-8247-400e-abf3-5ca85575ccfd"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") },
                    { new Guid("7c0fdd96-f11f-4fe8-b811-90577d20ff2f"), new Guid("0daebcfa-c6a5-44f3-8193-25c442f9ec37") },
                    { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") },
                    { new Guid("e7c3cd21-b001-4f84-b724-04ab0ce10a9d"), new Guid("daaf7eab-fc1c-4a84-a8bc-3d857def3f82") },
                    { new Guid("d6f523c1-8247-400e-abf3-5ca85575ccfd"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") },
                    { new Guid("e7c3cd21-b001-4f84-b724-04ab0ce10a9d"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") },
                    { new Guid("36533151-9264-41de-836e-b58d3dbfb364"), new Guid("3dc823d3-91e3-40c6-b141-2c25ed9cd64a") },
                    { new Guid("7c0fdd96-f11f-4fe8-b811-90577d20ff2f"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") },
                    { new Guid("36533151-9264-41de-836e-b58d3dbfb364"), new Guid("bcaede74-79bf-4147-8d5b-46f161f4d015") },
                    { new Guid("d54acd8a-4af1-4f65-9e6b-1ad68315c987"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") },
                    { new Guid("e7c3cd21-b001-4f84-b724-04ab0ce10a9d"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") },
                    { new Guid("d6f523c1-8247-400e-abf3-5ca85575ccfd"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") },
                    { new Guid("e7c3cd21-b001-4f84-b724-04ab0ce10a9d"), new Guid("3a4b1c09-e5b7-4246-843c-bca16a106b97") },
                    { new Guid("36533151-9264-41de-836e-b58d3dbfb364"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") },
                    { new Guid("3604c205-28eb-45e2-a029-f38e1c206c8a"), new Guid("3ffe678c-ac6b-4e5c-abe9-0eb5389b81ef") },
                    { new Guid("7ab98bef-40c1-4a1b-bf1a-88911ae38852"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") },
                    { new Guid("bf6b1c50-38fc-4bb4-8a5b-09f075f46d17"), new Guid("7af1f20b-db8a-4eb0-b484-eb03487efb02") },
                    { new Guid("7ab98bef-40c1-4a1b-bf1a-88911ae38852"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") },
                    { new Guid("3604c205-28eb-45e2-a029-f38e1c206c8a"), new Guid("90c09b29-4745-484f-b5d9-f24696e4acc7") },
                    { new Guid("36533151-9264-41de-836e-b58d3dbfb364"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") },
                    { new Guid("7ab98bef-40c1-4a1b-bf1a-88911ae38852"), new Guid("9062907e-6a91-4737-95f9-8db332df47f4") },
                    { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") },
                    { new Guid("7ab98bef-40c1-4a1b-bf1a-88911ae38852"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") },
                    { new Guid("bf6b1c50-38fc-4bb4-8a5b-09f075f46d17"), new Guid("11dcaf06-8a35-403a-8262-ebd98344039a") },
                    { new Guid("36533151-9264-41de-836e-b58d3dbfb364"), new Guid("d1b04963-e698-41bf-aded-25bb755c2877") },
                    { new Guid("55553fa1-4dfd-48c7-abbb-346f30d9d47e"), new Guid("044c6dd3-760b-4bfc-ac7a-9b1e01e9d2e2") },
                    { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") },
                    { new Guid("ce36a51a-9b79-45a9-beeb-40fa09a77d4f"), new Guid("0070a7dc-c760-4a34-8c87-d4747824191b") },
                    { new Guid("649c608d-e658-4a0c-ab24-7e35f36e409a"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") },
                    { new Guid("ce01eae4-256c-46e9-b42d-98f7e7602218"), new Guid("3d360ea3-0587-4e87-9eeb-07f7a429b6cb") },
                    { new Guid("5179e8fb-52d0-49b4-841f-aae3c0bd086e"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") },
                    { new Guid("8726cd04-1394-4f69-a016-2a80f15c6238"), new Guid("a8b7a5a4-1e35-462e-bfa3-eb450d8bae5d") },
                    { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") },
                    { new Guid("5179e8fb-52d0-49b4-841f-aae3c0bd086e"), new Guid("76352896-e547-485f-ae20-73b4f05b664f") },
                    { new Guid("ce01eae4-256c-46e9-b42d-98f7e7602218"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") },
                    { new Guid("8726cd04-1394-4f69-a016-2a80f15c6238"), new Guid("e4b77a8a-ff72-4851-94f5-a1faff00731a") },
                    { new Guid("649c608d-e658-4a0c-ab24-7e35f36e409a"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") },
                    { new Guid("5179e8fb-52d0-49b4-841f-aae3c0bd086e"), new Guid("d2b8c016-2bec-4a0a-acbe-c2fe03b55069") },
                    { new Guid("649c608d-e658-4a0c-ab24-7e35f36e409a"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") },
                    { new Guid("ce01eae4-256c-46e9-b42d-98f7e7602218"), new Guid("8ad8e2d5-e55a-48d2-aa5b-1306ae4445f0") },
                    { new Guid("cd64cb0b-f2a3-4b4b-9284-d6badceedcdb"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") },
                    { new Guid("5522d757-5cb0-4f10-adb2-5d472bbd79b0"), new Guid("765337c9-8382-4498-9314-8b0cb77eb8dd") },
                    { new Guid("91893923-a104-4a3c-ad98-a9487896f066"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") },
                    { new Guid("5522d757-5cb0-4f10-adb2-5d472bbd79b0"), new Guid("aba89aa6-e637-4518-83c1-125b625add12") },
                    { new Guid("a73af8ad-cd30-411e-bf70-f7af80b91018"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") },
                    { new Guid("5522d757-5cb0-4f10-adb2-5d472bbd79b0"), new Guid("3bec79ef-9a4a-41e4-9195-0a5c024cb193") },
                    { new Guid("cd64cb0b-f2a3-4b4b-9284-d6badceedcdb"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") },
                    { new Guid("5522d757-5cb0-4f10-adb2-5d472bbd79b0"), new Guid("4a817d53-485b-4634-9c09-22eb56d367fc") },
                    { new Guid("91893923-a104-4a3c-ad98-a9487896f066"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") },
                    { new Guid("a73af8ad-cd30-411e-bf70-f7af80b91018"), new Guid("cffed078-aa51-4fe2-913e-59e6e2624828") },
                    { new Guid("91893923-a104-4a3c-ad98-a9487896f066"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") },
                    { new Guid("cd64cb0b-f2a3-4b4b-9284-d6badceedcdb"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") },
                    { new Guid("5522d757-5cb0-4f10-adb2-5d472bbd79b0"), new Guid("40758cb7-f705-424a-9dac-db2bac62c9ae") },
                    { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") },
                    { new Guid("9282060a-bca0-4d10-8546-601862379082"), new Guid("c03a5b96-20c4-4bac-9c92-aea551fc9806") },
                    { new Guid("3f0798df-93be-4395-83d1-4f967067d5ce"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") },
                    { new Guid("55553fa1-4dfd-48c7-abbb-346f30d9d47e"), new Guid("e5487fe8-7a8f-47d4-9fb0-1ec6e16e2244") },
                    { new Guid("bf6b1c50-38fc-4bb4-8a5b-09f075f46d17"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") },
                    { new Guid("3604c205-28eb-45e2-a029-f38e1c206c8a"), new Guid("67966830-655d-42f7-9424-ef5cf441ff8d") }
                });
        }
    }
}
