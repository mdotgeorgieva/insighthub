﻿using InsightHub.Data.Seeder;
using InsightHub.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Reflection;

namespace InsightHub.Data
{
	public class InsightHubContext : IdentityDbContext<User, Role, Guid>
    {
        public InsightHubContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Industry> Industries { get; set; }
        public DbSet<ClientReports> ClientReports { get; set; }
        public DbSet<ReportTags> ReportTags { get; set; }
        public DbSet<UserIndustry> UserIndustries { get; set; }
        public DbSet<Notification> Notifications { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            Assembly assemblyWithConfigurations = Assembly.GetExecutingAssembly();
            builder.ApplyConfigurationsFromAssembly(assemblyWithConfigurations);

            builder.Seed();

            base.OnModelCreating(builder);
        }
    }
}
