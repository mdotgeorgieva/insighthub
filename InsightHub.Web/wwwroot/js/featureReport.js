﻿$(function () {
    $(document).on('click', '.rp-button-feature', function (x) {
        x.preventDefault();
        let reportId = $(this).data("value");

        $.ajax({
            url: '/Admin/Reports/ToggleFeature/',
            type: "POST",
            data: { 'reportId': reportId },
            success: function (data) {
                if (data) {
                    $('#' + reportId).html('Unfeature');
                }
                else {
                    $('#' + reportId).html('Feature');

                }
            },
            error: function (data) {
                console.log("Error");
            }
        })
    });
});