﻿$('#download').click(function (x) {
    x.preventDefault();
    let reportId = $(this).data("value");

    $.ajax({
        url: '/Users/Download/',
        type: 'POST',
        data:{
            'reportId': reportId
        },
        success: function () {
            console.log("Success");
        },
        error: function () {
            console.log("Error");
        }
    })
})