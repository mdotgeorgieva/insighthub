using InsightHub.Services.DTOs;
using InsightHub.Web.Areas.Admin.Models;
using InsightHub.Web.Areas.Author.Models;
using InsightHub.Web.Models.IndexModels;
using InsightHub.Web.Models.ReportModels;
using System.Collections.Generic;
using System.Linq;

namespace InsightHub.Web.Mappers
{
	public static class ReportMapper
    {
        public static ReportDTO GetDto(this CreateReportVM reportVm)
        {
            var reportDto = new ReportDTO
            {
                Name = reportVm.Name,
                IndustryId = reportVm.IndustryId,
                Summary = reportVm.Summary,
                Content = reportVm.Content.FileName,
                Tags = reportVm.Tags?.Split(','),
                ImagePath = reportVm.ImagePath?.FileName.Split('.').Last()
            };

            return reportDto;
        }

        public static ICollection<ReportPageVM> GetPageVM(this ICollection<ReportDTO> reportDto)
        {
            var reportVm = reportDto.Select(r => new ReportPageVM()
            {
                Id = r.Id,
                Name = r.Name,
                Description = r.Summary,
                ImagePath = r.ImagePath,
                AuthorId = r.AuthorId,
                Author = r.Author,
                Downloads = r.Downloads,
                Tags = r.Tags,
                IsFeatured = r.IsFeatured,
                CreatedOn = r.CreatedOn

            }).ToList();

            return reportVm;
        }

        public static ReportVM GetAdminReport(this ReportDTO reportDTO)
        {
            var reportVM = new ReportVM
            {
                Id = reportDTO.Id,
                Author = reportDTO.Author,
                Content = reportDTO.Content,
                Industry = reportDTO.Industry,
                Name = reportDTO.Name,
                Tags = reportDTO.Tags
            };

            return reportVM;
        }

        public static ICollection<ReportVM> GetAdminReport(this ICollection<ReportDTO> reports)
            => reports.Select(GetAdminReport).ToList();

        public static ReportDTO GetDTO(this UpdateReportVM reportVm)
        {
            var reportDto = new ReportDTO
            {
                Id = reportVm.Id,
                Name = reportVm.Name,
                IndustryId = reportVm.IndustryId,
                Summary = reportVm.Summary,
                Tags = reportVm.Tags?.Split(','),
                Content = reportVm.Content?.FileName,
                ImagePath = reportVm.Photo?.FileName.Split('.').Last()
            };

            return reportDto;
        }

        public static UpdateReportVM GetUpdateVM(this ReportDTO reportDto)
        {
            var reportVm = new UpdateReportVM
            {
                Id = reportDto.Id,
                Name = reportDto.Name,
                IndustryId = reportDto.IndustryId,
                Summary = reportDto.Summary,
                Tags = string.Join(',', reportDto.Tags)
            };

            return reportVm;
        }

        public static DeleteReportVM GetDeleteVM(this ReportDTO reportDto)
        {
            var reportVm = new DeleteReportVM
            {
                Id = reportDto.Id,
                Name = reportDto.Name
            };

            return reportVm;
        }

        public static ReportIndexVM GetHomepageVM(this ReportDTO report)
        {
            var reportVM = new ReportIndexVM
            {
                Id = report.Id,
                Description = report.Summary,
                ImagePath = report.ImagePath,
                Name = report.Name,
                CreatedOn = report.CreatedOn
            };

            return reportVM;
        }

        public static ICollection<ReportIndexVM> GetHomepageVM(this ICollection<ReportDTO> reports)
            => reports.Select(GetHomepageVM).ToList();

        public static ReportDetailsVM GetDetaisVM(this ReportDTO reportDto)
        {
            var reportDetailsVm = new ReportDetailsVM
            {
                Id = reportDto.Id,
                Name = reportDto.Name,
                ImagePath = reportDto.ImagePath,
                Author = reportDto.Author,
                AuthorId = reportDto.AuthorId,
                CreatedOn = reportDto.CreatedOn,
                Description = reportDto.Summary,
                Downloads = reportDto.Downloads,
                Tags = reportDto.Tags
            };

            return reportDetailsVm;
        }
    }
}
