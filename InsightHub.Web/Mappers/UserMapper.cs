﻿using InsightHub.Services.DTOs;
using InsightHub.Web.Areas.Admin.Models;
using InsightHub.Web.Models.AuthorsPageModels;
using InsightHub.Web.Models.IndexModels;
using System.Collections.Generic;
using System.Linq;

namespace InsightHub.Web.Mappers
{
	public static class UserMapper
	{
		public static UserVM GetVM(this UserDTO user)
		{
			var userVM = new UserVM
			{
				Id = user.Id,
				FullName = user.FirstName + " " + user.LastName,
				Username = user.Username,
				Role = user.Role
			};

			return userVM;
		}

		public static ICollection<UserVM> GetVM(this ICollection<UserDTO> users)
			=> users.Select(GetVM).ToList();

		public static AuthorVM GetVM(this AuthorDTO author)
		{
			var authorVM = new AuthorVM
			{
				Id = author.Id,
				Bio = author.Bio,
				Fullname = author.FirstName + " " + author.LastName,
				ImagePath = author.ImagePath

			};
			return authorVM;
		}
		public static ICollection<AuthorVM> GetVM(this ICollection<AuthorDTO> authors)
			=> authors.Select(GetVM).ToList();

		public static AuthorIndexVM GetHomepageVM(this AuthorDTO author)
		{
			var authorVM = new AuthorIndexVM
			{
				Id = author.Id,
				ImagePath = author.ImagePath,
				Name = author.FirstName + " " + author.LastName
			};

			return authorVM;
		}

		public static ICollection<AuthorIndexVM> GetHomepageVM(this ICollection<AuthorDTO> authors)
			=> authors.Select(GetHomepageVM).ToList();
	}
}
