﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace InsightHub.Web.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IUserService userService;
        private readonly IPhotoService photoService;

        public IndexModel(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IUserService userService,
            IPhotoService photoService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            this.userService = userService;
            this.photoService = photoService;
        }
        public Guid AuthorId { get; set; }
        public string Bio { get; set; }
        public string Username { get; set; }
        public string ImagePath { get; set; }
        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Phone]
            [Display(Name = "Phone number")]
            [RegularExpression(@"^[+][0-9]+", ErrorMessage = "Telephone number should start with '+' followed by numbers only")]

            public string PhoneNumber { get; set; }
            [Display(Name ="Profile picture")]
            public IFormFile ImagePath { get; set; }
            [Required]
            public Guid AuthorId { get; set; }
            [Display(Name ="Biography")]
            [StringLength(450, ErrorMessage = "{0} must be between {2} and {1} characters", MinimumLength = 100)]
            public string Bio { get; set; }
        }

        private async Task LoadAsync(User user)
        {
            var userName = await _userManager.GetUserNameAsync(user);
            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            if (await _userManager.IsInRoleAsync(user, "Author"))
            {
                var authorId = await this.userService.GetAuthorIdAsync(Guid.Parse(_userManager.GetUserId(User)));
                this.AuthorId = authorId;
                var image = await this.userService.GetAuthorInfoAsync(authorId);
                this.ImagePath = image[0];
                this.Bio = image[1];
            }
            Username = userName;

            Input = new InputModel
            {
                PhoneNumber = phoneNumber,
                AuthorId = this.AuthorId,
                Bio = this.Bio
            };
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await LoadAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(user);
                return Page();
            }

            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            if (Input.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, Input.PhoneNumber);
                if (!setPhoneResult.Succeeded)
                {
                    var userId = await _userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Unexpected error occurred setting phone number for user with ID '{userId}'.");
                }
            }
            if (await _userManager.IsInRoleAsync(user, "Author"))
            {
               
                    var fileName =Input.ImagePath!=null ? user.Email+"."+Input.ImagePath.FileName.Split('.').Last():null;
                    await this.userService.UpdateAuthorProfileAsync(Input.AuthorId, fileName,Input.Bio);
                    if(Input.ImagePath!=null)
                        await this.photoService.UploadProfilePicture(Input.ImagePath, fileName);
                
            }

                await _signInManager.RefreshSignInAsync(user);
            StatusMessage = "Your profile has been updated";
            return RedirectToPage();
        }
    }
}
