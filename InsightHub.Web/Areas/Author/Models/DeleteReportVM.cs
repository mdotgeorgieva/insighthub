﻿using System;

namespace InsightHub.Web.Areas.Author.Models
{
	public class DeleteReportVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
