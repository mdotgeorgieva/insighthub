﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Areas.Author.Models
{
	public class UpdateReportVM
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        [StringLength(150, ErrorMessage = "{0} must be between {2} and {1} characters", MinimumLength = 10)]
        [DisplayName("Title")]
        public string Name { get; set; }
        [Required]
        [DisplayName("Industry")]
        public Guid IndustryId { get; set; }
        [Required]
        [MinLength(500, ErrorMessage = "{0} must be between at least {1} characters long")]
        public string Summary { get; set; }
        [DisplayName("Tags (max. 3)")]
        public string Tags { get; set; }
        public IFormFile Content { get; set; }
        public IFormFile Photo { get; set; }
    }
}
