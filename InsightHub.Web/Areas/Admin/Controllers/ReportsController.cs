﻿using System;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;

namespace InsightHub.Web.Areas.Admin.Controllers
{
	[Area("Admin")]
	[Authorize(Roles="Admin")]
    public class ReportsController : Controller
    {
		private readonly IUserService userService;
		private readonly IToastNotification toastNotification;
		private readonly IBlobService blobService;
		private readonly IReportService reportService;

		public ReportsController(IUserService userService,IToastNotification toastNotification, IBlobService blobService,
								 IReportService reportService)
		{
			this.userService = userService;
			this.toastNotification = toastNotification;
			this.blobService = blobService;
			this.reportService = reportService;
		}
		public async Task<IActionResult> ApproveReport(Guid reportId)
		{
			try
			{
				var isApproved = await this.userService.ApproveReportAsync(reportId);

				this.toastNotification.AddInfoToastMessage("Report was approved successfully!");
				return RedirectToAction("Pending", "Users", new { area="Admin"});
			}
			catch
			{
				this.toastNotification.AddWarningToastMessage("Something went wrong. Could not approve report!");
				return RedirectToAction("Pending", "Users", new { area = "Admin"});
			}
		}


		public async Task<IActionResult> RefuseReport(Guid reportId)
		{
			try
			{
				var isRefused = await this.userService.RefusePendingReportAsync(reportId);
				await this.blobService.DeleteBlob(reportId.ToString());
				this.toastNotification.AddInfoToastMessage("Report was rejected successfully!");

				return RedirectToAction("Pending", "Users");
			}
			catch
			{
				this.toastNotification.AddWarningToastMessage("Something went wrong. Could not refuse report approval!");
				return RedirectToAction("Pending", "Users", new { area = "Admin" });
			}
		}

		public async Task<IActionResult> ToggleFeature(Guid reportId)
		{
			try
			{
				var isFeatured = await this.userService.ToggleReportAsync(reportId);
				return Ok(isFeatured);
			}
			catch
			{
				return NotFound();
			}
		}
	}
}