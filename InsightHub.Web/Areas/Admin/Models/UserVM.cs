﻿using System;

namespace InsightHub.Web.Areas.Admin.Models
{
	public class UserVM
	{
		public Guid Id { get; set; }
		public string Username { get; set; }
		public string FullName { get; set; }
		public string Role { get; set; }
	}
}
