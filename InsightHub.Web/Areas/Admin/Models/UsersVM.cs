﻿using System.Collections.Generic;

namespace InsightHub.Web.Areas.Admin.Models
{
	public class UsersVM
	{
		public ICollection<UserVM> Active { get; set; }
		public ICollection<UserVM> Disabled { get; set; }
	}
}
