﻿using System;
using System.Collections.Generic;

namespace InsightHub.Web.Areas.Admin.Models
{
	public class ReportVM
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Content { get; set; }
		public string Author { get; set; }
		public string Industry { get; set; }
		public ICollection<string> Tags { get; set; }
	}
}
