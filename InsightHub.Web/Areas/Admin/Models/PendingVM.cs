﻿using System.Collections.Generic;

namespace InsightHub.Web.Areas.Admin.Models
{
	public class PendingVM
	{
		public ICollection<UserVM> Users { get; set; }
		public ICollection<ReportVM> Reports { get; set; }

	}
}
