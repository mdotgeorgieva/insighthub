﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace InsightHub.Web.Utilities
{
	public class NotFoundMiddleware
	{
        private readonly RequestDelegate next;
        public NotFoundMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            
            await this.next.Invoke(httpContext);

            if (httpContext.Response.StatusCode == 401)
            {
                httpContext.Response.Redirect("/Home/Restricted");
            }

            if (httpContext.Response.StatusCode == 404)
            {
                httpContext.Response.Redirect("/Home/PageNotFound");
            }

        }
    }
}
