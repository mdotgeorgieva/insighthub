﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.Controllers
{
	public class UsersController : Controller
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }
        public async Task<IActionResult> CheckSubscription(string industry)
        {
            try
            {
                var isLoggedIn = User.Identity.IsAuthenticated;
                if (isLoggedIn)
                {
                    var userId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                    var hasSubscription = await this.userService.IsSubscribed(userId, industry);

                    return Ok(hasSubscription);
                }

                return NotFound();
            }
            catch
            {
                return NotFound();
            }
        }
        [Authorize(Roles ="Client")]
        public async Task<IActionResult> Subscribe(string industry)
        {

            try
            {
                var isLoggedIn = User.Identity.IsAuthenticated;
                if (isLoggedIn)
                {
                    var userId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                    var isSubscribed = await this.userService.Subscribe(userId, industry);

                    return Ok(isSubscribed);
                }

                return NotFound();
            }
            catch
            {
                return NotFound();
            }
        }

        [Authorize(Roles = "Client")]
        public async Task<IActionResult> Unsubscribe(string industry)
        {

            try
            {
                var isLoggedIn = User.Identity.IsAuthenticated;
                if (isLoggedIn)
                {
                    var userId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                    var isUnsubbed = await this.userService.Unsubscribe(userId, industry);

                    return Ok(isUnsubbed);
                }

                return NotFound();
            }
            catch
            {
                return NotFound();
            }
        }

        [Authorize(Roles = "Client")]
        public async Task<IActionResult> Download(Guid reportId)
        {

            try
            {
                var isLoggedIn = User.Identity.IsAuthenticated;
                if (isLoggedIn)
                {
                    var userId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                    var download = await this.userService.DownloadAsync(userId, reportId);

                    return Ok(download);
                }

                return NotFound();
            }
            catch
            {
                return NotFound();
            }
        }
    }
}