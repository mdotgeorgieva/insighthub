﻿using System;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Web.Mappers;
using InsightHub.Web.Models.IndexModels;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.Controllers
{
	public class ReportsController : Controller
    {
        private readonly IReportService reportService;
        private readonly IUserService userService;

        public ReportsController(IReportService reportService, IUserService userService)
        {
            this.reportService = reportService;
            this.userService = userService;
        }
        public async Task<IActionResult> Index(ReportQueryVM reportQuery)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (reportQuery.CurrentPage == 0)
                        reportQuery.CurrentPage = 1;
                    var reports = await this.reportService.GetReportByPage(reportQuery.Industry, reportQuery.SearchCriteria,
                                                                           reportQuery.SearchString, reportQuery.SortString,
                                                                           reportQuery.CurrentPage, reportQuery.ItemsPerPage);

                    reportQuery.Reports = reports.Item1.GetPageVM();
                    reportQuery.TotalPages = reports.Item2;

                    return View(reportQuery);
                }
                catch
                {
                    return RedirectToAction("Index", "Home");

                }
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Details (Guid reportId)
        {
            try
            {
                var report = await this.reportService.GetAsync(reportId);
                var author = await this.userService.GetAuthorInfoAsync(report.AuthorId);

                var reportVm = report.GetDetaisVM();
                reportVm.AuthorPhoto = author[0];

                var relatedReports = await this.reportService.GetRelatedReportsAsync(report.Industry, report.Id);
                var relatedReportsVm = relatedReports.GetHomepageVM();
                reportVm.RelatedReports = relatedReportsVm;

                return View(reportVm);

            }
            catch
            {
                return RedirectToAction("Index", "Reports");
            }
        }
    }
}