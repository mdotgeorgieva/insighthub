﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using InsightHub.Web.Models;
using InsightHub.Services.Contracts;
using InsightHub.Web.Mappers;
using InsightHub.Web.Models.IndexModels;

namespace InsightHub.Web.Controllers
{
	public class HomeController : Controller
    {
        private readonly IReportService reportService;
        private readonly IUserService userService;

        public HomeController(IReportService reportService,IUserService userService)
        {
            this.reportService = reportService;
            this.userService = userService;
        }

        public async Task<IActionResult> Index()
        {
            try
            {
                var featured =await this.reportService.GetFeaturedReportsAsync();
            
                var newest =  await this.reportService.GetNewReportsAsync();
                
                var popular =  await this.reportService.GetPopularReportsAsync();
              
                var authors = await this.userService.GetHomepageAuthorsAsync();
              

                var featuredVM = featured.GetHomepageVM();
                var newestVM = newest.GetHomepageVM();
                var popularVM = popular.GetHomepageVM();

                var authorsVM = authors.GetHomepageVM();

                var homepageVM = new HomePageVM
                {
                    Authors = authorsVM,
                    Featured = featuredVM,
                    New = newestVM,
                    Popular = popularVM

                };
                return View(homepageVM);

            }
            catch
            {
                return View();
            }
        }

        public IActionResult PageNotFound()
        {
            return View();
        }

        public IActionResult Restricted()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
