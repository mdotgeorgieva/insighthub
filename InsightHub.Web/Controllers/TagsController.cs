﻿using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Controllers
{
	public class TagsController : ControllerBase
    {
        private readonly ITagService tagService;

        public TagsController(ITagService tagService)
        {
            this.tagService = tagService;
        }
        // GET: Tags
        [HttpGet]
        public async Task<IActionResult> GetTags()
        {
            try
            {
                var tags = await this.tagService.GetAsync();
                var tagsString = tags.Select(t => t.Name).ToList();

                return Ok(tagsString);
            }
            catch (ArgumentOutOfRangeException)
            {
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}
