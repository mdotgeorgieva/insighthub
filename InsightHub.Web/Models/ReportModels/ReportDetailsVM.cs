﻿using InsightHub.Web.Models.IndexModels;
using System;
using System.Collections.Generic;

namespace InsightHub.Web.Models.ReportModels
{
	public class ReportDetailsVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public Guid AuthorId { get; set; }
        public string Author { get; set; }
        public string AuthorPhoto { get; set; }
        public ICollection<string> Tags { get; set; }
        public int? Downloads { get; set; }
        public DateTime CreatedOn { get; set; }
        public ICollection<ReportIndexVM> RelatedReports { get; set; }
    }
}
