﻿using InsightHub.Web.Models.IndexModels;
using System;
using System.Collections.Generic;

namespace InsightHub.Web.Models.AuthorsPageModels
{
	public class AuthorReportsQueryVM
	{
        public string SearchCriteria { get; set; } = "author";
        public string SearchString { get; set; }
        public string ImagePath { get; set; }
        public string Bio { get; set; }
        public Guid UserId { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int ItemsPerPage { get => 5; }
        public ICollection<ReportPageVM> Reports { get; set; }
    }
}
