﻿using System;

namespace InsightHub.Web.Models.AuthorsPageModels
{
	public class AuthorVM
	{
		public Guid Id { get; set; }
		public string Fullname { get; set; }
		public string Bio { get; set; }
		public string ImagePath { get; set; }
	}
}
