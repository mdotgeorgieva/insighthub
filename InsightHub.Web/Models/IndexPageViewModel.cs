﻿using InsightHub.Web.Models.IndexModels;
using System.Collections.Generic;

namespace InsightHub.Web.Models
{
	public class IndexPageViewModel
	{
		public ICollection<ReportIndexVM> New { get; set; }
		public ICollection<ReportIndexVM> Featured { get; set; }
		public ICollection<ReportIndexVM> Popular { get; set; }

		public ICollection<AuthorIndexVM> TopAuthors { get; set; }
		public ICollection<string> Industries { get; set; }
	}
}
