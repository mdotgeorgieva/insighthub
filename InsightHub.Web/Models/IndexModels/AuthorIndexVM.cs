﻿using System;

namespace InsightHub.Web.Models.IndexModels
{
	public class AuthorIndexVM
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string ImagePath { get; set; }
	}
}
