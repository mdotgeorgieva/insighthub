﻿using System;

namespace InsightHub.Web.Models.IndexModels
{
	public class ReportIndexVM
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string ImagePath { get; set; }
		public DateTime CreatedOn { get; set; }
	}
}
