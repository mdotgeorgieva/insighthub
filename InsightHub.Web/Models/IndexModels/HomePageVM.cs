﻿using System.Collections.Generic;

namespace InsightHub.Web.Models.IndexModels
{
	public class HomePageVM
	{
		public ICollection<ReportIndexVM> Featured { get; set; }
		public ICollection<ReportIndexVM> Popular { get; set; }
		public ICollection<ReportIndexVM> New { get; set; }
		public ICollection<AuthorIndexVM> Authors { get; set; }
	}
}
