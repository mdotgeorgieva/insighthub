﻿using System;
using System.Collections.Generic;

namespace InsightHub.Web.Models.IndexModels
{
	public class ReportPageVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public Guid AuthorId { get; set; }
        public string Author { get; set; }
        public ICollection<string> Tags { get; set; }
        public int? Downloads { get; set; }
        public bool IsFeatured { get; set; }
        public DateTime CreatedOn { get; set; }

    }
}
